﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Management;
using CertificationTracker.Services.Management;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration.CommandLine;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using System;
using System.IO;
using AutoMapper;
using CertificationTracker.Web.Helpers.Mappers;
using System.Net.Mail;
using System.Net;
using System.Collections.Generic;
using FluentEmail.Core.Models;
using CertificationTracker.Services;
using CertificationTracker.Interface;
using CertificationTracker.Dto.Credential;

namespace CertificationTracker.AutomatedCredentialReport
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var connectionString = GetConnectionString(args);
            var serviceProvider = RegisterServices(connectionString);

            var emailService = serviceProvider.GetService<IEmailService>();
            var userCredentialService = serviceProvider.GetService<IUserCredentialService>();
            var reportEmailService = serviceProvider.GetService<IReportEmailService>();
            var reportTypeService = serviceProvider.GetService<IReportTypeService>();

            var reportEmails = reportEmailService.GetReportEmails(reportTypeService.GetReportTypeByName("Credential Report").Id);
            //convert all the reportEmails to address

            var emails = new List<Address>();
            emails.Add(new Address { EmailAddress = "bibesh@primtek.net" });
            emailService.SendEmail<UserCredentialEmail>(emails, "Credential Expiration Report", $"{Directory.GetCurrentDirectory()}../../../../EmailTemplates/CredentialAutomatedReport.cshtml", 
                new UserCredentialEmail { 
                Name = "Credential Expiration Report",
                Description = "Listed are the credentials that are set to expire in the next 60 days",
                UserCredentialListingDtos = userCredentialService.GetUserCredentialExpiringInNextXDays(60)
            });

            emailService.SendEmail<UserCredentialEmail>(emails, "Credential Expired Report", $"{Directory.GetCurrentDirectory()}../../../../EmailTemplates/CredentialAutomatedReport.cshtml",
                new UserCredentialEmail {
                Name = "Credentials Expired Report",
                Description = "Listed are the credentials that are expired in the last 30 days",
                UserCredentialListingDtos = userCredentialService.GetUserCredentialExpiredInLastXDays(60)
            });
        }

        public static string GetConnectionString(string[] args)
        {
            IConfiguration Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddEnvironmentVariables()
               .AddCommandLine(args)
               .Build();

           return Configuration.GetConnectionString("DefaultConnection");
        }

        public static ServiceProvider RegisterServices(string connectionString)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            var serviceProvider = new ServiceCollection();

            serviceProvider.AddDbContext<DataContext>(options => options.UseSqlServer(connectionString, b => b.MigrationsAssembly("CertificationTracker.Web")));
            serviceProvider.AddTransient<ISettingService, SettingService>();
            serviceProvider.AddTransient<IEmailService, EmailService>();
            serviceProvider.AddTransient<IUserCredentialService, UserCredentialService>();
            serviceProvider.AddTransient<IReportEmailService, ReportEmailService>();
            serviceProvider.AddTransient<IReportTypeService, ReportTypeService>();

            serviceProvider.AddSingleton(mapper);

            var _settingService = serviceProvider.BuildServiceProvider().GetService<ISettingService>();

            var client = new SmtpClient(_settingService.GetSettingByKey("smtpHost"), Convert.ToInt32(_settingService.GetSettingByKey("smtpPort")))
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_settingService.GetSettingByKey("smtpUsername"), _settingService.GetSettingByKey("smtpPassword")),
                EnableSsl = true,
            };
            serviceProvider.AddFluentEmail(_settingService.GetSettingByKey("smtpUsername")).AddRazorRenderer().AddSmtpSender(client);

            return serviceProvider.BuildServiceProvider();
        }
    }
}
