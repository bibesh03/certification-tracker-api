﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using CertificationTracker.Models;

namespace CertificationTracker.Data
{
    public class DataContext : DbContext
    {
       
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .HasMany(x => x.Locations)
                .WithOne(x => x.Client)
                .HasForeignKey(x => x.ClientId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ProjectType>()
                .HasMany(x => x.Proposals)
                .WithOne(x => x.ProjectType)
                .HasForeignKey(x => x.ProjectTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Proposal>()
                .HasOne(x => x.Client)
                .WithMany(x => x.Proposals)
                .HasForeignKey(x => x.ClientId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Proposal>()
                .HasOne(x => x.Location)
                .WithMany(x => x.Proposals)
                .HasForeignKey(x => x.LocationId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TrainingSubType>()
                .HasOne(x => x.TrainingType)
                .WithMany(x => x.TrainingSubTypes)
                .HasForeignKey(x => x.TrainingTypeId);

            modelBuilder.Entity<Credential>()
                .HasOne(x => x.TrainingType)
                .WithMany(x => x.Credentials)
                .HasForeignKey(x => x.TrainingTypeId);

            modelBuilder.Entity<Credential>()
                .HasOne(x => x.TrainingSubType)
                .WithMany(x => x.Credentials)
                .HasForeignKey(x => x.TrainingSubTypeId);

            modelBuilder.Entity<UserCredential>()
                .HasOne(x => x.Credential)
                .WithMany(x => x.UserCredentials)
                .HasForeignKey(x => x.CredentialId);

            modelBuilder.Entity<UserCredential>()
                .HasMany(x => x.UserCredentialAttachments)
                .WithOne(x => x.UserCredential)
                .HasForeignKey(x => x.UserCredentialId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserIdsFromCredentialSearch>().HasNoKey();
        }

        public DbSet<Address> Address { get; set; }
        public DbSet<Credential> Credentials { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserCredential> UserCredentials { get; set; }
        public DbSet<UserInRole> UserInRoles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TrainingType> TrainingTypes { get; set; }
        public DbSet<IntervalType> IntervalTypes { get; set; }
        public DbSet<Location> Locations{ get; set; }
        public DbSet<LocationCredential> LocationCredentials { get; set; }
        public DbSet<ProjectType> ProjectTypes{ get; set; }
        public DbSet<ProjectSubType> ProjectSubTypes{ get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Proposal> Proposals{ get; set; }
        public DbSet<ReferralSource> ReferralSources { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<ReportType> ReportTypes { get; set; }
        public DbSet<ReportEmail> ReportEmails { get; set; }
        public DbSet<ProposalAttachment> ProposalAttachments { get; set; }
        public DbSet<ProposalContactLog> ProposalContactLogs { get; set; }
        public DbSet<TrainingSubType> TrainingSubTypes { get; set; }
        public DbSet<UserCredentialAttachment> UserCredentialAttachments { get; set; }
        public DbSet<ProposalReviewer> ProposalReviewers { get; set; }
        public DbSet<ProposalReviewersLog> ProposalReviewersLogs { get; set; }
        public DbSet<FormType> FormTypes { get; set; }
        public DbSet<UploadForm> UploadForms { get; set; }
        public DbSet<Form> Forms { get; set; }
        public DbSet<ProposalProject> ProposalProjects { get; set; }
        public DbSet<ProjectForm> ProjectForms { get; set; }
        public DbSet<ScopeOfWork> ScopeOfWorks{ get; set; }
        public DbSet<UnitOfCost> UnitOfCosts { get; set; }
        public DbSet<ScheduleOfCharge> ScheduleOfCharges{ get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportServer> ReportServers { get; set; }
        public DbSet<ProposalEstimatedConsolidatedCharge> ProposalEstimatedConsolidatedCharges { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<JobDocument> JobDocuments { get; set; }
        public DbSet<JobCredential> JobCredentials { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<JobCredentialAndBlock> JobCredentialAndBlocks { get; set; }
        public DbSet<JobCredentialOrBlock> JobCredentialOrBlock { get; set; }
        public DbSet<UserIdsFromCredentialSearch> UserIdsFromCredentialSearches { get; set; }
        public DbSet<JobTeamMember> JobTeamMembers { get; set; }
        public DbSet<ProposalProjectLocation> ProposalProjectLocations { get; set; }
    }
}
