﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using CertificationTracker.Models;

namespace CertificationTracker.Data
{
    public static class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            if (!context.Roles.Any())
            {
                SeedRoles(context);
            }

            if (!context.Users.Any())
            {
                SeedUsers(context);
            }


            if (!context.FormTypes.Any())
            {
                SeedFormTypes(context);
            }

            if (!context.UnitOfCosts.Any())
            {
                SeedUnitOfCosts(context);
            }
          
            if (!context.ReportServers.Any())
            {
                SeedReportServer(context);
            }
        }

        private static void SeedRoles(DataContext context){
            var roles = new Role[]
            {
                new Role
                {
                    Name = "Admin",
                    Description = "Admin role is capable of everything in the portal"
                },
                new Role
                {
                    Name = "Project Manager",
                    Description = "Project manager manages project"
                },
                new Role
                {
                    Name = "Supervisor",
                    Description = "Supervisor has more authority than other roles but less than admin"
                },
                new Role
                {
                    Name = "Worker",
                    Description = "Workers fill out the form and complete the tasks assigned to them"
                }
            };

            foreach (var role in roles)
            {
                context.Roles.Add(role);
            }

            context.SaveChanges();
        }

        private static void SeedUsers(DataContext context)
        {
            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            var users = new User[]
            {
                new User {
                    IsActive = true,
                    Email = "admin@CertificationTracker.com",
                    FirstName = "Admin",
                    LastName = "User",
                    UserName = "CertificationTrackerAdmin",
                    Password = HashPassword("P@$$w0rd", salt),
                    PasswordSalt = Convert.ToBase64String(salt),
                    HourlyRate = 12.2,
                    PhoneNumber = "123456789",
                    DateOfBirth = DateTime.Now,
                    Address = new Address
                    {
                        Address1 = "11862 Cloverland Ct",
                        City = "Baton Rouge",
                        State = "LA",
                        ZipCode = "70809",
                    }
                },
                new User {
                    IsActive = true,
                    Email = "admin@primtek.net",
                    FirstName = "Bibesh",
                    LastName = "KC",
                    UserName = "admin",
                    Password = HashPassword("pr1mt3kP@$$", salt),
                    PasswordSalt = Convert.ToBase64String(salt),
                    HourlyRate = 10,
                    PhoneNumber = "9859564088",
                    DateOfBirth = DateTime.Now,
                    Address = new Address
                    {
                        Address1 = "108 Business Park Ave ste a",
                        City = "Denham Springs",
                        State = "LA",
                        ZipCode = "70726",
                    }
                },
            };

            foreach (var user in users)
            {
                context.Users.Add(user);
            }

            context.SaveChanges();
        }

        private static void SeedFormTypes(DataContext context)
        {
            var formTypes = new FormType[]
            {
                new FormType
                {
                    Name = "UPLOAD",
                },
                new FormType
                {
                    Name = "BUILT_IN",
                },
                new FormType
                {
                    Name = "CUSTOM",
                }
            };

            foreach (var formType in formTypes)
            {
                context.FormTypes.Add(formType);
            }

            context.SaveChanges();
        }

        private static void SeedUnitOfCosts(DataContext context)
        {
            var unitOfCosts = new UnitOfCost[]
            {
                new UnitOfCost
                {
                    Unit = "Hour",
                },
                new UnitOfCost
                {
                    Unit = "Day",
                },
                new UnitOfCost
                {
                    Unit = "Sample",
                }
            };

            foreach (var unitOfCost in unitOfCosts)
            {
                context.UnitOfCosts.Add(unitOfCost);
            }

            context.SaveChanges();
        }

        private static void SeedReportServer(DataContext context)
        {
            context.ReportServers.Add(new ReportServer
            {
                Url = "http://67.225.138.34/ReportServer",
                UserName = "Administrator",
                Password = "{nslie7KKirmo3"
            });
            context.SaveChanges();
        }

        private static string HashPassword(string password, byte[] salt) => Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));
    }
}
