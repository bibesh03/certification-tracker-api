﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using CertificationTracker.Models;

public class ClientDetailDto
{
    public int Id { get; set; }
    public string AccountNumber { get; set; }
    public string Name { get; set; }
    public string ContactName { get; set; }
    public string PrimaryPhone { get; set; }
    public string Email { get; set; }
    public string AdditionalEmails { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string ZipCode { get; set; }
    public bool IsActive { get; set; }
    public bool DuplicateAsLocation { get; set; }
    public IFormFile UploadedFile { get; set; }
}
