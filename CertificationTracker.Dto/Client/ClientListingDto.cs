﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Client
{
    public class ClientListingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PrimaryPhone { get; set; }
        public string AccountNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int Locations { get; set; }
        public string IsActive{ get; set; }
        public string LogoPath { get; set; }
    }
}
