﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Credential
{
    public class CredentialListingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Frequency { get; set; }
        public string Description { get; set; }
        public double? CreditHours { get; set; }
        public string TrainingType { get; set; }
        public string TrainingSubType { get; set; }
        public string IntervalType { get; set; }
    }
}
