﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Credential
{
    public class EmployeeSearchDto
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string TWICPin { get; set; }
        public string SafetyCouncilStudentId { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DISAId { get; set; }
        public string AIId { get; set; }
    }
}
