﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Credential
{
    public class UserCredentialAttachmentDto
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public string UploadedDateTime { get; set; }
    }
}
