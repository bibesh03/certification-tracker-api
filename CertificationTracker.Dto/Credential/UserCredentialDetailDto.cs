﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Credential
{
    public class UserCredentialDetailDto : UserCredential
    {
        public List<IFormFile> UploadedFiles { get; set; }
    }
}
