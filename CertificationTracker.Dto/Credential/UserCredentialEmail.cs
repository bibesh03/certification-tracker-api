﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Credential
{
    public class UserCredentialEmail
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<UserCredentialListingDto> UserCredentialListingDtos { get; set; }
    }
}
