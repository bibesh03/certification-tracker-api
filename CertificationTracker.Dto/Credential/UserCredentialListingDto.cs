﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Credential
{
    public class UserCredentialListingDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Credential { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpiryDate { get; set; }
        public string ImageUrl { get; set; }
        public string SiteCode { get; set; }
        public bool IsArchived { get; set; }
        public string ArchivedDateTime { get; set; }
    }
}
