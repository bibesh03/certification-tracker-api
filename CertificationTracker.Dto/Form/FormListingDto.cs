﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Form
{
    public class FormListingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FormType { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
    }
}
