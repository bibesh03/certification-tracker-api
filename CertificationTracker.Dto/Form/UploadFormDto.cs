﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

public class UploadFormDto
{
    public int Id { get; set; }
    public int FormTypeId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string FilePath { get; set; }
    public IFormFile File { get; set; }
}
