﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Job
{
    public class JobCardViewDto
    {
        public int Id { get; set; }
        public int ProposalId { get; set; }
        public string ProjectNumber { get; set; }
        public string AwardedDateTime { get; set; }
        public string ProjectName { get; set; }
    }
}
