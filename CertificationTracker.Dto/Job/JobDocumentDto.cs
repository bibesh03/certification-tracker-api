﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Job
{
    public class JobDocumentDto
    {
        public int? JobId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public IFormFile File { get; set; }
    }
}
