﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Job
{
    public class JobListingDto
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string ProjectNumber { get; set; }
        public string Client { get; set; }
    }
}
