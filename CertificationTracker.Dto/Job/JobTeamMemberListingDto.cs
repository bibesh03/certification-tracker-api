﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Job
{
    public class JobTeamMemberListingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public bool IsFormLead { get; set; }
    }
}
