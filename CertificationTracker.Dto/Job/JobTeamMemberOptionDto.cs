﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Job
{
    public class JobTeamMemberOptionDto
    {
        public OptionsDto User { get; set; }
        public List<OptionsDto> RoleOptions { get; set; }
    }

    public class OptionsDto
    {
        public string Label { get; set; }
        public string Value { get; set; }
    }
}
