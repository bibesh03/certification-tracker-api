﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Project
{
    public class ProjectFormListingDto : ProjectForm
    {
        public string FormName { get; set; }
    }
}
