﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Project
{
    public class ProjectListingDto
    {
        public int Id { get; set; }
        public int ProjectTypeId { get; set; }
        public string ProjectType { get; set; }
        public int ProjectSubTypeId { get; set; }
        public string ProjectSubType { get; set; }
        public string Name { get; set; }
        public string Forms { get; set; }
    }
}
