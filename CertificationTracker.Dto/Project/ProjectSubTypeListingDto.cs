﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Project
{
    public class ProjectSubTypeListingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProjectType { get; set; }
        public string Description { get; set; }
    }
}
