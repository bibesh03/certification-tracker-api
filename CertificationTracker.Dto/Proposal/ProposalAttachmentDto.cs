﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalAttachmentDto : ProposalAttachment
    {
        public IFormFile UploadedFile { get; set; }
    }
}
