﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalAttachmentListingDto
    {
        public int Id { get; set; }
        public bool SendToClient { get; set; }
        public string Name { get; set; }
        public string Description{ get; set; }
        public string UploadedBy { get; set; }
        public string UploadedDateTime { get; set; }
        public string FileExtension { get; set; }
        public string FileLocation { get; set; }
    }
}
