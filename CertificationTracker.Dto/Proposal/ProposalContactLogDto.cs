﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalContactLogDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string FollowUpDate { get; set; }
        public string CreatedDate { get; set; }
        public string Description { get; set; }
        public string Role { get; set; }
    }
}
