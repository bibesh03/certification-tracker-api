﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalEstimatorDto
    {
        public int Id { get; set; }
        public string EstimatorFilePath { get; set; }
        public string ProposalEstimatedConsolidatedChargesString { get; set; }
        public decimal? BudgetPercentage { get; set; }
        public ICollection<ProposalEstimatedConsolidatedCharge> ProposalEstimatedConsolidatedCharges { get; set; }
        public decimal? EstimatedTotalCost { get; set; }

        public IFormFile UploadedFile { get; set; }
    }
}
