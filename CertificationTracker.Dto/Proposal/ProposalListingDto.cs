﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalListingDto
    {
        public int Id { get; set; }
        public string ProjectNumber { get; set; }
        public string ProposalId { get; set; }
        public string Client { get; set; }
        public string Location { get; set; }
        public string ProjectType { get; set; }
        public string ProjectSubType { get; set; }
        public string CreatedBy { get; set; }
        public string ExpiryDate { get; set; }
        public bool? IsApproved { get; set; }
    }
}
