﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalLogDto
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public string ProjectNumber { get; set; }
        public string CreatedDateTime { get; set; }
        public string Client { get; set; }
        public string ProjectType { get; set; }
        public string ProjectSubType { get; set; }
        public string Projects { get; set; }
        public string ProjectLocation { get; set; }
        public string ReferralSource { get; set; }
        public string ProjectManager { get; set; }
    }
}
