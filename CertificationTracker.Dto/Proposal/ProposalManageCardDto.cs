﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalManageCardDto
    {
        public string ProjectNumber { get; set; }
        public string ProposalNumber { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public string SubmittedBy { get; set; }
        public string SubmittedOn { get; set; }
    }
}
