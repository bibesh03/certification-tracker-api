﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalMessageInfoDto
    {
        public bool HasMore { get; set; }
        public List<ProposalMessageDto> ProposalMessages { get; set; }
    }

    public class ProposalMessageDto
    {
        public int? SenderId { get; set; }
        public int ProposalId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
    }
}
