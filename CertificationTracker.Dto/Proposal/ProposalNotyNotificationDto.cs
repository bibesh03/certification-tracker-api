﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalNotyNotificationDto
    {
        public List<String> ChatNoty { get; set; }
        public List<Notification> Notifications { get; set; }
    }
}
