﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalReviewerDto
    {
        public int Id { get; set; }
        public string ProjectNumber { get; set; }
        public int NoOfHoursForDueDate { get; set; }
        public int[] UserIdForReview { get; set; }
    }
}
