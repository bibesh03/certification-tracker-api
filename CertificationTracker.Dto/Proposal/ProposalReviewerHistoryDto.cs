﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalReviewerHistoryDto
    {
        public string FullName { get; set; }
        public string ImageUrl { get; set; }
        public string ProposalRole { get; set; }
        public string AssignedDate { get; set; }
        public bool HasReviewed { get; set; }
        public string ReviewedDateTime { get; set; }
    }
}
