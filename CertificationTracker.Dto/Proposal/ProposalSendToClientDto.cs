﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.Proposal
{
    public class ProposalSendToClientDto
    {
        public List<String> Emails { get; set; }
        public List<ProposalFile> ProposalFiles { get; set; }
    }

    public class ProposalFile
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
