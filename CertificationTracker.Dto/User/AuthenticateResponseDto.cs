﻿using System;
using System.Collections.Generic;
using System.Text;
using CertificationTracker.Models;

public class AuthenticateResponseDto
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Username { get; set; }
    public string Token { get; set; }
    public string ImageUrl { get; set; }

    public AuthenticateResponseDto(User user, string token)
    {
        Id = user.Id;
        FirstName = user.FirstName;
        LastName = user.LastName;
        Username = user.UserName;
        Token = token;
        Email = user.Email;
        ImageUrl = user.ImageUrl;
    }
}
