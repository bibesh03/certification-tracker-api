﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

public class RegisterUserDto
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public string ConfirmPassword { get; set; }
    public string Roles { get; set; }
    public string SSN { get; set; }
    public double? HourlyRate { get; set; }
    public bool IsActive { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string ZipCode { get; set; }
    public string PictureUrl { get; set; }
    public DateTime DateOfBirth { get; set; }
    public string TWICPin { get; set; }
    public string SafetyCouncilStudentId { get; set; }
    public string DriverLicenseNumber { get; set; }
    public string DISAId { get; set; }
    public string AIId { get; set; }
    public string SignatureImageUrl { get; set; }
    public IFormFile ImageUrl { get; set; }
    public IFormFile SignatureImage { get; set; }
}
