﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.User
{
    public class UserAvatarUpdateDto
    {
        public int Id { get; set; }
        public IFormFile ImageUrl { get; set; }
    }
}
