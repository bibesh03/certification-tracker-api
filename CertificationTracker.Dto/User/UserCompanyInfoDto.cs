﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.User
{
    public class UserCompanyInfoDto
    {
        public int Id { get; set; }
        public string Roles { get; set; }
        public string TWICPin { get; set; }
        public string SafetyCouncilStudentId { get; set; }
        public string DISAId { get; set; }
        public string AIId { get; set; }
        public bool IsActive { get; set; }
    }
}
