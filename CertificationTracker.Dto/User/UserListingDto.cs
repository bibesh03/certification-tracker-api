﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.User
{
    public class UserListingDto
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string ImageUrl { get; set; }
        public string Role { get; set; }
        public string UserName { get; set; }
        public string IsActive { get; set; }
    }
}
