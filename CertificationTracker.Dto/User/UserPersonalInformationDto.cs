﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Dto.User
{
    public class UserPersonalInformationDto : UserProfileCardDto
    {
        public string Username { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string SSN { get; set; }
        public double? HourlyRate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string SignatureImageUrl { get; set; }
        public IFormFile SignatureFile { get; set; }
    }
}
