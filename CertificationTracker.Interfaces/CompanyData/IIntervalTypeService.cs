﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface
{
    public interface IIntervalTypeService
    {
        IntervalType GetIntervalTypeById(int id);

        List<IntervalType> GetIntervalTypes();

        void SaveIntervalType(IntervalType IntervalType);

        void DeleteIntervalType(int id);
    }
}
