﻿using CertificationTracker.Dto.Project;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.CompanyData
{
    public interface IProjectFormService
    {
        ProjectForm GetProjectFormById(int id);

        List<ProjectFormListingDto> GetProjectFormsByProjectId(int projectId);

        bool SaveProjectForm(ProjectForm projectForm);

        void DeleteProjectForm(int id);
    }
}
