﻿using CertificationTracker.Dto.Project;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.CompanyData
{
    public interface IProjectService
    {
        Project GetProjectById(int id);

        List<ProjectListingDto> GetProjects();

        List<FormType> GetFormTypes();

        void SaveProject(Project Project);

        void DeleteProject(int id);
    }
}
