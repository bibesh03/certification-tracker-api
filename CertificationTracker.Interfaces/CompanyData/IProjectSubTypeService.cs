﻿using CertificationTracker.Dto.Project;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.CompanyData
{
    public interface IProjectSubTypeService
    {
        ProjectSubType GetProjectSubTypeById(int id);

        List<ProjectSubTypeListingDto> GetProjectSubTypes();

        void SaveProjectSubType(ProjectSubType projectSubType);

        void DeleteProjectSubType(int id);
    }
}
