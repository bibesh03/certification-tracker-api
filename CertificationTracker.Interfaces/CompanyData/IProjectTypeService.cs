﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.CompanyData
{
    public interface IProjectTypeService
    {
        ProjectType GetProjectTypeById(int id);

        List<ProjectType> GetProjectTypes();

        List<ProjectType> GetProjectTypesWithSubTypes();

        void SaveProjectType(ProjectType projectType);

        void DeleteProjectType(int id);
    }
}
