﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.CompanyData
{
    public interface IReferralSourceService
    {
        ReferralSource GetReferralSourceById(int id);

        List<ReferralSource> GetReferralSources();

        void SaveReferralSource(ReferralSource referralSource);

        void DeleteReferralSource(int id);
    }
}
