﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.CompanyData
{
    public interface ITrainingSubTypeService
    {
        TrainingSubType GetTrainingSubTypeById(int id);

        List<TrainingSubType> GetTrainingSubTypes(int trainingTypeId, bool showArchived);

        void SaveTrainingSubType(TrainingSubType trainingSubType);

        void ArchiveTrainingSubType(int id);

        bool UnArchiveTrainingSubType(int id);
    }
}
