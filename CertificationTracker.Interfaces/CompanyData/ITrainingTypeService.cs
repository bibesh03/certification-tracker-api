﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface
{
    public interface ITrainingTypeService
    {
        TrainingType GetTrainingTypeById(int id);

        List<TrainingType> GetTrainingTypes(bool showArchived);

        void SaveTrainingType(TrainingType trainingType);

        void ArchiveTrainingType(int id);

        void UnArchiveTrainingType(int id);
    }
}
