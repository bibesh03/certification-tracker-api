﻿using CertificationTracker.Dto.Form;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Form
{
    public interface IFormService
    {
        bool IsFormNameAvailable(string formName, int id);

        UploadFormDto GetUploadFormById(int formId);

        void DeleteForm(int formId);

        List<FormListingDto> GetAllForms();

        void SaveUploadForm(UploadFormDto uploadForm);
    }
}
