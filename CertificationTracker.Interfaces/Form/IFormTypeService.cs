﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Form
{
    public interface IFormTypeService
    {
        FormType GetFormTypeByKey(string key);
    }
}
