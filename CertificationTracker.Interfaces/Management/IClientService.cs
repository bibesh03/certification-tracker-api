﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface
{
    public interface IClientService
    {
        Client GetClientById(int id);

        List<Client> GetClients();

        int[] SaveClient(ClientDetailDto client, string filePath);

        void DeleteClient(int id);

        void UpdateClientLogo(int id, string filePath);

        void UpdateClient(Client client);

        string GetAllEmails(int clientId);
    }
}
