﻿using FluentEmail.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Management
{
    public interface IEmailService
    {
        void SendEmail(IEnumerable<Address> to, string subject, string body);

        void SendEmail<T>(IEnumerable<Address> to, string subject, string path, T model, List<Attachment> attachments = null);
    }
}
