﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface
{
    public interface ILocationService
    {
        Location GetLocationById(int id);

        List<Location> GetLocations(int clientId);

        int SaveLocation(Location location);

        void DeleteLocation(int id);
    }
}
