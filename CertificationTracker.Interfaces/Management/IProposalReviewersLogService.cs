﻿using CertificationTracker.Dto.Proposal;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Management
{
    public interface IProposalReviewersLogService
    {
        ProposalMessageInfoDto GetMessages(int proposalId, int length);

        bool SaveMessage(ProposalMessageDto messageDto);
    }
}
