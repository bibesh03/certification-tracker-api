﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Management
{
    public interface IReportEmailService
    {
        ReportEmail GetReportEmailById(int id);

        List<ReportEmail> GetReportEmails(int reportTypeId);

        void SaveReportEmail(ReportEmail reportEmail);

        void DeleteReportEmail(int id);
    }
}
