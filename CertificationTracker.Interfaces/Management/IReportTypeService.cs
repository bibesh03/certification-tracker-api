﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Management
{
    public interface IReportTypeService
    {
        ReportType GetReportTypeById(int id);

        List<ReportType> GetReportTypes(bool isAutomated);

        void SaveReportType(ReportType reportType);

        void DeleteReportType(int id);

        ReportType GetReportTypeByName(string name);
    }
}
