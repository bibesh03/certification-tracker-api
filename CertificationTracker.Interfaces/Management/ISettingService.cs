﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Management
{
    public interface ISettingService
    {
        Setting GetSettingById(int id);

        List<Setting> GetSettings();

        void SaveSetting(Setting setting);

        void DeleteSetting(int id);

        string GetSettingByKey(string key);
    }
}
