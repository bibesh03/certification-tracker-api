﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Management
{
    public interface IUnitOfCostService
    {
        UnitOfCost GetUnitOfCostById(int id);

        List<UnitOfCost> GetUnitOfCosts();
    }
}
