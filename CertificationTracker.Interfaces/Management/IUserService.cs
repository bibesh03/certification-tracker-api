﻿using System;
using System.Collections.Generic;
using System.Text;
using CertificationTracker.Dto.User;
using CertificationTracker.Models;

public interface IUserService
{
    User GetUserById(int id);

    int DoesUserNameOrEmailExist(string email, string username);

    List<UserListingDto> GetAllUsers(bool isProjectManagerView, bool isActive);

    AuthenticateResponseDto Authenticate(AuthenticateRequestDto model);

    bool RegisterUser(RegisterUserDto userToSave, string imageUrl, string signatureUrl);

    void ChangePassword(int userId, string password);

    void SaveUser(RegisterUserDto userToSave);

    RegisterUserDto GetUserDetails(int id);

    void UpdateUserAvatar(int userToUpdateId, string newFilePath);

    bool UpdatePersonalInformation(UserPersonalInformationDto updatedUser);

    void UpdateCompanyInformation(UserCompanyInfoDto updatedUser);

    void UpdateUsersSSN(int id, string ssn);

    string GetUserSSN(int id);

    string[] GetUserRoles(int id);
}
