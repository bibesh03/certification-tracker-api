﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

public interface INotificationService
{
    List<Notification> GetNotifications(int userId);

    void SetNotificationAsRead(int notificationId);

    void SaveNotification(Notification notification);
}
