﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Report
{
    public interface IPrintService
    {
        byte[] GetFileFromServer(string url, string userName, string password);
    }
}
