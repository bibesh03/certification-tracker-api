﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Report
{
    public interface IReportService
    {
        byte[] GetProposalPrintOut(int proposalId);
    }
}
