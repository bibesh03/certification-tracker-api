﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IJobCredentialAndBlockService
    {
        JobCredentialAndBlock GetJobCredentialAndBlockById(int id);

        List<JobCredentialAndBlock> GetJobCredentialAndBlocks(int jobId);

        List<JobCredentialAndBlock> SaveJobCredentialAndBlock(List<JobCredentialAndBlock> jobCredentialAndBlock);

        void DeleteJobCredentialAndBlock(int id, bool isOrBlock);

        bool IsCredentialCurrentlyBeingUsed(int jobId, int jobCredentialId);
    }
}
