﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IJobCredentialService
    {
        JobCredential GetJobCredentialById(int id);

        List<JobCredential> GetJobCredentials(int jobId);

        bool SaveJobCredential(List<JobCredential> jobCredentials);

        void DeleteJobCredential(int id);
    }
}
