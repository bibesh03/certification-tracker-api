﻿using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IJobDocumentService
    {
        JobDocument GetJobDocuments(int jobId);

        JobDocument GetJobDocumentById(int jobDocumentId);

        void Create(JobDocument jobDocument);

        void Rename(int jobDocumnetId, string newName);

        void Delete(int jobDocumentId);
    }
}
