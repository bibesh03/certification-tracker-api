﻿using CertificationTracker.Dto.Job;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IJobService
    {
        Job GetJobById(int id);

        List<JobListingDto> GetAllJobs();
        
        void SaveJob(Job job);
    }
}
