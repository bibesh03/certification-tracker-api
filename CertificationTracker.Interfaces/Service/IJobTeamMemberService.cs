﻿using CertificationTracker.Dto.Job;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IJobTeamMemberService
    {
        List<JobTeamMemberListingDto> GetJobTeamMembers(int jobId);

        List<JobTeamMemberOptionDto> GetAvailableUsersForTeamMember(int jobId);

        bool SaveJobTeamMember(JobTeamMember jobTeamMember);

        void DeleteTeamMember(int jobTeamMemberId);

        bool DoesFormLeadExist(int jobId);

        JobTeamMember GetJobTeamMemberById(int jobTeamMemberId);
    }
}
