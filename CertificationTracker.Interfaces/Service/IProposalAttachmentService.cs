﻿using CertificationTracker.Dto.Proposal;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IProposalAttachmentService
    {
        ProposalAttachment GetProposalAttachmentById(int id);

        List<ProposalAttachmentListingDto> GetProposalAttachments(int proposalId);

        void SaveProposalAttachment(ProposalAttachmentDto proposalAttachment, string filePath);

        void DeleteProposalAttachment(int proposalAttachmentId, int userId);
    }
}
