﻿using CertificationTracker.Dto.Proposal;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IProposalContactLogService
    {
        ProposalContactLog GetProposalContactLogById(int id);

        List<ProposalContactLogDto> GetProposalContactLogs(int proposalId);

        void SaveProposalContactLog(ProposalContactLog proposal);
    }
}
