﻿using CertificationTracker.Dto.Proposal;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface.Service
{
    public interface IProposalService
    {
        Proposal GetProposalById(int id);

        string GetProjectNumber(int proposalId);

        List<Proposal> GetProposals(string status, bool isAdmin, int userId);

        void SaveProposal(Proposal proposal);

        void DeleteProposal(int id, int userId);

        ProposalManageCardDto GetProposalManageInfo(int proposalId);

        ProposalNotyNotificationDto SaveProposalReviewers(ProposalReviewerDto reviewers, int userId);

        List<ProposalReviewer> GetProposalReviewers(int proposalId);

        Dictionary<string, string> GetProposalPermission(int proposalId, int userId);

        List<ProposalReviewerHistoryDto> GetReviewersHistory(int proposalId);

        ProposalNotyNotificationDto SetProposalAsReviewed(int proposalId, int userId);

        List<Notification> AddExtension(int proposalId, int noOfHours);

        ProposalEstimatorDto GetProposalEstimator(int proposalId);

        void SaveProposalEstimator(ProposalEstimatorDto proposalEstimatorDto);

        ProposalSendToClientDto GetProposalSendInfo(int proposalId);

        ProposalNotyNotificationDto SendProposalToClient(int proposalId, int userId);

        List<Notification> RejectProposal(int proposalId, int loggedInUserId, string rejectionReason);

        List<ProposalLogDto> GetProposalProjectLogs();

        List<Notification> ApproveProposal(int proposalId, int loggedInUserId);
    }
}
