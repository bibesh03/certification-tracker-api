﻿using CertificationTracker.Dto.Credential;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface
{
    public interface ICredentialService
    {
        Credential GetCredentialByName(string name);

        Credential GetCredentialById(int id);

        List<CredentialListingDto> GetCredentialsByTypeAndSubType(int typeId, int? subTypeId);

        List<CredentialListingDto> GetCredentials(bool showArchived);

        void SaveCredential(Credential Credential);

        void ArchiveCredential(int id);

        bool UnArchiveCredential(int id);

        void SetAllCredentialToArchivedByType(int typeId);

        void SetAllCredentialToArchivedBySubType(int subTypeId);
    }
}
