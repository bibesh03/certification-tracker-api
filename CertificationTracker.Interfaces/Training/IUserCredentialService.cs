﻿using CertificationTracker.Dto.Credential;
using CertificationTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Interface
{
    public interface IUserCredentialService
    {
        UserCredential GetUserCredentialById(int id);

        List<UserCredentialListingDto> GetUserCredentials(bool showArchived);

        void SaveUserCredential(UserCredentialDetailDto UserCredential);

        void DeleteUserCredential(int id);

        List<UserCredentialListingDto> GetUserCredentialByUserId(int userId, bool showArchived);

        bool? ToogleUserCredentialArchive(int userCredentialId);

        void SetAllUserCredentialToArchived(int credentialId);

        List<UserCredentialListingDto> GetUserCredentialExpiringInNextXDays(int days);

        List<UserCredentialListingDto> GetUserCredentialExpiredInLastXDays(int days);

        List<EmployeeSearchDto> SearchEmployeeByCredential(int[] credentialIds);

        UserCredential CheckDuplicateUserCredential(int userId, int credentialId, int? userCredentialId);

        ICollection<UserCredentialAttachmentDto> GetAttachments(int userCredentialId);

        void SaveAttachments(UserCredentialDetailDto userCredentialDetail);

        void RenameAttachment(int id, string name);

        void DeleteAttachment(int id);

        void MapAll();

        List<string> GetCredentialsFilePathsBasedOnJobForUser(int userId, int jobId);
    }
}
