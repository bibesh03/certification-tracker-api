﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string PrimaryPhone { get; set; }
        public string Email { get; set; }
        public string AdditionalEmails { get; set; }
        public string ContactName { get; set; }

        [ForeignKey("Address")]
        public int AddressId { get; set; }
        public string LogoPath { get; set; }
        public bool IsActive { get; set; }
        
        public virtual Address Address { get; set; }
        public virtual ICollection<Proposal> Proposals { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
    }
}
