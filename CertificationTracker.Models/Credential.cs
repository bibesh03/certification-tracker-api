﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class Credential
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Frequency { get; set; }
        public string Description { get; set; }
        public bool IsArchived { get; set; }

        [ForeignKey("TrainingType")]
        public int TrainingTypeId { get; set; }

        [ForeignKey("TrainingSubType")]
        public int? TrainingSubTypeId { get; set; }

        [ForeignKey("IntervalType")]
        public int IntervalTypeId { get; set; }

        public TrainingType TrainingType { get; set; }
        public TrainingSubType? TrainingSubType { get; set; }
        public IntervalType IntervalType { get; set; }
        public List<UserCredential> UserCredentials { get; set; }
    }
}
