﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class Form
    {
        public int Id { get; set; }
        [ForeignKey("FormType")]
        public int FormTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("UploadForm")]
        public int? UploadFormId { get; set; }
        
        public FormType FormType { get; set; }
        public UploadForm UploadForm { get; set; }
    }
}
