﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class FormType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Form> Forms { get; set; }
    }
}
