﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class Job
    {
        public int Id { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }
        public string SiteContact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EstimatedCompletionDate { get; set; }
        public string Duration { get; set; }
        public string JobScope { get; set; }
        public string SampleRequirements { get; set; }
        public string WastePermit { get; set; }
        public string Wastehauler { get; set; }
        public string LandFill { get; set; }
        public string JobNotes { get; set; }
        public int? EstimatedNumberOfProjectManagers { get; set; }
        public int? EstimatedNumberOfTechnicians { get; set; }
        public int? EstimatedNumberOfWorkers { get; set; }

        public Proposal Proposal { get; set; }
        public List<JobCredential> JobCredentials { get; set; }
        public List<JobDocument> JobDocuments { get; set; }
        public List<JobTeamMember> JobTeamMembers { get; set; }
        public List<JobCredentialAndBlock> JobCredentialAndBlocks { get; set; }
    }
}