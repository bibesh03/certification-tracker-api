﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class JobCredential
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Job")]
        [Column(Order = 1)]
        public int JobId { get; set; }

        [ForeignKey("Credential")]
        [Column(Order = 2)]
        public int CredentialId { get; set; }

        public Job Job { get; set; }
        public Credential Credential { get; set; }
    }
}
