﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class JobCredentialAndBlock
    {
        public int Id { get; set; }
        [ForeignKey("Job")]
        public int JobId { get; set; }
        [ForeignKey("CredentialId")]
        public int? CredentialId { get; set; }

        public Job Job { get; set; }
        public Credential Credential { get; set; }
        public List<JobCredentialOrBlock> JobCredentialOrBlocks { get; set; }
    }
}
