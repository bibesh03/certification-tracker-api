﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class JobCredentialOrBlock
    {
        public int Id { get; set; }
        [ForeignKey("JobCredentialAndBlock")]
        public int JobCredentialAndBlockId { get; set; }
        [ForeignKey("Credential")]
        public int CredentialId { get; set; }

        public JobCredentialAndBlock JobCredentialAndBlock { get; set; }
        public Credential Credential { get; set; }
    }
}
