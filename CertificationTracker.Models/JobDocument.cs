﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class JobDocument
    {
        public int Id { get; set; }
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        [ForeignKey("Job")]
        public int? JobId { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }

        [ForeignKey("User")]
        public int CreatedByUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedDateTime { get; set; }

        public JobDocument Parent { get; set; }
        public Job Job { get; set; }
        public User User { get; set; }
        public List<JobDocument> Children { get; set; }
    }
}
