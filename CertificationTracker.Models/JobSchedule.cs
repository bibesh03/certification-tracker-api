﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class JobSchedule
    {
        public int Id { get; set; }
        [ForeignKey("Job")]
        public int JobId { get; set; }

        [ForeignKey("JobTeamMember")]
        public int JobTeamMemberId { get; set; }

        [ForeignKey("ProposalProjectLocation")]
        public int ProposalProjectLocationId { get; set; }

        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }

        public Job Job { get; set; }
        public JobTeamMember JobTeamMember { get; set; }
        public ProposalProjectLocation ProposalProjectLocation { get; set; }
    }
}
