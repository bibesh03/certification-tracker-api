﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class JobTeamMember
    {
        [Key]
        public int Id { get; set; }
        
        [ForeignKey("Job")]
        [Column(Order = 1)]
        public int JobId { get; set; }

        [ForeignKey("User")]
        [Column(Order = 2)]
        public int UserId { get; set; }

        public string Role { get; set; }
        public bool IsFormLead { get; set; }
        public string JobCredentialPdfPath { get; set; }

        public Job Job { get; set; }
        public User User { get; set; }
    }
}