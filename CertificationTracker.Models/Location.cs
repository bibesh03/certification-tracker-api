﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class Location
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }

        [ForeignKey("Address")]
        public int AddressId { get; set; }
        public string BusinessName { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string AdditionalEmails { get; set; }
        public string AccountingName { get; set; }
        public string AccountingPhone { get; set; }
        public string AccountingEmail { get; set; }
        public string Description { get; set; }
        public virtual Client Client { get; set; }
        public virtual Address Address { get; set; }

        public List<LocationCredential> LocationCredentials { get; set; }
        public List<Proposal> Proposals { get; set; }
    }
}
