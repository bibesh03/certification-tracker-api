﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class LocationCredential
    {
        public int Id { get; set; }

        [ForeignKey("Location")]
        public int LocationId { get; set; }

        [ForeignKey("Credential")]
        public int CredentialId { get; set; }

        public Location Location { get; set; }
        public Credential Credential { get; set; }
    }
}
