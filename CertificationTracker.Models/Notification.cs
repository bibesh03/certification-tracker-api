﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class Notification
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime DateTime { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
        public bool HasOpened { get; set; }
    }
}
