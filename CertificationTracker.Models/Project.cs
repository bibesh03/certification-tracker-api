﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class Project
    {
        public int Id { get; set; }
        [ForeignKey("ProjectType")]
        public int? ProjectTypeId { get; set; }
        [ForeignKey("ProjectSubType")]
        public int? ProjectSubTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ProjectType ProjectType { get; set; }
        public virtual ProjectSubType ProjectSubType { get; set; }
        public List<ProjectForm> ProjectForms { get; set; }
    }
}
