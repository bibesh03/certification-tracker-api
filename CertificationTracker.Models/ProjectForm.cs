﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProjectForm
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Project")]
        [Column(Order = 1)]
        public int ProjectId { get; set; }

        [ForeignKey("Form")]
        [Column(Order = 2)]
        public int FormId { get; set; }
        public bool IsShareable { get; set; }
        public string Frequency { get; set; }

        public virtual Project Project { get; set; }
        public virtual Form Form {get;set;}
    }
}
