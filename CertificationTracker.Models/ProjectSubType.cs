﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProjectSubType
    {
        public int Id { get; set; }

        [ForeignKey("ProjectType")]
        public int? ProjectTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ProjectType ProjectType { get; set; }
        public List<Project> Projects { get; set; }
    }
}
