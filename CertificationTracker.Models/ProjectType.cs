﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProjectType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public List<ProjectSubType> ProjectSubTypes { get; set; }
        public List<Proposal> Proposals { get; set; }
    }
}
