﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class Proposal
    {
        public int Id { get; set; }
        public string ProjectNumber { get; set; }
        [ForeignKey("Client")]
        public int ClientId { get; set; }
        [ForeignKey("Location")]
        public int LocationId { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string AdditionalEmails { get; set; }
        public string ProjectName { get; set; }
        [ForeignKey("ProjectType")]
        public int ProjectTypeId { get; set; }
        [ForeignKey("ProjectSubType")]
        public int ProjectSubTypeId { get; set; }
        public string Description { get; set; }
        public DateTime ExpirationDate { get; set; }
        [ForeignKey("ReferralSource")]
        public int? ReferralSourceId { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        [ForeignKey("CreatedByUser")]
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        [ForeignKey("UpdatedByUser")]
        public int? UpdatedBy { get; set; }
        [ForeignKey("DeletedByUser")]
        public int? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedDateTime { get; set; }
        public string ProposalHeader { get; set; }
        public string ScheduleAndCharges { get; set; }
        [ForeignKey("ProposalSenderSignatureUser")]
        public int? ProposalSenderSignatureUserId { get; set; }
        public string ProposalSenderName { get; set; }
        public string ProposalSenderTitle { get; set; }
        public string EstimatorFilePath { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? EstimatedTotalCost { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? BudgetPercentage { get; set; }

        [ForeignKey("SentByUser")]
        public int? SentByUserId { get; set; }
        public DateTime? SentToClientDateTime { get; set; }
        public bool? HasApproved { get; set; }
        public DateTime? ApprovedOrRejectedDateTime { get; set; }
        [ForeignKey("ApprovedOrRejectedByUser")]
        public int? ApprovedOrRejectedByUserId { get; set; }
        public string RejectionReason { get; set; }

        public Job Job { get; set; }
        public ProjectType ProjectType { get; set; }
        public ProjectSubType ProjectSubType { get; set; }
        public Location Location { get; set; }
        public Client Client { get; set; }
        public User CreatedByUser { get; set; }
        public User UpdatedByUser { get; set; }
        public User DeletedByUser { get; set; }
        public User SentByUser { get; set; }
        public User ApprovedOrRejectedByUser { get; set; }
        public User ProposalSenderSignatureUser { get; set; }
        public ReferralSource ReferralSource { get; set; }

        public ICollection<ProposalReviewer> ProposalReviewers { get; set; }
        public ICollection<ScheduleOfCharge> ScheduleOfCharges { get; set; }
        public ICollection<ProposalProject> ProposalProjects { get; set; }
        public ICollection<ScopeOfWork> ScopeOfWorks { get; set; }
        public ICollection<ProposalEstimatedConsolidatedCharge> ProposalEstimatedConsolidatedCharges { get; set; }
        public ICollection<ProposalAttachment> ProposalAttachments { get; set; }
        public ICollection<ProposalProjectLocation> ProposalProjectLocations { get; set; }
    }
}
