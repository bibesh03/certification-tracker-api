﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProposalAttachment
    {
        
        public int Id { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }

        [ForeignKey("UploadedBy")]
        public int UploadedById { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime UploadedDateTime { get; set; }
        public string FileLocation { get; set; }
        public bool SendToClient { get; set; }
        [ForeignKey("DeletedBy")]
        public int? DeletedById { get; set; }
        public DateTime? DeletedDateTime { get; set; }

        public User UploadedBy { get; set; }
        public User DeletedBy { get; set; }
        public Proposal Proposal { get; set; }
    }
}
