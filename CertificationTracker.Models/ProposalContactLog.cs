﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProposalContactLog
    {
        public int Id { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public string Description { get; set; }
        public DateTime FollowUpDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public Proposal Proposal { get; set; }
        public User User { get; set; }
    }
}
