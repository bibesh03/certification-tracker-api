﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProposalEstimatedConsolidatedCharge
    {
        public int Id { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Cost { get; set; }

        public Proposal Proposal { get; set; }
    }
}
