﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProposalProject
    {
        public int Id { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }
        [ForeignKey("Project")]
        public int ProjectId { get; set; }

        public virtual Proposal Proposal { get; set; }
        public virtual Project Project { get; set; }
    }
}
