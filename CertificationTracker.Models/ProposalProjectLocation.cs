﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProposalProjectLocation
    {
        public int Id { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }
        public string ProjectLocation { get; set; }

        public Proposal Proposal { get; set; }
    }
}
