﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProposalReviewer
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }
        public bool IsReviewed { get; set; }
        public DateTime AssignedDateTime { get; set; }
        public DateTime ReviewDeadLineDateTime { get; set; }
        public DateTime? DateReviewed { get; set; }

        public Proposal Proposal { get; set; }
        public User User { get; set; }
    }
}
