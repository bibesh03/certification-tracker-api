﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ProposalReviewersLog
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int? SenderId { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }
        public DateTime DateTime { get; set; }
        public string Message { get; set; }

        public virtual User User { get; set; }
        public virtual Proposal Proposal { get; set; }
    }
}