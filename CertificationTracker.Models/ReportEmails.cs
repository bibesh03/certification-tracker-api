﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ReportEmail
    {
        public int Id { get; set; }

        [ForeignKey("ReportType")]
        public int ReportTypeId { get; set; }
        public string Email { get; set; }

        public ReportType ReportType { get; set; }
    }
}
