﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class ReportType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAutomated { get; set; }
        public List<ReportEmail> ReportEmails { get; set; }
    }
}
