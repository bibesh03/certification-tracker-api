﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<UserInRole> UserInRoles { get; set; }
    }
}
