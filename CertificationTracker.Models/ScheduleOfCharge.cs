﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ScheduleOfCharge
    {
        public int Id { get; set; }
        [ForeignKey("ProposalId")]
        public int ProposalId { get; set; }
        [ForeignKey("UnitOfCost")]
        public int UnitOfCostId { get; set; }
        public int SortOrder { get; set; }
        public string Service { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Cost { get; set; }

        public Proposal Proposal { get; set; }
        public UnitOfCost UnitOfCost { get; set; }
    }
}
