﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class ScopeOfWork
    {
        public int Id { get; set; }
        [ForeignKey("Proposal")]
        public int ProposalId { get; set; }
        public string Text { get; set; }
        public int SortOrder { get; set; }
        [ForeignKey("AddedByUser")]
        public int AddedByUserId { get; set; }
        public DateTime AddedDateTime { get; set; }

        public Proposal Proposal { get; set; }
        public User AddedByUser { get; set; }
    }
}
