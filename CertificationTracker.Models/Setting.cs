﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class Setting
    {
        public int Id {get;set;}
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
