﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class TrainingSubType
    {
        public int Id { get; set; }

        [ForeignKey("TrainingType")]
        public int TrainingTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsArchived { get; set; }

        public TrainingType TrainingType { get; set; }
        public List<Credential> Credentials { get; set; }
    }
}
