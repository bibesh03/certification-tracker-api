﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class TrainingType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsArchived { get; set; }

        public List<TrainingSubType> TrainingSubTypes { get; set; }
        public List<Credential> Credentials { get; set; }
    }
}
