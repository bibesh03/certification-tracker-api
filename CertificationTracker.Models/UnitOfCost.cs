﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class UnitOfCost
    {
        public int Id { get; set; }
        public string Unit { get; set; }
    }
}
