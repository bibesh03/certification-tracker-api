﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class UploadForm
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public DateTime UpdatedDateTime { get; set; }
    }
}
