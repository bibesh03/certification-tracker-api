﻿using System;
using System.Collections.Generic;

namespace CertificationTracker.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public double? HourlyRate { get; set; }
        public bool IsActive { get; set; }
        public string SSN { get; set; }
        public string SSNKey { get; set; }
        public string ImageUrl { get; set; }
        public string SignatureImageUrl { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string TWICPin { get; set; }
        public string SafetyCouncilStudentId { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DISAId { get; set; }
        public string AIId { get; set; }
        public int? AddressId { get; set; }
        public Address Address { get; set; }
        public ICollection<UserCredential> UserCredentials { get; set; }
        public ICollection<UserInRole> UserInRoles { get; set; }
    }
}
