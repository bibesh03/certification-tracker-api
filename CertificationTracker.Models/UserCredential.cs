﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CertificationTracker.Models
{
    public class UserCredential
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }

        [ForeignKey("TrainingType")]
        public int? TrainingTypeId { get; set; }

        [ForeignKey("TrainingSubType")]
        public int? TrainingSubTypeId { get; set; }

        [ForeignKey("Credential")]
        public int CredentialId { get; set; }

        public DateTime EffectiveDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Comments { get; set; }
        public string ImageUrl { get; set; }
        public bool IsArchived { get; set; }
        public string SiteCode { get; set; }
        public DateTime? ArchivedDateTime { get; set; }
        public User User { get; set; }

        public Credential Credential { get; set; }
        public TrainingType TrainingType { get; set; }
        public TrainingSubType TrainingSubType { get; set; }
        public List<UserCredentialAttachment> UserCredentialAttachments { get; set; }
    }
}
