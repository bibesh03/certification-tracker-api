﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    public class UserCredentialAttachment
    {
        public int Id { get; set; }
        public int UserCredentialId { get; set; }

        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public DateTime UploadedDateTime { get; set; }
        public bool IsDeleted { get; set; }

        public UserCredential UserCredential { get; set; }
    }
}
