﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CertificationTracker.Models
{
    [Keyless]
    public class UserIdsFromCredentialSearch
    {
        public int Ids { get; set; }
    }
}
