﻿using CertificationTracker.Data;
using CertificationTracker.Interface;
using CertificationTracker.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services
{
    public class IntervalTypeService : IIntervalTypeService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        
        public IntervalTypeService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public IntervalType GetIntervalTypeById(int id) => context.IntervalTypes.FirstOrDefault(x => x.Id == id);
        public List<IntervalType> GetIntervalTypes() =>  context.IntervalTypes.ToList();

        public void SaveIntervalType(IntervalType IntervalType)
        {
            if (IntervalType.Id > 0)
            {
                var savedIntervalType = GetIntervalTypeById(IntervalType.Id);
                mapper.Map(IntervalType, savedIntervalType);
            }
            else
            {
                context.IntervalTypes.Add(IntervalType);
            }

            context.SaveChanges();
        }

        public void DeleteIntervalType(int id)
        {
            context.IntervalTypes.Remove(GetIntervalTypeById(id));
            context.SaveChanges();
        }
    }
}
