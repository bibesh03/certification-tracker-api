﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Project;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.CompanyData
{
    public class ProjectFormService : IProjectFormService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ProjectFormService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ProjectForm GetProjectFormById(int id) => context.ProjectForms.FirstOrDefault(x => x.Id == id);

        public List<ProjectFormListingDto> GetProjectFormsByProjectId(int projectId)
        {
            var projects = context.ProjectForms.Include(x => x.Form).Where(x => x.ProjectId == projectId).ToList();

            return mapper.Map<List<ProjectFormListingDto>>(projects);
        }

        public bool SaveProjectForm(ProjectForm projectForm)
        {
            try
            {
                if (projectForm.Id > 0)
                {
                    var savedProjectForm = GetProjectFormById(projectForm.Id);
                    mapper.Map(projectForm, savedProjectForm);
                }
                else
                {
                    context.ProjectForms.Add(projectForm);
                }

                context.SaveChanges();

                return true;
            } catch (Exception ex)
            {
                return false;
            }
        }

        public void DeleteProjectForm(int id)
        {
            context.ProjectForms.Remove(GetProjectFormById(id));
            context.SaveChanges();
        }
    }
}
