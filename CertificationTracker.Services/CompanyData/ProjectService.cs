﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Project;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.CompanyData
{
    public class ProjectService : IProjectService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ProjectService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Project GetProjectById(int id) => context.Projects.Include(x => x.ProjectForms).FirstOrDefault(x => x.Id == id);

        public List<FormType> GetFormTypes() => context.FormTypes.ToList();

        public List<ProjectListingDto> GetProjects() {
            var allActiveProjects = context.Projects.Include(x => x.ProjectType).Include(x => x.ProjectSubType).Include(x => x.ProjectForms).ThenInclude(x => x.Form).Where(x => x.IsDeleted == false).OrderBy(x => x.Name).ToList();

            return mapper.Map<List<ProjectListingDto>>(allActiveProjects);
        } 

        public void SaveProject(Project project)
        {
            if (project.Id > 0)
            {
                var savedProject = GetProjectById(project.Id);
                mapper.Map(project, savedProject);
            }
            else
            {
                context.Projects.Add(project);
            }

            context.SaveChanges();
        }

        public void DeleteProject(int id)
        {
            var project = GetProjectById(id);
            project.IsDeleted = true;

            context.SaveChanges();
        }
    }
}
