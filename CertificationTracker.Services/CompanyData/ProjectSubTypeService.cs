﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Project;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.CompanyData
{
    public class ProjectSubTypeService : IProjectSubTypeService
    {
        private readonly DataContext context;
        private readonly IProjectService projectService;
        private readonly IMapper mapper;

        public ProjectSubTypeService(DataContext _context, IMapper _mapper, IProjectService _projectService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.projectService = _projectService;
        }

        public ProjectSubType GetProjectSubTypeById(int id) => context.ProjectSubTypes.FirstOrDefault(x => x.Id == id);

        public List<ProjectSubTypeListingDto> GetProjectSubTypes()
        {
            var activeProjects = context.ProjectSubTypes.Where(x => x.IsDeleted == false).Include(x => x.ProjectType).OrderBy(x => x.Name).ToList();

            return mapper.Map<List<ProjectSubTypeListingDto>>(activeProjects);
        }

        public void SaveProjectSubType(ProjectSubType projectSubType)
        {
            if (projectSubType.Id > 0)
            {
                var savedProjectSubType = GetProjectSubTypeById(projectSubType.Id);
                mapper.Map(projectSubType, savedProjectSubType);
            }
            else
            {
                context.ProjectSubTypes.Add(projectSubType);
            }

            context.SaveChanges();
        }

        public void DeleteProjectSubType(int id)
        {
            var projectSubType = GetProjectSubTypeById(id);
            projectSubType.IsDeleted = true;
            context.Projects.Where(x => x.ProjectSubTypeId == id).Select(x => x.Id).ToList().ForEach(x =>
            {
                projectService.DeleteProject(x);
            });

            context.SaveChanges();
        }
    }
}
