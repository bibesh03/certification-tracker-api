﻿using CertificationTracker.Data;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.CompanyData
{
    public class ProjectTypeService : IProjectTypeService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly IProjectSubTypeService projectSubTypeService;
        private readonly IProjectService projectService;

        public ProjectTypeService(DataContext _context, IMapper _mapper, IProjectSubTypeService _projectSubTypeService, IProjectService _projectService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.projectSubTypeService = _projectSubTypeService;
            this.projectService = _projectService;
        }

        public ProjectType GetProjectTypeById(int id) => context.ProjectTypes.Include(x => x.ProjectSubTypes).FirstOrDefault(x => x.Id == id);

        public List<ProjectType> GetProjectTypesWithSubTypes() => context.ProjectTypes.Where(x => x.IsDeleted == false).Include(x => x.ProjectSubTypes).OrderBy(x => x.Name).ToList();

        public List<ProjectType> GetProjectTypes() => context.ProjectTypes.Where(x => x.IsDeleted == false).OrderBy(x => x.Name).ToList();

        public void SaveProjectType(ProjectType projectType)
        {
            if (projectType.Id > 0)
            {
                var savedProjectType = GetProjectTypeById(projectType.Id);
                mapper.Map(projectType, savedProjectType);
            }
            else
            {
                context.ProjectTypes.Add(projectType);
            }

            context.SaveChanges();
        }

        public void DeleteProjectType(int id)
        {
            ProjectType projectType = GetProjectTypeById(id);
            projectType.IsDeleted = true;
            projectType.ProjectSubTypes.ForEach(x =>
            {
                projectSubTypeService.DeleteProjectSubType(x.Id);
            });

            context.Projects.Where(x => x.ProjectTypeId == id).Select(x => x.Id).ToList().ForEach(x =>
            {
                projectService.DeleteProject(x);
            });

            context.SaveChanges();
        }
    }
}
