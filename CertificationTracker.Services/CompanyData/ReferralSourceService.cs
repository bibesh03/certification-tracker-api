﻿using CertificationTracker.Data;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.CompanyData
{
    public class ReferralSourceService : IReferralSourceService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ReferralSourceService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ReferralSource GetReferralSourceById(int id) => context.ReferralSources.FirstOrDefault(x => x.Id == id);

        public List<ReferralSource> GetReferralSources() => context.ReferralSources.OrderBy(x => x.Name).ToList();

        public void SaveReferralSource(ReferralSource referralSource)
        {
            if (referralSource.Id > 0)
            {
                var savedReferralSource = GetReferralSourceById(referralSource.Id);
                mapper.Map(referralSource, savedReferralSource);
            }
            else
            {
                context.ReferralSources.Add(referralSource);
            }

            context.SaveChanges();
        }

        public void DeleteReferralSource(int id)
        {
            context.ReferralSources.Remove(GetReferralSourceById(id));
            context.SaveChanges();
        }
    }
}
