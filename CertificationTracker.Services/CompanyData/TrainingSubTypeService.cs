﻿using CertificationTracker.Data;
using CertificationTracker.Interface;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.CompanyData
{
    public class TrainingSubTypeService : ITrainingSubTypeService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly ICredentialService credentialService;

        public TrainingSubTypeService(DataContext _context, IMapper _mapper, ICredentialService _credentialService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.credentialService = _credentialService;
        }

        public TrainingSubType GetTrainingSubTypeById(int id) => context.TrainingSubTypes.Include(x => x.TrainingType).Include(x => x.Credentials).FirstOrDefault(x => x.Id == id);

        public List<TrainingSubType> GetTrainingSubTypes(int trainingTypeId, bool showArchived) => context.TrainingSubTypes.Where(x => x.TrainingTypeId == trainingTypeId && x.IsArchived == showArchived).OrderBy(x => x.Name).ToList();

        public void SaveTrainingSubType(TrainingSubType trainingSubType)
        {
            if (trainingSubType.Id > 0)
            {
                var savedTrainingSubType = context.TrainingSubTypes.FirstOrDefault(x => x.Id == trainingSubType.Id); //GetTrainingSubTypeById(trainingSubType.Id);
                mapper.Map(trainingSubType, savedTrainingSubType);
            }
            else {
                context.TrainingSubTypes.Add(trainingSubType);
            }

            context.SaveChanges();
        }

        public bool UnArchiveTrainingSubType(int id)
        {
            var subTypeToArchive = GetTrainingSubTypeById(id);
            if (subTypeToArchive.TrainingType.IsArchived)
            {
                return false;
            }
            subTypeToArchive.IsArchived = false;

            context.SaveChanges();
            return true;
        }

        public void ArchiveTrainingSubType(int id)
        {
            var subTypeToArchive = GetTrainingSubTypeById(id);
            credentialService.SetAllCredentialToArchivedBySubType(id);
            subTypeToArchive.IsArchived = true;

            context.SaveChanges();
        }
    }
}
