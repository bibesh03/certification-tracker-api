﻿using CertificationTracker.Data;
using CertificationTracker.Interface;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services
{
    public class TrainingTypeService : ITrainingTypeService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly ITrainingSubTypeService trainingSubTypeService;
        private readonly ICredentialService credentialService;

        public TrainingTypeService(DataContext _context, IMapper _mapper, ITrainingSubTypeService _trainingSubTypeService, ICredentialService _credentialService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.trainingSubTypeService = _trainingSubTypeService;
            this.credentialService = _credentialService;
        }

        public TrainingType GetTrainingTypeById(int id) => context.TrainingTypes.Include(x => x.TrainingSubTypes).FirstOrDefault(x => x.Id == id);

        public List<TrainingType> GetTrainingTypes(bool showArchived) => context.TrainingTypes.Where(x => x.IsArchived == showArchived).OrderBy(x => x.Name).ToList();

        public void SaveTrainingType(TrainingType trainingType)
        {
            if (trainingType.Id > 0)
            {
                var savedTrainingType = GetTrainingTypeById(trainingType.Id);
                mapper.Map(trainingType, savedTrainingType);
            }
            else {
                context.TrainingTypes.Add(trainingType);
            }

            context.SaveChanges();
        }

        public void UnArchiveTrainingType(int id)
        {
            var trainingType = GetTrainingTypeById(id);
            trainingType.IsArchived = false;

            context.SaveChanges();
        }

        public void ArchiveTrainingType(int id)
        {
            var trainingType = GetTrainingTypeById(id);
            trainingType.TrainingSubTypes.ForEach(subType =>
            {
                trainingSubTypeService.ArchiveTrainingSubType(subType.Id);
            });
            credentialService.SetAllCredentialToArchivedByType(trainingType.Id);

            trainingType.IsArchived = true;

            context.SaveChanges();
        }
    }
}
