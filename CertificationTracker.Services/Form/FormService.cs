﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Form;
using CertificationTracker.Interface.Form;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FormService : IFormService
{
    private readonly DataContext context;
    private readonly IMapper mapper;
    private readonly IFormTypeService formTypeService;

    public FormService(DataContext _context, IMapper _mapper, IFormTypeService _formTypeService)
    {
        this.context = _context;
        this.mapper = _mapper;
        this.formTypeService = _formTypeService;
    }

    public List<FormListingDto> GetAllForms()
    {
        var allForms = context.Forms.Include(x => x.FormType).Include(x => x.UploadForm).Where(x => x.IsDeleted == false).ToList();

        return mapper.Map<List<FormListingDto>>(allForms);
    }

    public UploadFormDto GetUploadFormById(int formId)
    {
        var savedUploadedForm = context.Forms.Include(x => x.UploadForm).FirstOrDefault(x => x.Id == formId);

        return mapper.Map<UploadFormDto>(savedUploadedForm);
    }

    public void DeleteForm(int formId)
    {
        var savedForm = context.Forms.FirstOrDefault(x => x.Id == formId);
        savedForm.IsDeleted = true;

        context.SaveChanges();
    }

    public bool IsFormNameAvailable(string formName, int id)
    {
        var formWithName = context.Forms.Where(x => x.Name == formName && x.IsDeleted == false);
        if (id > 0)
        {
            formWithName = formWithName.Where(x => x.Id != id);
        }

        return formWithName.FirstOrDefault() == null;
    }

    public void SaveUploadForm(UploadFormDto uploadForm)
    {
        var form = mapper.Map<Form>(uploadForm);
        form.FormTypeId = formTypeService.GetFormTypeByKey("UPLOAD").Id;

        if (uploadForm.Id > 0)
        {
            var savedUploadForm = context.Forms.Include(x => x.UploadForm).FirstOrDefault(x => x.Id == uploadForm.Id);
            savedUploadForm.Name = form.Name;
            savedUploadForm.Description = form.Description;
            savedUploadForm.UploadForm.FilePath = form.UploadForm.FilePath;
            savedUploadForm.UploadForm.UpdatedDateTime = DateTime.Now;
        } else
        {
            context.Forms.Add(form);
        }

        context.SaveChanges();
    }
}
