using CertificationTracker.Data;
using CertificationTracker.Interface;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services
{
    public class ClientService : IClientService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ClientService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Client GetClientById(int id) => context.Clients.Include(x => x.Address).FirstOrDefault(x => x.Id == id);

        public List<Client> GetClients() =>  context.Clients.Include(x => x.Locations).Include(x => x.Address).ToList();

        public void UpdateClient(Client client)
        {
            var savedClient = GetClientById(client.Id);
            mapper.Map(client, savedClient);

            context.SaveChanges();
        }

        public int[] SaveClient(ClientDetailDto clientToSave, string filePath)
        {
            var client = mapper.Map<Client>(clientToSave);
            var clientLocation = new Location();
            if (!String.IsNullOrEmpty(filePath))
            {
                client.LogoPath = filePath;
            }
            
            if (clientToSave.DuplicateAsLocation)
            {
                Address locationAddress = new Address();
                client.Locations = new List<Location>();
                clientLocation = new Location
                {
                    Address = mapper.Map(clientToSave, locationAddress),
                    ContactName = clientToSave.ContactName,
                    ContactEmail = clientToSave.Email,
                    ContactPhone = clientToSave.PrimaryPhone,
                    AdditionalEmails = clientToSave.AdditionalEmails
                };
                client.Locations.Add(clientLocation);
            }
            context.Clients.Add(client);
            context.SaveChanges();

            return new int[2] { client.Id, clientLocation.Id };
        }

        public void DeleteClient(int id)
        {
            context.Clients.Remove(GetClientById(id));
            context.SaveChanges();
        }

        public void UpdateClientLogo(int clientId, string filePath)
        {
            var clientToEdit = GetClientById(clientId);
            clientToEdit.LogoPath = filePath;

            context.SaveChanges();
        }

        public string GetAllEmails(int clientId)
        {
            StringBuilder sb = new StringBuilder();
            var client = context.Clients.Include(x => x.Locations).FirstOrDefault(x => x.Id == clientId);
            sb.Append(String.IsNullOrEmpty(client.Email) ? "" : client.Email + ", ");
            sb.Append(String.IsNullOrEmpty(client.AdditionalEmails) ? "" : client.AdditionalEmails + ", ");
            sb.Append(String.Join(",", client.Locations.Select(x => (String.IsNullOrEmpty(x.ContactEmail) ? "" : (x.ContactEmail)) + (String.IsNullOrEmpty(x.AdditionalEmails) ? "" : "," + x.AdditionalEmails)).ToArray()));

            var emails = sb.ToString();
            if (emails == "")
            {
                return "";
            }
            if (emails[0] == ',')
            {
                return emails.Substring(1, emails.Length-1);
            }

            return emails;
        }
    }
}
                                                                                                                                                                                                                                           