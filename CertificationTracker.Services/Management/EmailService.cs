﻿using CertificationTracker.Interface.Management;
using FluentEmail.Core;
using FluentEmail.Core.Models;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CertificationTracker.Services.Management
{
    public class EmailService : IEmailService
    {
        private readonly IFluentEmail email;
        private readonly IConfiguration configuration;
        public EmailService(IFluentEmail _email, IConfiguration _configuration)
        {
            this.email = _email;
            this.configuration = _configuration;
        }

        public void SendEmail(IEnumerable<Address> to, string subject, string body)
        {
            email
                .To(to)
                .Subject(subject)
                .Body(body)
                .Send();
        }

        public void SendEmail<T>(IEnumerable<Address> to, string subject, string path, T model, List<Attachment> attachments = null)
        {
            //var addressToSendTo = configuration["Environment"] == "development" ? new List<Address> {
            //    new Address
            //    {
            //        Name = "Bibesh KC",
            //        EmailAddress = "bibesh@primtek.net"
            //    }
            //} : to;

            var addressToSendTo = new List<Address> { 
                new Address
                {
                    Name = "Bibesh KC",
                    EmailAddress = "bibesh@primtek.net"
                } 
            };

            email.To(addressToSendTo)
                .Subject(subject)
                .UsingTemplateFromFile(path, model)
                .Attach(attachments == null ? new List<Attachment>() : attachments)
                .Send();
        }
    }
}
