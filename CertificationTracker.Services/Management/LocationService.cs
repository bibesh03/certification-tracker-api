﻿using CertificationTracker.Data;
using CertificationTracker.Interface;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services
{
    public class LocationService : ILocationService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public LocationService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Location GetLocationById(int id) => context.Locations.Include(x => x.Address).Include(x => x.LocationCredentials).FirstOrDefault(x => x.Id == id);

        public List<Location> GetLocations(int clientId) => context.Locations.Include(x => x.Address).Where(x => x.ClientId == clientId).ToList();

        public int SaveLocation(Location location)
        {
            if (location.Id > 0)
            {
                var savedLocation = GetLocationById(location.Id);
                mapper.Map(location, savedLocation);
            }
            else
            {
                context.Locations.Add(location);
            }
            context.SaveChanges();

            return location.Id;
        }

        public void DeleteLocation(int id)
        {
            context.Locations.Remove(GetLocationById(id));
            context.SaveChanges();
        }
    }
}
