﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Management;
using CertificationTracker.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Management
{
    public class ReportEmailService : IReportEmailService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ReportEmailService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ReportEmail GetReportEmailById(int id) => context.ReportEmails.FirstOrDefault(x => x.Id == id);
        public List<ReportEmail> GetReportEmails(int reportTypeId) => context.ReportEmails.Where(x => x.ReportTypeId == reportTypeId).ToList();

        public void SaveReportEmail(ReportEmail reportEmail)
        {
            if (reportEmail.Id > 0)
            {
                var savedReportEmail = GetReportEmailById(reportEmail.Id);
                mapper.Map(reportEmail, savedReportEmail);
            }
            else
            {
                context.ReportEmails.Add(reportEmail);
            }

            context.SaveChanges();
        }

        public void DeleteReportEmail(int id)
        {
            context.ReportEmails.Remove(GetReportEmailById(id));
            context.SaveChanges();
        }
    }
}
