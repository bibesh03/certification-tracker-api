﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Management;
using CertificationTracker.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Management
{
    public class ReportTypeService : IReportTypeService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ReportTypeService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ReportType GetReportTypeById(int id) => context.ReportTypes.FirstOrDefault(x => x.Id == id);

        public ReportType GetReportTypeByName(string name) => context.ReportTypes.FirstOrDefault(x => x.Name.ToUpper() == name.ToUpper());
        public List<ReportType> GetReportTypes(bool isAutomated) => context.ReportTypes.Where(x => x.IsAutomated == isAutomated).ToList();

        public void SaveReportType(ReportType reportType)
        {
            if (reportType.Id > 0)
            {
                var savedReportType = GetReportTypeById(reportType.Id);
                mapper.Map(reportType, savedReportType);
            }
            else
            {
                context.ReportTypes.Add(reportType);
            }

            context.SaveChanges();
        }

        public void DeleteReportType(int id)
        {
            context.ReportTypes.Remove(GetReportTypeById(id));
            context.SaveChanges();
        }
    }
}
