﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Management;
using CertificationTracker.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Management
{
    public class SettingService : ISettingService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public SettingService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Setting GetSettingById(int id) => context.Settings.FirstOrDefault(x => x.Id == id);

        public List<Setting> GetSettings() => context.Settings.ToList();

        public string GetSettingByKey(string key) => context.Settings.FirstOrDefault(x => x.Key.ToLower() == key.ToLower()).Value;

        public void SaveSetting(Setting setting)
        {
            if (setting.Id > 0)
            {
                var savedSetting = GetSettingById(setting.Id);
                mapper.Map(setting, savedSetting);
            }
            else
            {
                context.Settings.Add(setting);
            }

            context.SaveChanges();
        }

        public void DeleteSetting(int id)
        {
            context.Settings.Remove(GetSettingById(id));
            context.SaveChanges();
        }
    }
}
