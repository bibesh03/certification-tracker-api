﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Management;
using CertificationTracker.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Management
{
    public class UnitOfCostService : IUnitOfCostService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public UnitOfCostService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public UnitOfCost GetUnitOfCostById(int id) => context.UnitOfCosts.FirstOrDefault(x => x.Id == id);

        public List<UnitOfCost> GetUnitOfCosts() => context.UnitOfCosts.ToList();
    }
}
