﻿using AutoMapper;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using CertificationTracker.Data;
using CertificationTracker.Dto.User;
using CertificationTracker.Models;
using Microsoft.EntityFrameworkCore;
using System.IO;
using CertificationTracker.Interface.Management;

namespace CertificationTracker.Services
{
    public class UserService : IUserService
    {
        private readonly DataContext context;
        public readonly IConfiguration configuration;
        private readonly IMapper mapper;

        public UserService(DataContext _context, IConfiguration _configuration, IMapper _mapper)
        {
            this.context = _context;
            this.configuration = _configuration;
            this.mapper = _mapper;
        }

        public User GetUserById(int id) => context.Users.Include(x => x.UserInRoles).ThenInclude(x => x.Role).Include(x => x.Address).FirstOrDefault(x => x.Id == id);

        public string[] GetUserRoles(int id) => context.UserInRoles.Include(x => x.Role).Where(x => x.UserId == id).Select(x => x.Role.Name).ToArray();

        public RegisterUserDto GetUserDetails(int id)
        {
            var user = context.Users.Include(x => x.UserInRoles).ThenInclude(x => x.Role).Include(x => x.Address).FirstOrDefault(x => x.Id == id);
            
            return mapper.Map<RegisterUserDto>(user);
        }

        public int DoesUserNameOrEmailExist(string email, string username) => context.Users.Where(x => x.Email == email || x.UserName == username).Count();

        public List<UserListingDto> GetAllUsers(bool isProjectManagerView, bool isActive)
        {
            var allUser = context.Users.Include(x => x.UserInRoles).ThenInclude(x => x.Role).OrderBy(x => x.LastName).Where(x => x.IsActive == isActive).AsQueryable();
            
            return mapper.Map<List<UserListingDto>>(isProjectManagerView ? allUser.Where(x => !x.UserInRoles.Any(y => y.Role.Name == "Admin")).ToList() : allUser.ToList());
        }

        public AuthenticateResponseDto Authenticate(AuthenticateRequestDto model)
        {
            var user = context.Users.Include(x => x.UserInRoles).ThenInclude(x => x.Role).FirstOrDefault(x => (x.UserName == model.Email || x.Email == model.Email) && x.IsActive == true);
            if (user == null)
            {
                return null;
            }
            if (user.Password != HashPassword(model.Password, Convert.FromBase64String(user.PasswordSalt)))
            {
                return null;
            }

            var jwtToken = GenerateJWTToken(user);
            return new AuthenticateResponseDto(user, jwtToken);
        }

        public bool RegisterUser(RegisterUserDto userToSave, string imagePath, string signaturePath)
        {
            var user = mapper.Map<User>(userToSave);
            if (!String.IsNullOrEmpty(imagePath))
            {
                user.ImageUrl = imagePath;
            }
            if (!String.IsNullOrEmpty(signaturePath))
            {
                user.SignatureImageUrl = signaturePath;
            }

            SetUserRoles(user, userToSave);
            SetPasword(user, userToSave.Password);
            if (!String.IsNullOrEmpty(user.SSN))
            {
                user.SSNKey = GenerateKey();
                user.SSN = EncryptText(user.SSN, user.SSNKey);
            }
            context.Users.Add(user);

            return context.SaveChanges() > 0;
        }

        private void SetUserRoles(User user, RegisterUserDto userToSave)
        {
            user.UserInRoles = new List<UserInRole>();
            userToSave.Roles.Split(',').ToList().ForEach(x =>
            {
                int roleId = context.Roles.Where(a => a.Name.ToUpper() == x.ToUpper()).Select(b => b.Id).FirstOrDefault(); //.FirstOrDefault(y => y.Name.ToUpper() == x).Id;
                user.UserInRoles.Add(new UserInRole
                {
                    RoleId = roleId
                });
            });
        }

        public void UpdateUserAvatar(int userToUpdateId, string newFilePath)
        {
            var user = GetUserById(userToUpdateId);
            user.ImageUrl = newFilePath;

            context.SaveChanges();
        }

        public void UpdateCompanyInformation(UserCompanyInfoDto updatedUser)
        {
            var user = GetUserById(updatedUser.Id);
            mapper.Map(updatedUser, user);
            var regModel = new RegisterUserDto();
            SetUserRoles(user, mapper.Map(updatedUser, regModel));

            context.SaveChanges();
        }

        public bool UpdatePersonalInformation(UserPersonalInformationDto updatedUser)
        {
            var userWithDifferentEmail = context.Users.FirstOrDefault(x => x.Id !=  updatedUser.Id && x.Email == updatedUser.Email);
            if (userWithDifferentEmail != null)
            {
                return false;
            }

            var user = GetUserById(updatedUser.Id);
            mapper.Map(updatedUser, user);

            user.Address.Address1 = updatedUser.Address1;
            user.Address.Address2 = updatedUser.Address2;
            user.Address.City = updatedUser.City;
            user.Address.State = updatedUser.State;
            user.Address.ZipCode = updatedUser.ZipCode;

            context.SaveChanges();
            return true;
        }

        public void SaveUser(RegisterUserDto userToSave)
        {
            var user = GetUserById(userToSave.Id);
            mapper.Map(userToSave, user);

            context.SaveChanges();
        }

        public void ChangePassword(int userId, string password)
        {
            var userToSave = GetUserById(userId);
            SetPasword(userToSave, password);

            context.SaveChanges();
        }

        private void SetPasword(User user, string password)
        {
            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            user.Password = HashPassword(password, salt);
            user.PasswordSalt = Convert.ToBase64String(salt);
        }

        public void UpdateUsersSSN(int id, string ssn)
        {
            var user = GetUserById(id);
            if (!String.IsNullOrEmpty(ssn))
            {
                user.SSNKey = GenerateKey();
                user.SSN = EncryptText(ssn, user.SSNKey);
            } else
            {
                user.SSNKey = "";
                user.SSN = "";
            }

            context.SaveChanges();
        }

        public string GetUserSSN(int id)
        {
            var user = GetUserById(id);
            if (string.IsNullOrEmpty(user.SSN))
            {
                return "";
            }

            return DecryptText(user.SSN, user.SSNKey);
        }

        private string HashPassword(string password, byte[] salt) => Convert.ToBase64String(KeyDerivation.Pbkdf2(
                                                                                password: password,
                                                                                salt: salt,
                                                                                prf: KeyDerivationPrf.HMACSHA1,
                                                                                iterationCount: 10000,
                                                                                numBytesRequested: 256 / 8));

        private string GenerateJWTToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(configuration["JwtSecretKey"]);
            List<Claim> userRoles = new List<Claim>();
            user.UserInRoles.ToList().ForEach(u =>
            {
                userRoles.Add(new Claim(ClaimTypes.Role, u.Role.Name));
            });
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserId", user.Id.ToString()),
                    }),
                Expires = DateTime.UtcNow.AddDays(4),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Issuer = "AES",
                IssuedAt = DateTime.Now,
                Audience = "AES_USERS"
            };
            tokenDescriptor.Subject.AddClaims(userRoles);
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        private string GenerateKey()
        {
            var random = new RNGCryptoServiceProvider();
            byte[] salt = new byte[32];
            random.GetNonZeroBytes(salt);

            return Convert.ToBase64String(salt);
        }

        private string EncryptText(string input, string ssnKey)
        {
            // Get the bytes of the string
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
            // Hash the ssnKey with SHA256
            byte[] passwordBytes = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(ssnKey));
            byte[] bytesEncrypted = null;
            // The salt bytes must be at least 8 bytes. Private key
            byte[] saltBytes = new byte[] { 53, 59, 61, 67, 71, 73, 79, 83 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    bytesEncrypted = ms.ToArray();
                }
            }

            return Convert.ToBase64String(bytesEncrypted);
        }

        public static string DecryptText(string input, string ssnKey)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(ssnKey));
            byte[] bytesDecrypted = null;
            // The salt bytes must be at least 8 bytes. Private key.
            byte[] saltBytes = new byte[] { 53, 59, 61, 67, 71, 73, 79, 83 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    bytesDecrypted = ms.ToArray();
                }
            }

            return Encoding.UTF8.GetString(bytesDecrypted);
        }

    }
}
