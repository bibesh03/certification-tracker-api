﻿using CertificationTracker.Data;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using CertificationTracker.Models;

public class NotificationService : INotificationService
{
    private readonly DataContext context;
    private readonly IMapper mapper;

    public NotificationService(DataContext _context, IMapper _mapper)
    {
        this.context = _context;
        this.mapper = _mapper;
    }

    public List<Notification> GetNotifications(int userId) => context.Notifications.Where(x => x.UserId == userId).OrderByDescending(x => x.DateTime).ToList();

    public void SetNotificationAsRead(int notificationId)
    {
        var notification = context.Notifications.FirstOrDefault(x => x.Id == notificationId);
        notification.HasOpened = true;

        context.SaveChanges();
    }

    public void SaveNotification(Notification notification)
    {
        context.Notifications.Add(notification);
        context.SaveChanges();
    }
}