﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Report
{
    public class ReportService : IReportService
    {
        private readonly DataContext context;
        private readonly IPrintService printService;

        public ReportService(DataContext _context, IPrintService _printService)
        {
            this.context = _context;
            this.printService = _printService;
        }

        public byte[] GetProposalPrintOut(int proposalId)
        {
            var server = context.ReportServers.FirstOrDefault();
            if (server != null)
            {
                var report = context.Reports.FirstOrDefault(x => x.Name == "Proposal" && x.ReportServerId == server.Id);
                if (report != null)
                {
                    var url = string.Format("{0}{1}&proposalId={2}", server.Url.Trim(), report.ReportPath.Trim(), proposalId);

                    return printService.GetFileFromServer(url, server.UserName, server.Password);
                }
            }

            return null;
        }
    }
}
