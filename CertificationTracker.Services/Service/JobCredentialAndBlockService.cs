﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class JobCredentialAndBlockService : IJobCredentialAndBlockService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly IJobCredentialService jobCredentialService;

        public JobCredentialAndBlockService(DataContext _context, IMapper _mapper, IJobCredentialService _jobCredentialService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.jobCredentialService = _jobCredentialService;
        }

        public JobCredentialAndBlock GetJobCredentialAndBlockById(int id) => context.JobCredentialAndBlocks.Include(x => x.JobCredentialOrBlocks).FirstOrDefault(x => x.Id == id);

        public JobCredentialOrBlock GetJobCredentialOrBlockById(int id) => context.JobCredentialOrBlock.Include(x => x.JobCredentialAndBlock).ThenInclude(x => x.JobCredentialOrBlocks).FirstOrDefault(x => x.Id == id);

        public List<JobCredentialAndBlock> GetJobCredentialAndBlocks(int jobId) => context.JobCredentialAndBlocks.Include(x => x.JobCredentialOrBlocks).Where(x => x.JobId == jobId).ToList();

        public bool IsCredentialCurrentlyBeingUsed(int jobId, int jobCredentialId)
        {
            var credentialId = jobCredentialService.GetJobCredentialById(jobCredentialId).CredentialId;
            var jobCredentialAndBlocksWithCredentialIdCount = context.JobCredentialAndBlocks.Where(x => x.JobId == jobId && x.CredentialId == credentialId).Count();
            if (jobCredentialAndBlocksWithCredentialIdCount > 0)
            {
                return true;
            }

            var jobCredentialOrBlocksWithCredentialIdCount = context.JobCredentialAndBlocks.Include(x => x.JobCredentialOrBlocks).Where(x => x.JobId == jobId && x.CredentialId == null)
                .SelectMany(x => x.JobCredentialOrBlocks).Where(x => x.CredentialId == credentialId).Count();

            return jobCredentialOrBlocksWithCredentialIdCount > 0;
        }


        public List<JobCredentialAndBlock> SaveJobCredentialAndBlock(List<JobCredentialAndBlock> jobCredentialAndBlock)
        {
            jobCredentialAndBlock.ForEach(x =>
            {
                context.JobCredentialAndBlocks.Add(x);
            });

            context.SaveChanges();

            return jobCredentialAndBlock;
        }

        public void DeleteJobCredentialAndBlock(int id, bool isOrBlock)
        {
            if (isOrBlock)
            {
                var jobCredentialOrBlock = GetJobCredentialOrBlockById(id);
                if (jobCredentialOrBlock.JobCredentialAndBlock.JobCredentialOrBlocks.Count() == 1)
                {
                    context.JobCredentialAndBlocks.Remove(jobCredentialOrBlock.JobCredentialAndBlock);
                }
                else
                {
                    context.JobCredentialOrBlock.Remove(jobCredentialOrBlock);
                }
            }
            else
            {
                context.JobCredentialAndBlocks.Remove(GetJobCredentialAndBlockById(id));
            }

            context.SaveChanges();
        }
    }
}