﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class JobCredentialService : IJobCredentialService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public JobCredentialService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public JobCredential GetJobCredentialById(int id) => context.JobCredentials.FirstOrDefault(x => x.Id == id);

        public List<JobCredential> GetJobCredentials(int jobId) => context.JobCredentials.Where(x => x.JobId == jobId).ToList();

        public bool SaveJobCredential(List<JobCredential> jobCredentials)
        {
            var transaction = context.Database.BeginTransaction();
            try
            {
                context.JobCredentials.AddRange(jobCredentials);

                context.SaveChanges();
                transaction.Commit();
                
                return true;
            } catch (Exception ex)
            {
                transaction.Rollback();

                return false;
            }
            
        }

        public void DeleteJobCredential(int id)
        {
            context.JobCredentials.Remove(GetJobCredentialById(id));
            context.SaveChanges();
        }
    }
}
