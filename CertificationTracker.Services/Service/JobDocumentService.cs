﻿using CertificationTracker.Data;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class JobDocumentService : IJobDocumentService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public JobDocumentService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public JobDocument GetJobDocuments(int jobId) {
            var allactive = context.JobDocuments.Include(x => x.Children).Where(x => x.JobId == jobId && x.IsDeleted == false).ToList();
            var test = FilterDeleted(new JobDocument {
              Id = 99999,
              Name = "root",
              Children = allactive
            });

            return test;
        }//=> context.JobDocuments.Include(x => x.Children).Where(x => x.JobId == jobId && IsDeleted(x)).ToList();

        private JobDocument FilterDeleted(JobDocument document)
        {
            if (document.Children == null)
            {
                return document;
            }
            document.Children.RemoveAll(x => x.IsDeleted == true);
            foreach(var child in document.Children)
            {
                FilterDeleted(child);
            }

            return document;
        }

        public JobDocument GetJobDocumentById(int jobDocumentId) => context.JobDocuments.FirstOrDefault(x => x.Id == jobDocumentId);

        public void Create(JobDocument jobDocument)
        {
            context.JobDocuments.Add(jobDocument);

            context.SaveChanges();
        }

        public void Rename(int jobDocumnetId, string newName)
        {
            var jobDocument = GetJobDocumentById(jobDocumnetId);
            jobDocument.Name = newName;

            context.SaveChanges();
        }

        public void Delete(int jobDocumentId)
        {
            var jobDocument = GetJobDocumentById(jobDocumentId);
            jobDocument.IsDeleted = true;
            jobDocument.DeletedDateTime = DateTime.Now;

            context.SaveChanges();
        }
    }
}
