﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Job;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class JobService : IJobService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public JobService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public Job GetJobById(int id) => context.Jobs.Include(x => x.Proposal).ThenInclude(x => x.Client).FirstOrDefault(x => x.Id == id);

        public List<JobListingDto> GetAllJobs()
        {
            var jobs = context.Jobs.Include(x => x.Proposal).ThenInclude(x => x.Client).ToList();

            return mapper.Map<List<JobListingDto>>(jobs);
        }

        public void SaveJob(Job job)
        {
            if (job.Id > 0)
            {
                var savedJob = context.Jobs.FirstOrDefault(x => x.Id == job.Id);
                mapper.Map(job, savedJob);
            }
            else
            {
                context.Jobs.Add(job);
            }

            context.SaveChanges();
        }
    }
}
