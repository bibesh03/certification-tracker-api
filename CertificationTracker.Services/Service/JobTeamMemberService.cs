﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Job;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class JobTeamMemberService : IJobTeamMemberService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly IJobCredentialAndBlockService jobCredentialAndBlockService;

        public JobTeamMemberService(DataContext _context, IMapper _mapper, IJobCredentialAndBlockService _jobCredentialAndBlockService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.jobCredentialAndBlockService = _jobCredentialAndBlockService;
        }

        public JobTeamMember GetJobTeamMemberById(int jobTeamMemberId) => context.JobTeamMembers.FirstOrDefault(x => x.Id == jobTeamMemberId);

        public List<JobTeamMemberListingDto> GetJobTeamMembers(int jobId)
        {
            var jobTeamMembers = context.JobTeamMembers.Where(x => x.JobId == jobId).Include(x => x.User).ToList();

            return mapper.Map<List<JobTeamMemberListingDto>>(jobTeamMembers);
        }

        public bool SaveJobTeamMember(JobTeamMember jobTeamMember)
        {
            try
            {
                if (jobTeamMember.Id > 0)
                {
                    var savedJobTeamMember = context.JobTeamMembers.FirstOrDefault(x => x.Id == jobTeamMember.Id);
                    mapper.Map(jobTeamMember, savedJobTeamMember);
                }
                else
                {
                    context.JobTeamMembers.Add(jobTeamMember);
                }

                context.SaveChanges();
                return true;
            } catch (Exception ex)
            {
                return false;
            }
        }

        public void DeleteTeamMember(int jobTeamMemberId)
        {
            var teamMember = context.JobTeamMembers.FirstOrDefault(x => x.Id == jobTeamMemberId);
            context.JobTeamMembers.Remove(teamMember);

            context.SaveChanges();
        }

        public bool DoesFormLeadExist(int jobId) => context.JobTeamMembers.Count(x => x.IsFormLead == true && x.JobId == jobId) > 0;

        public List<JobTeamMemberOptionDto> GetAvailableUsersForTeamMember(int jobId)
        {
            var jobCredentialBlocks = jobCredentialAndBlockService.GetJobCredentialAndBlocks(jobId);
            var availableUserIds = ExecuteSqlToGetAvailableUsersForJob(jobCredentialBlocks, jobId);

            var users = context.Users.Include(x => x.UserInRoles).ThenInclude(x => x.Role).Where(x => availableUserIds.Contains(x.Id) && x.IsActive).ToList();
            var options = new List<JobTeamMemberOptionDto>();

            foreach(var user in users)
            {
                options.Add(new JobTeamMemberOptionDto
                {
                    User = new OptionsDto
                    {
                        Label = user.FirstName + " " + user.LastName,
                        Value = user.Id + ""
                    },
                    RoleOptions = user.UserInRoles.Where(x => x.Role.Name != "Admin").Select(x => new OptionsDto
                    {
                        Label = x.Role.Name,
                        Value = x.Role.Name
                    }).ToList()
                });
            };

            return options;
        }

        private int[] ExecuteSqlToGetAvailableUsersForJob(List<JobCredentialAndBlock> jobCredentialBlocks, int jobId)
        {
            var sqlString = "WITH ALLAVAILABLEUSERS AS (SELECT UserId as Ids FROM UserCredentials WHERE ";
            var index = 0;

            foreach (var credentialBlock in jobCredentialBlocks)
            {
                if (credentialBlock.CredentialId != null)
                {
                    sqlString += "CredentialId IN (" + credentialBlock.CredentialId + ") ";
                }
                else
                {
                    sqlString += "CredentialId IN (" + String.Join(",", credentialBlock.JobCredentialOrBlocks.Select(x => x.CredentialId)) + ") ";
                }

                if (index != jobCredentialBlocks.Count() - 1)
                {
                    sqlString += "OR "; //or represents AND
                }
                index++;
            }

            sqlString += "AND IsArchived = 0 GROUP BY UserId HAVING COUNT(*) >= " + jobCredentialBlocks.Count() + ") ";
            sqlString += "SELECT ALLAVAILABLEUSERS.Ids from ALLAVAILABLEUSERS WHERE IDS NOT in (SELECT UserId FROM [JobTeamMembers] WHERE JobId = " + jobId + ")";
            var userIdsFromCredentialSearch = context.UserIdsFromCredentialSearches.FromSqlRaw(sqlString).ToList();
            
            return userIdsFromCredentialSearch.Select(x => x.Ids).ToArray();
        }
    }
}
