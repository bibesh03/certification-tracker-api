﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Proposal;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class ProposalAttachmentService : IProposalAttachmentService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ProposalAttachmentService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ProposalAttachment GetProposalAttachmentById(int id) => context.ProposalAttachments.Include(x => x.DeletedBy).Include(x => x.UploadedBy).FirstOrDefault(x => x.Id == id);

        public List<ProposalAttachmentListingDto> GetProposalAttachments(int proposalId)
        {
            var attachments = context.ProposalAttachments.Include(x => x.UploadedBy).Where(x => x.ProposalId == proposalId && x.DeletedById == null).ToList();

            return mapper.Map<List<ProposalAttachmentListingDto>>(attachments);
        }

        public void SaveProposalAttachment(ProposalAttachmentDto proposalAttachment, string filePath)
        {
            var proposalAttachmentModel = mapper.Map<ProposalAttachment>(proposalAttachment);
            if (!String.IsNullOrEmpty(filePath))
            {
                proposalAttachmentModel.FileLocation = filePath;
            }
            if (proposalAttachmentModel.Id > 0)
            {
                var savedProposalAttachment = GetProposalAttachmentById(proposalAttachmentModel.Id);
                savedProposalAttachment.Name = proposalAttachment.Name;
                savedProposalAttachment.Description = proposalAttachment.Description;
                savedProposalAttachment.SendToClient = proposalAttachment.SendToClient;
            }
            else
            {
                context.ProposalAttachments.Add(proposalAttachmentModel);
            }

            context.SaveChanges();
        }

        public void DeleteProposalAttachment(int proposalAttachmentId, int userId)
        {
            var savedProposalAttachment = GetProposalAttachmentById(proposalAttachmentId);
            savedProposalAttachment.DeletedById = userId;
            savedProposalAttachment.DeletedDateTime = DateTime.Now;

            context.SaveChanges();
        }
    }
}
