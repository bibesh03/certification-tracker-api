﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Proposal;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class ProposalContactLogService : IProposalContactLogService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ProposalContactLogService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ProposalContactLog GetProposalContactLogById(int id) => context.ProposalContactLogs.FirstOrDefault(x => x.Id == id);

        public List<ProposalContactLogDto> GetProposalContactLogs(int proposalId) {
            var contactLogs = context.ProposalContactLogs.Include(x => x.User).ThenInclude(x => x.UserInRoles).ThenInclude(x => x.Role).Where(x => x.ProposalId == proposalId).OrderByDescending(x => x.CreatedDate).ToList();
           
            return mapper.Map<List<ProposalContactLogDto>>(contactLogs);
        }

        public void SaveProposalContactLog(ProposalContactLog proposal)
        {
            if (proposal.Id > 0)
            {
                var savedProposal = GetProposalContactLogById(proposal.Id);
                mapper.Map(proposal, savedProposal);
            }
            else
            {
                context.ProposalContactLogs.Add(proposal);
            }

            context.SaveChanges();
        }
    }
}
