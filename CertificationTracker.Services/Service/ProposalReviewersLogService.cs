﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Proposal;
using CertificationTracker.Interface.Management;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class ProposalReviewersLogService : IProposalReviewersLogService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;

        public ProposalReviewersLogService(DataContext _context, IMapper _mapper)
        {
            this.context = _context;
            this.mapper = _mapper;
        }

        public ProposalMessageInfoDto GetMessages(int proposalId, int length)
        {
            var messages = context.ProposalReviewersLogs.Include(x => x.User).Where(x => x.ProposalId == proposalId);

            return new ProposalMessageInfoDto
            {
                HasMore = messages.Count() > (length + 20),
                ProposalMessages = mapper.Map<List<ProposalMessageDto>>(messages.OrderByDescending(x => x.DateTime).Take(length + 20).Reverse())
            };
        }

        public bool SaveMessage(ProposalMessageDto messageDto)
        {
            try
            {
                var message = mapper.Map<ProposalReviewersLog>(messageDto);
                message.DateTime = DateTime.Now;
                context.ProposalReviewersLogs.Add(message);

                context.SaveChanges();

                return true;
            } catch (Exception ex)
            {
                return false;
            }
        }
    }
}
