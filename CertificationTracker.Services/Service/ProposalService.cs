﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Proposal;
using CertificationTracker.Interface.Management;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services.Service
{
    public class ProposalService : IProposalService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly IProposalReviewersLogService proposalReviewersLogService;
        private readonly ISettingService settingService;
        private readonly IUserService userService;
        private readonly INotificationService notificationService;

        public ProposalService(DataContext _context, IMapper _mapper, IProposalReviewersLogService _proposalReviewersLogService, ISettingService _settingService, IUserService _userService, INotificationService _notificationService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.proposalReviewersLogService = _proposalReviewersLogService;
            this.settingService = _settingService;
            this.userService = _userService;
            this.notificationService = _notificationService;
        }

        public ProposalManageCardDto GetProposalManageInfo(int proposalId)
        {
            var proposal= context.Proposals.Include(x => x.CreatedByUser).Include(x => x.UpdatedByUser).Include(x => x.SentByUser).FirstOrDefault(x => x.Id == proposalId);

            return mapper.Map<ProposalManageCardDto>(proposal);
        }

        public string GetProjectNumber(int proposalId) => context.Proposals.FirstOrDefault(x => x.Id == proposalId).ProjectNumber;

        public List<ProposalLogDto> GetProposalProjectLogs()
        {
            var proposals = context.Proposals.Include(x => x.Client).Include(x => x.Job).Include(x => x.ProjectType)
                .Include(x => x.ProjectSubType).Include(x => x.ProposalProjects).ThenInclude(x => x.Project).Include(x => x.ReferralSource).Where(x => x.IsDeleted == false).ToList();

            return mapper.Map<List<ProposalLogDto>>(proposals);
        }

        public Proposal GetProposalById(int id) => context.Proposals.Include(x => x.ProposalProjectLocations).Include(x => x.ScopeOfWorks).Include(x => x.ScheduleOfCharges).Include(x => x.ProposalProjects).Include(x => x.ProposalReviewers).FirstOrDefault(x => x.Id == id);

        public List<Proposal> GetProposals(string status, bool isAdmin, int userId)
        {
            IQueryable<Proposal> query = context.Proposals.Include(x => x.ProjectType)
                .Include(x => x.ProjectSubType).Include(x => x.CreatedByUser).Include(x => x.UpdatedByUser)
                .Include(x => x.Client).Include(x => x.ProposalReviewers).Include(x => x.Location).ThenInclude(x => x.Address).Where(x => x.IsDeleted == false);
            
            switch (status.ToUpper())
            {
                case "UNASSIGNED":
                    query = query.Where(x => x.ProposalReviewers.Count() == 0 && x.SentToClientDateTime == null);
                    break;
                case "PENDING-REVIEW":
                    query = query.Where(x => x.ProposalReviewers.Count() > 0 && x.SentToClientDateTime == null);
                    break;
                default:
                    query = query.Where(x => x.SentToClientDateTime != null);
                    break;
                //case "APPROVED":
                //    query = query.Where(x => x.SentToClientDateTime != null && x.RejectionReason == null && x.HasApproved == true);
                //    break;
                //case "REJECTED":
                //    query = query.Where(x => x.SentToClientDateTime != null && x.RejectionReason != null && x.HasApproved == false);
                //    break;
                //default:
                //    query = query.Where(x => x.SentToClientDateTime != null && x.HasApproved == null);
                //    break;
            }

            if (isAdmin)
            {
                return query.ToList();
            }

            return query.Where(x => x.CreatedBy == userId || x.ProposalReviewers.Select(x => x.UserId).Contains(userId)).ToList();
        }

        public void SaveProposal(Proposal proposal)
        {
            var isNew = false;
            if (proposal.Id > 0)
            {
                var savedProposal = GetProposalById(proposal.Id);
                mapper.Map(proposal, savedProposal);
            }
            else
            {
                isNew = true;
                SetProposalNumberAndContent(proposal);
                context.Proposals.Add(proposal);
            }

            context.SaveChanges();

            if (isNew)
            {
                var user = context.Users.FirstOrDefault(x => x.Id == proposal.CreatedBy);
                proposalReviewersLogService.SaveMessage(new ProposalMessageDto
                {
                    Message = user.FirstName + " " + user.LastName + " created the proposal.",
                    ProposalId = proposal.Id
                });
            }
        }

        private void SetProposalNumberAndContent(Proposal proposal)
        {
            var location = context.Locations.Include(x => x.Address).FirstOrDefault(x => x.Id == proposal.LocationId);
            proposal.ProjectNumber = DateTime.Now.ToString("yy") + "-" + (context.Proposals.Count() + 1 + "").PadLeft(4, '0') + "-" + "01";
            Dictionary<string, string> proposalHeaderMapper = new Dictionary<string, string>() {
                { "##CURRENT_DATE##", DateTime.Now.ToString("dd/MM/yyyy") },
                { "##CLIENT_CONTACT_NAME##", proposal.ContactName },
                { "##CLIENT_LOCATION_BUSINESS_NAME##", location.BusinessName},
                { "##CLIENT_LOCATION_STREET_ADDRESS##", location.Address.Address1 },
                { "##CLIENT_LOCATION_CITY##", location.Address.City },
                { "##CLIENT_LOCATION_STATE##", location.Address.State },
                { "##CLIENT_LOCATION_ZIPCODE##", location.Address.ZipCode },
                { "##PROJECT_NAME##", proposal.ProjectName },
                { "##PROJECT_NUMBER##", proposal.ProjectNumber },
                { "##PROJECT_LOCATIONS##", String.Join("; ", proposal.ProposalProjectLocations.Select(x => x.ProjectLocation).ToArray())} //multiple Locations
            };

            var headerText = settingService.GetSettingByKey("Proposal Header");
            foreach (KeyValuePair<string, string> entry in proposalHeaderMapper)
            {
                headerText = headerText.Replace(entry.Key, entry.Value);
            }

            proposal.ProposalHeader = headerText;

            var scheduleOfChargesText = settingService.GetSettingByKey("Schedule And Charges");
            var originatorUser = userService.GetUserById(proposal.CreatedBy ?? 0);
            Dictionary<string, string> scheduleOfChargesMapper = new Dictionary<string, string>() {
                { "##ORIGNIATOR_EMAIL##", originatorUser.FirstName + " " + originatorUser.LastName },
            };

            foreach (KeyValuePair<string, string> entry in scheduleOfChargesMapper)
            {
                scheduleOfChargesText = headerText.Replace(entry.Key, entry.Value);
            }

            proposal.ScheduleAndCharges = scheduleOfChargesText;
        }

        public void DeleteProposal(int id, int userId)
        {
           var proposal = GetProposalById(id);
            proposal.IsDeleted = true;
            proposal.DeletedBy = userId;
            proposal.DeletedDateTime = DateTime.Now;

            context.SaveChanges();
        }

        public List<ProposalReviewer> GetProposalReviewers(int proposalId) => context.ProposalReviewers.Where(x => x.ProposalId == proposalId).ToList();

        public Dictionary<string, string> GetProposalPermission(int proposalId, int userId)
        {
            var proposal = GetProposalById(proposalId);
            Dictionary<string, string> roleHasReviewed = new Dictionary<string, string>();
            if (proposal.CreatedBy == userId)
            {
                roleHasReviewed.Add("Role", "Originator");
                roleHasReviewed.Add("HasReviewed", "False");
                roleHasReviewed.Add("DateReviewed", "N/A");

                return roleHasReviewed;
            } 

            var reviewer = context.ProposalReviewers.FirstOrDefault(x => x.ProposalId == proposalId && x.UserId == userId);
            roleHasReviewed.Add("Role", (reviewer != null) ? "Reviewer" : "NONE");
            roleHasReviewed.Add("HasReviewed", (reviewer != null) ? reviewer.IsReviewed.ToString() : (false).ToString());
            roleHasReviewed.Add("DateReviewed", (reviewer == null) ? "N/A" : reviewer.DateReviewed == null ? "N/A" : reviewer.DateReviewed.Value.ToString("MM/dd/yyyy hh:mm tt"));

            return roleHasReviewed;
        }

        public List<ProposalReviewerHistoryDto> GetReviewersHistory(int proposalId)
        {
            var reviewers = context.ProposalReviewers.Include(x => x.User).Where(x => x.ProposalId == proposalId).ToList();

            return mapper.Map<List<ProposalReviewerHistoryDto>>(reviewers);
        }

        public List<Notification> AddExtension(int proposalId, int noOfHours)
        {
            var proposalReviewers = context.ProposalReviewers.Include(x => x.Proposal).Where(x => x.ProposalId == proposalId).ToList();
            var noOfHoursToExtend = DateTime.Now.AddHours(noOfHours);
            List<Notification> notifications = new List<Notification>();
            proposalReviewers.ForEach(x =>
            {
                x.ReviewDeadLineDateTime = noOfHoursToExtend;

                var notificationToSave = new Notification
                {
                    DateTime = DateTime.Now,
                    Message = "The deadline for proposal with project # "  + x.Proposal.ProjectNumber + " has been extendend. The new deadline is " + noOfHoursToExtend.ToString("MM/dd/yyyy hh:mm:ss tt"),
                    UserId = x.UserId,
                    Type = "PRIMARY",
                    Link = "/services/manage-proposal/general-information/" + x.Proposal.Id,
                };

                notificationService.SaveNotification(notificationToSave);
                notifications.Add(notificationToSave);
            });

            context.SaveChanges();

            return notifications;
        }

        public ProposalNotyNotificationDto SaveProposalReviewers(ProposalReviewerDto reviewers, int userId)
        {
            var savedReviewers = context.ProposalReviewers.Where(x => x.ProposalId == reviewers.Id).ToList();
            var reviewDeadLine = DateTime.Now.AddHours(reviewers.NoOfHoursForDueDate);
            var loggedInUser = userService.GetUserById(userId);
            var proposalNotyNotificationDto = new ProposalNotyNotificationDto {
                ChatNoty = new List<string>(),
                Notifications = new List<Notification>()
            };

            if (savedReviewers.Count > 0)
            {
                reviewDeadLine = savedReviewers.FirstOrDefault().ReviewDeadLineDateTime;
                DeleteReviewers(savedReviewers, reviewers, proposalNotyNotificationDto, loggedInUser.FirstName + " " + loggedInUser.LastName);
            }
            
            var newUserToBeAdded = reviewers.UserIdForReview.Except(savedReviewers.Select(x => x.UserId)).ToArray();
            for (int i = 0; i < newUserToBeAdded.Length; i++)
            {
                context.ProposalReviewers.Add(new ProposalReviewer
                {
                    AssignedDateTime = DateTime.Now,
                    ProposalId = reviewers.Id,
                    ReviewDeadLineDateTime = reviewDeadLine,
                    UserId = newUserToBeAdded[i]
                });
                proposalNotyNotificationDto.ChatNoty.Add(SaveSystemLog(newUserToBeAdded[i], reviewers.Id, " has joined the thread"));
                var notificationToSave = new Notification
                {
                    DateTime = DateTime.Now,
                    Link = "/services/manage-proposal/general-information/" + reviewers.Id,
                    Message = loggedInUser.FirstName + " " + loggedInUser.LastName + " added you as a reviewer for proposal #" + reviewers.ProjectNumber,
                    UserId = newUserToBeAdded[i],
                    Type = "SUCCESS"
                };
                notificationService.SaveNotification(notificationToSave);
                proposalNotyNotificationDto.Notifications.Add(notificationToSave);
            }

            context.SaveChanges();
            return proposalNotyNotificationDto;
        }

        private void DeleteReviewers(List<ProposalReviewer> dbUsers, ProposalReviewerDto proposalReviewerDto, ProposalNotyNotificationDto proposalNotyNotificationDto, string deletedByUser)
        {
            var usersToBeDeleted = dbUsers.Select(x => x.UserId).Except(proposalReviewerDto.UserIdForReview).ToArray();

            for (int i = 0; i < usersToBeDeleted.Length; i++)
            {
                var userToBeDeleted = context.ProposalReviewers.FirstOrDefault(x => x.UserId == usersToBeDeleted[i] && x.ProposalId == proposalReviewerDto.Id);
                proposalNotyNotificationDto.ChatNoty.Add(SaveSystemLog(usersToBeDeleted[i], proposalReviewerDto.Id, "  has been removed from the thread"));

                var notificationToSave = new Notification
                {
                    DateTime = DateTime.Now,
                    Message = deletedByUser + " has removed you as reviewer from proposal with project #" + proposalReviewerDto.ProjectNumber,
                    UserId = usersToBeDeleted[i],
                    Type = "DANGER"
                };

                notificationService.SaveNotification(notificationToSave);
                proposalNotyNotificationDto.Notifications.Add(notificationToSave);

                context.Remove(userToBeDeleted);
            }
        }

        public ProposalSendToClientDto GetProposalSendInfo(int proposalId)
        {
            var proposal = context.Proposals.Include(x => x.ProposalAttachments.Where(x => x.SendToClient == true && x.DeletedDateTime == null)).FirstOrDefault(x => x.Id == proposalId);

            if (proposal.SentToClientDateTime != null)
            {
                return null;
            }

            var proposalSendToClientDto = new ProposalSendToClientDto {
                Emails = new List<string>() { proposal.ContactEmail },
                ProposalFiles = new List<ProposalFile>()
            };

            proposal.AdditionalEmails.Split(",").ToList().ForEach(x =>
            {
                if (!String.IsNullOrEmpty(x))
                {
                    proposalSendToClientDto.Emails.Add(x);
                }
            });

            var filesToSend = proposal.ProposalAttachments.Select(x => new ProposalFile
            {
                Name = x.Name,
                Path = x.FileLocation
            }).ToList();

            if (filesToSend.Count > 0)
            {
                proposalSendToClientDto.ProposalFiles = filesToSend;
            }

            return proposalSendToClientDto;
        }

        public ProposalNotyNotificationDto SetProposalAsReviewed(int proposalId, int userId)
        {
            var reviewerForProposal = context.ProposalReviewers.Include(x => x.Proposal).FirstOrDefault(x => x.UserId == userId && x.ProposalId == proposalId);
            reviewerForProposal.DateReviewed = DateTime.Now;
            reviewerForProposal.IsReviewed = true;
            var loggedInUser = userService.GetUserById(userId);

            ProposalNotyNotificationDto proposalNotyNotificationDto = new ProposalNotyNotificationDto
            {
                ChatNoty = new List<string>(),
                Notifications = new List<Notification>()
            };

            var notificationToSend = new Notification
            {
                DateTime = DateTime.Now,
                Link = "",
                Message = loggedInUser.FirstName + " " +loggedInUser.LastName + " has marked the proposal with project number #" + reviewerForProposal.Proposal.ProjectNumber + " as reviewed." ,
                Type = "PRIMARY",
                UserId = reviewerForProposal.Proposal.CreatedBy ?? 0
            };
            notificationService.SaveNotification(notificationToSend);
            context.SaveChanges();

            proposalNotyNotificationDto.ChatNoty.Add(SaveSystemLog(userId, proposalId, " has reviewed the proposal."));
            proposalNotyNotificationDto.Notifications.Add(notificationToSend);

            return proposalNotyNotificationDto;
        }

        private string SaveSystemLog(int userId, int proposalId, string noty)
        {
            var user = context.Users.FirstOrDefault(x => x.Id == userId);
            string message = (user.FirstName + " " + user.LastName) + noty;
            proposalReviewersLogService.SaveMessage(new ProposalMessageDto
            {
                Message = message,
                ProposalId = proposalId
            });

            return message;
        }

        public ProposalEstimatorDto GetProposalEstimator(int proposalId)
        {
            var estimatorDetails = context.Proposals.Include(x => x.ProposalEstimatedConsolidatedCharges).FirstOrDefault();

            return mapper.Map<ProposalEstimatorDto>(estimatorDetails);
        }

        public void SaveProposalEstimator(ProposalEstimatorDto proposalEstimatorDto)
        {
            var proposal = context.Proposals.Include(x => x.ProposalEstimatedConsolidatedCharges).FirstOrDefault(x => x.Id == proposalEstimatorDto.Id);
            mapper.Map(proposalEstimatorDto, proposal);

            context.SaveChanges();
        }

        public ProposalNotyNotificationDto SendProposalToClient(int proposalId, int userId)
        {
            var proposal = context.Proposals.Include(x => x.ProposalReviewers).FirstOrDefault(x => x.Id == proposalId);
            proposal.SentByUserId = userId;
            proposal.SentToClientDateTime = DateTime.Now;
            var sentByUser = userService.GetUserById(userId);
            ProposalNotyNotificationDto proposalNotyNotificationDto = new ProposalNotyNotificationDto
            {
                ChatNoty = new List<string>(),
                Notifications = new List<Notification>()
            };

            proposal.ProposalReviewers.ToList().ForEach(x =>
            {
                var notificationToSave = new Notification
                {
                    DateTime = DateTime.Now,
                    Message = sentByUser.FirstName + " " + sentByUser.LastName + " has submitted the proposal with project #" + proposal.ProjectNumber + " to the client",
                    UserId = x.UserId,
                    Type = "SUCCESS",
                    Link = "/services/manage-proposal/general-information/" + proposalId,
                };
                proposalNotyNotificationDto.Notifications.Add(notificationToSave);
                notificationService.SaveNotification(notificationToSave);
            });

            context.SaveChanges();

            proposalNotyNotificationDto.ChatNoty.Add(SaveSystemLog(userId, proposalId, " has submitted the proposal to client"));

            return proposalNotyNotificationDto;
        }

        public List<Notification> RejectProposal(int proposalId, int loggedInUserId, string rejectionReason)
        {
            var proposal = GetProposalById(proposalId);
            proposal.RejectionReason = rejectionReason;
            proposal.ApprovedOrRejectedDateTime = DateTime.Now;
            proposal.ApprovedOrRejectedByUserId = loggedInUserId;
            proposal.HasApproved = false;
            SaveNotifications(proposal);

            context.SaveChanges();

            return SaveNotifications(proposal);
        }

        public List<Notification> ApproveProposal(int proposalId, int loggedInUserId)
        {
            var proposal = GetProposalById(proposalId);
            proposal.ApprovedOrRejectedDateTime = DateTime.Now;
            proposal.ApprovedOrRejectedByUserId = loggedInUserId;
            proposal.HasApproved = true;
            proposal.Job = new Job()
            {
                ProposalId = proposalId
            };

            context.SaveChanges();
            return SaveNotifications(proposal);
        }

        private List<Notification> SaveNotifications(Proposal proposal)
        {
            List<Notification> notifications = new List<Notification>();
            var loggedInUser = userService.GetUserById(proposal.ApprovedOrRejectedByUserId ?? 0);

            var message = loggedInUser.FirstName + " " + loggedInUser.LastName + " has " + ((bool)proposal.HasApproved ? "approved" : "rejected") + " the proposal with project number #" + proposal.ProjectNumber + (((bool)proposal.HasApproved) ? ". Click to view the job" : "");
            var type = (bool)proposal.HasApproved ? "SUCCESS" : "DANGER";
            var link = (bool)proposal.HasApproved ? "/services/manage-job/general-information/" + proposal.Job.Id : "/services/proposals";

            proposal.ProposalReviewers.ToList().ForEach(x =>
            {
                var notificationToSend = new Notification
                {
                    DateTime = DateTime.Now,
                    Link = link,
                    Message = message,
                    Type = type,
                    UserId = x.UserId
                };

                notificationService.SaveNotification(notificationToSend);
                notifications.Add(notificationToSend);
            });

            return notifications;
        }
    }
}
