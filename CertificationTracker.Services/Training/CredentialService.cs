﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Credential;
using CertificationTracker.Interface;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services
{
    public class CredentialService : ICredentialService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly IUserCredentialService userCredentialService;

        public CredentialService(DataContext _context, IMapper _mapper, IUserCredentialService _userCredentialService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.userCredentialService = _userCredentialService;
        }

        public Credential GetCredentialById(int id) => context.Credentials.Include(x => x.TrainingType).Include(x => x.TrainingType).Include(x => x.TrainingSubType).Include(x => x.IntervalType).FirstOrDefault(x => x.Id == id);

        public Credential GetCredentialByName(string name) => context.Credentials.FirstOrDefault(x => x.Name == name);

        public List<CredentialListingDto> GetCredentials(bool showArchived)
        {
            var credentials = context.Credentials.Include(x => x.IntervalType).Include(x => x.TrainingType).Include(x => x.TrainingSubType).OrderBy(x => x.Name).Where(x => x.IsArchived == showArchived).ToList();

            return mapper.Map<List<CredentialListingDto>>(credentials);
        }

        public List<CredentialListingDto> GetCredentialsByTypeAndSubType(int typeId, int? subTypeId)
        {
            var credentials = context.Credentials.Include(x => x.IntervalType).Include(x => x.TrainingType).Include(x => x.TrainingSubType).OrderBy(x => x.Name)
                .Where(x => x.TrainingTypeId == typeId && x.TrainingSubTypeId == subTypeId && x.IsArchived == false);

            return mapper.Map<List<CredentialListingDto>>(credentials);
        }

        public void SaveCredential(Credential credential)
        {
            if (credential.Id > 0)
            {
                var savedCredential = context.Credentials.FirstOrDefault(x => x.Id == credential.Id);
                mapper.Map(credential, savedCredential);
            }
            else
            {
                context.Credentials.Add(credential);
            }

            context.SaveChanges();
        }
        
        public bool UnArchiveCredential(int id)
        {
            var savedCredential = GetCredentialById(id);
            if (savedCredential.TrainingSubType == null)
            {
                if (savedCredential.TrainingType.IsArchived)
                {
                    return false;
                }
            }

            if (savedCredential.TrainingSubType.IsArchived)
            {
                return false;
            }

            savedCredential.IsArchived = false;

            context.SaveChanges();
            return true;
        }

        public void ArchiveCredential(int id)
        {
            var savedCredential = GetCredentialById(id);
            ArchiveCredentialAndChild(savedCredential);
        }

        public void SetAllCredentialToArchivedByType(int typeId)
        {
            var allCredentialsByType = context.Credentials.Where(x => x.TrainingTypeId == typeId && x.TrainingSubTypeId == null).ToList();
            allCredentialsByType.ForEach(credential =>
            {
                ArchiveCredentialAndChild(credential);
            });
        }

        public void SetAllCredentialToArchivedBySubType(int subTypeId)
        {
            var allCredentialsBySubType = context.Credentials.Where(x => x.TrainingSubTypeId == subTypeId).ToList();
            allCredentialsBySubType.ForEach(credential =>
            {
                ArchiveCredentialAndChild(credential);
            });
        }

        private void ArchiveCredentialAndChild(Credential credential)
        {
            userCredentialService.SetAllUserCredentialToArchived(credential.Id);
            credential.IsArchived = true;

            context.SaveChanges();
        }
    }
}
