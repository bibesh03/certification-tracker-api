﻿using CertificationTracker.Data;
using CertificationTracker.Dto.Credential;
using CertificationTracker.Interface;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CertificationTracker.Services
{
    public class UserCredentialService : IUserCredentialService
    {
        private readonly DataContext context;
        private readonly IMapper mapper;
        private readonly IJobCredentialAndBlockService jobCredentialAndBlockService;

        public UserCredentialService(DataContext _context, IMapper _mapper, IJobCredentialAndBlockService _jobCredentialAndBlockService)
        {
            this.context = _context;
            this.mapper = _mapper;
            this.jobCredentialAndBlockService = _jobCredentialAndBlockService;
        }

        public UserCredential GetUserCredentialById(int id) => context.UserCredentials.Include(x => x.Credential).Include(x => x.UserCredentialAttachments).FirstOrDefault(x => x.Id == id);

        public List<UserCredentialListingDto> GetUserCredentialExpiringInNextXDays(int days)
        {
            var userCredentials = context.UserCredentials.Include(x => x.Credential).ThenInclude(x => x.TrainingType).Include(x => x.User).Where(x => x.ExpiryDate <= DateTime.Now.AddDays(days) && x.ExpiryDate >= DateTime.Now).OrderBy(x => x.User.FirstName).ToList();
            return mapper.Map<List<UserCredentialListingDto>>(userCredentials);
        }

        public List<UserCredentialListingDto> GetUserCredentialExpiredInLastXDays(int days)
        {
            var userCredentials = context.UserCredentials.Include(x => x.Credential).ThenInclude(x => x.TrainingType).Include(x => x.User).Where(x => x.ExpiryDate >= DateTime.Now.AddDays(-days) && x.ExpiryDate < DateTime.Now && x.IsArchived == false).OrderBy(x => x.User.FirstName).ToList();
            return mapper.Map<List<UserCredentialListingDto>>(userCredentials); 
        }

        public List<UserCredentialListingDto> GetUserCredentials(bool showArchived)
        {
            var userCredentials = context.UserCredentials.Include(x => x.Credential).ThenInclude(x => x.TrainingType)
                                    .Include(x => x.Credential).ThenInclude(x => x.TrainingSubType).Include(x => x.User).Where(x => x.IsArchived == showArchived).ToList();

            return mapper.Map<List<UserCredentialListingDto>>(userCredentials);
        }

        public List<UserCredentialListingDto> GetUserCredentialByUserId(int userId, bool showArchived)
        {
            var userCredentials = context.UserCredentials.Include(x => x.Credential).ThenInclude(x => x.TrainingType)
                .Include(x => x.Credential).ThenInclude(x => x.TrainingSubType)
                .Include(x => x.User).Where(x => x.UserId == userId && x.IsArchived == showArchived);

            return mapper.Map<List<UserCredentialListingDto>>(userCredentials);
        }

        public void SetAllUserCredentialToArchived(int credentialId)
        {
            var userCredentials = context.UserCredentials.Where(x => x.CredentialId == credentialId).ToList();
            userCredentials.ForEach(userCert =>
            {
                userCert.IsArchived = true;
                userCert.ArchivedDateTime = DateTime.Now;
            });

            context.SaveChanges();
        }

        public UserCredential CheckDuplicateUserCredential(int userId, int credentialId, int? userCredentialId)
        {
            var query = context.UserCredentials.Where(x => x.UserId == userId && x.CredentialId == credentialId && x.IsArchived == false);
            if (userCredentialId != null)
            {
                query = query.Where(x => x.Id != userCredentialId);
            }
            var test = query.ToList();
            return query.FirstOrDefault();
        }


        public bool? ToogleUserCredentialArchive(int userCredentialId)
        {
            var userCredential = GetUserCredentialById(userCredentialId);
            if (userCredential.IsArchived)
            {
                if (userCredential.Credential.IsArchived)
                {
                    return null;
                }
                userCredential.IsArchived = false;
            } else
            {
                userCredential.IsArchived = true;
                userCredential.ArchivedDateTime = DateTime.Now;
            }

            context.SaveChanges();

            return userCredential.IsArchived;
        }

        public void MapAll()
        {
            var allCredentials = context.UserCredentials.Where(x => x.ImageUrl != null).ToList();
            allCredentials.ForEach(x =>
            {
                var attachment = new UserCredentialAttachment
                {
                    IsDeleted = false,
                    FileExtension = System.IO.Path.GetExtension(x.ImageUrl),
                    FileName = "Credential",
                    FilePath = x.ImageUrl,
                    UploadedDateTime = DateTime.Now,
                    UserCredentialId = x.Id,
                };
                context.UserCredentialAttachments.Add(attachment);
            });

            context.SaveChanges();
        }

        public void SaveUserCredential(UserCredentialDetailDto userCredentialDetail)
        {
            var userCredential = mapper.Map<UserCredential>(userCredentialDetail);

            if (userCredential.Id > 0)
            {
                var savedUserCredential = context.UserCredentials.FirstOrDefault(x => x.Id == userCredential.Id);
                mapper.Map(userCredential, savedUserCredential);
            }
            else
            {
                context.UserCredentials.Add(userCredential);
            }
            context.SaveChanges();
        }

        public void SaveAttachments(UserCredentialDetailDto userCredentialDetail)
        {
            var userCredential = GetUserCredentialById(userCredentialDetail.Id);
            if (userCredential.UserCredentialAttachments == null)
            {
                userCredentialDetail.UserCredentialAttachments = new List<UserCredentialAttachment>();
            }

            userCredentialDetail.UserCredentialAttachments.ForEach(file =>
            {
                userCredential.UserCredentialAttachments.Add(file);
            });

            context.SaveChanges();
        }

        public ICollection<UserCredentialAttachmentDto> GetAttachments(int userCredentialId)
        {
            var userCredentialAttachments = context.UserCredentialAttachments.Where(x => x.UserCredentialId == userCredentialId && x.IsDeleted == false).ToList();

            return mapper.Map<List<UserCredentialAttachmentDto>>(userCredentialAttachments);
        }

        private UserCredentialAttachment GetAttachmentById (int id) => context.UserCredentialAttachments.FirstOrDefault(x => x.Id == id);

        public void RenameAttachment(int id, string name)
        {
            var attachment = GetAttachmentById(id);
            attachment.FileName = name;

            context.SaveChanges();
        }

        public void DeleteAttachment(int id)
        {
            var attachment = GetAttachmentById(id);
            attachment.IsDeleted = true;

            context.SaveChanges();
        }

        public void DeleteUserCredential(int id)
        {
            //context.UserCredentials.Remove(GetUserCredentialById(id));
            //context.SaveChanges();
        }

        //needs refactoring: EXPENSIVE 
        public List<EmployeeSearchDto> SearchEmployeeByCredential(int[] credentialIds)
        {
            List<User> usersWithAllCredentials = new List<User>();

            var userWithAtLeastOneCredentials = context.Users.Where(x => x.IsActive == true).Include(x => x.UserCredentials)
                .Where(x => x.UserCredentials.Any(z => credentialIds.Contains(z.CredentialId))).ToList();
            
            userWithAtLeastOneCredentials.ForEach(user =>
            {
                var allUserCredentialIds = user.UserCredentials.Where(x => x.IsArchived == false && x.ExpiryDate >= DateTime.Now).Select(x => x.CredentialId).ToArray();

                if (!credentialIds.Except(allUserCredentialIds).Any())
                {
                    usersWithAllCredentials.Add(user);
                }
            });

            return mapper.Map<List<EmployeeSearchDto>>(usersWithAllCredentials);
        }

        public List<string> GetCredentialsFilePathsBasedOnJobForUser(int userId, int jobId)
        {
            var jobCredentialAndConditions = jobCredentialAndBlockService.GetJobCredentialAndBlocks(jobId);
            List<int> credentialIds = new List<int>();

            foreach(var condition in jobCredentialAndConditions)
            {
                if (condition.CredentialId != null)
                {
                    credentialIds.Add(condition.CredentialId ?? 0);
                } else
                {
                    condition.JobCredentialOrBlocks.Select(x => x.CredentialId).ToList().ForEach(x => {
                        credentialIds.Add(x);
                    });
                }
            }
            var credentialsPath = context.UserCredentials.Where(x => x.UserId == userId && credentialIds.Contains(x.CredentialId) && x.IsArchived == false).SelectMany(x => x.UserCredentialAttachments.Select(x => x.FilePath)).ToList();

            return credentialsPath;
        }
    }
}
