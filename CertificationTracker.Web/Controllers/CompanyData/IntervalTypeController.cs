﻿using CertificationTracker.Interface;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers
{
    [Authorize(Roles ="Admin")]
    [ApiController]
    [Route("api/[controller]")]
    public class IntervalTypeController : ControllerBase
    {
        private IIntervalTypeService IntervalTypeService;
        public IntervalTypeController(IIntervalTypeService _IntervalTypeService)
        {
            this.IntervalTypeService = _IntervalTypeService;
        }

        [HttpGet("GetIntervalTypeById")]
        public IActionResult GetIntervalTypeById(int id) => Ok(IntervalTypeService.GetIntervalTypeById(id));

        [HttpGet("GetIntervalTypes")]
        public IActionResult GetIntervalTypes() => Ok(IntervalTypeService.GetIntervalTypes());

        [HttpPost("SaveIntervalType")]
        public IActionResult SaveIntervalType(IntervalType IntervalType)
        {
            IntervalTypeService.SaveIntervalType(IntervalType);

            return Ok(new { Message = "Success! Interval Type has been updated successfully." });
        }

        [HttpGet("DeleteIntervalType")]
        public IActionResult DeleteIntervalType(int id)
        {
            IntervalTypeService.DeleteIntervalType(id);

            return Ok(new { Message = "Success! Interval Type has been deleted successfully." });
        }
    }
}
