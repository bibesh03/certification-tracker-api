﻿using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.CompanyData
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private IProjectService projectService;
        public ProjectController(IProjectService _projectService)
        {
            this.projectService = _projectService;
        }

        [HttpGet("GetProjectById")]
        public IActionResult GetProjectById(int id) => Ok(projectService.GetProjectById(id));

        [HttpGet("GetFormTypes")]
        public IActionResult GetFormTypes(int id) => Ok(projectService.GetFormTypes());

        [HttpGet("GetProjects")]
        public IActionResult GetProjects() => Ok(projectService.GetProjects());

        [HttpPost("SaveProject")]
        public IActionResult SaveProject(Project project)
        {
            projectService.SaveProject(project);

            return Ok(new { Message = "Success! Project has been updated successfully." });
        }

        [HttpGet("DeleteProject")]
        public IActionResult DeleteProject(int id)
        {
            projectService.DeleteProject(id);

            return Ok(new { Message = "Success! Project has been deleted successfully." });
        }
    }
}
