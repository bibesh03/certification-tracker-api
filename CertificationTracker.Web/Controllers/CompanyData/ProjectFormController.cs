﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CertificationTracker.Web.Controllers.CompanyData
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectFormController : ControllerBase
    {
        private readonly IProjectFormService projectFormService;

        public ProjectFormController(IProjectFormService _projectFormService)
        {
            this.projectFormService = _projectFormService;
        }

        [HttpGet("GetProjectFormById")]
        public IActionResult GetProjectFormById(int id) => Ok(projectFormService.GetProjectFormById(id));

        [HttpGet("GetProjectFormsByProjectId")]
        public IActionResult GetProjectFormsByProjectId(int projectId) => Ok(projectFormService.GetProjectFormsByProjectId(projectId));

        [HttpPost("SaveProjectForm")]
        public IActionResult SaveProjectForm(ProjectForm projectForm)
        {
            var saved = projectFormService.SaveProjectForm(projectForm);
            if (!saved)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "This form is already assigned to this project.");
            }
            return Ok(new { Message = "Success! Project Form has been updated successfully." });
        }

        [HttpGet("DeleteProjectForm")]
        public IActionResult DeleteProjectForm(int id)
        {
            projectFormService.DeleteProjectForm(id);

            return Ok(new { Message = "Success! Project Form has been deleted successfully." });
        }
    }
}
