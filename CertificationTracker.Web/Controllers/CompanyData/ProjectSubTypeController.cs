﻿using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.CompanyData
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectSubTypeController : ControllerBase
    {
        private IProjectSubTypeService projectSubTypeService;
        public ProjectSubTypeController(IProjectSubTypeService _projectSubTypeService)
        {
            this.projectSubTypeService = _projectSubTypeService;
        }

        [HttpGet("GetProjectSubTypeById")]
        public IActionResult GetProjectSubTypeById(int id) => Ok(projectSubTypeService.GetProjectSubTypeById(id));

        [HttpGet("GetProjectSubTypes")]
        public IActionResult GetProjectSubTypes() => Ok(projectSubTypeService.GetProjectSubTypes());

        [HttpPost("SaveProjectSubType")]
        public IActionResult SaveProjectSubType(ProjectSubType projectSubType)
        {
            projectSubTypeService.SaveProjectSubType(projectSubType);

            return Ok(new { Message = "Success! Project Sub Type has been updated successfully." });
        }

        [HttpGet("DeleteProjectSubType")]
        public IActionResult DeleteProjectSubType(int id)
        {
            projectSubTypeService.DeleteProjectSubType(id);

            return Ok(new { Message = "Success! Project Sub Type has been deleted successfully." });
        }
    }
}
