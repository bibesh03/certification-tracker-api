﻿using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.CompanyData
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectTypeController : ControllerBase
    {
        private readonly IProjectTypeService projectTypeService;

        public ProjectTypeController(IProjectTypeService _projectTypeService)
        {
            this.projectTypeService = _projectTypeService;
        }

        [HttpGet("GetProjectTypeById")]
        public IActionResult GetProjectTypeById(int id) => Ok(projectTypeService.GetProjectTypeById(id));

        [HttpGet("GetProjectTypes")]
        public IActionResult GetProjectTypes() => Ok(projectTypeService.GetProjectTypes());

        [HttpPost("SaveProjectType")]
        public IActionResult SaveProjectType(ProjectType projectType)
        {
            projectTypeService.SaveProjectType(projectType);

            return Ok(new { Message = "Success! Project Type has been updated successfully." });
        }

        [HttpGet("DeleteProjectType")]
        public IActionResult DeleteProjectType(int id)
        {
            projectTypeService.DeleteProjectType(id);

            return Ok(new { Message = "Success! Project Type has been deleted successfully." });
        }

        [HttpGet("GetProjectTypesWithSubTypes")]
        public IActionResult GetProjectTypesWithSubTypes() => Ok(projectTypeService.GetProjectTypesWithSubTypes());
    }
}
