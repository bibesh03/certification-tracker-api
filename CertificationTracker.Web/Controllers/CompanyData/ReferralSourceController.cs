﻿using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.CompanyData
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ReferralSourceController : ControllerBase
    {
        private IReferralSourceService referralSourceService;
        public ReferralSourceController(IReferralSourceService _referralSourceService)
        {
            this.referralSourceService = _referralSourceService;
        }

        [HttpGet("GetReferralSourceById")]
        public IActionResult GetReferralSourceById(int id) => Ok(referralSourceService.GetReferralSourceById(id));

        [HttpGet("GetReferralSources")]
        public IActionResult GetReferralSources() => Ok(referralSourceService.GetReferralSources());

        [HttpPost("SaveReferralSource")]
        public IActionResult SaveReferralSource(ReferralSource referralSource)
        {
            referralSourceService.SaveReferralSource(referralSource);

            return Ok(new { Message = "Success! Referral Source has been updated successfully." });
        }

        [HttpGet("DeleteReferralSource")]
        public IActionResult DeleteReferralSource(int id)
        {
            referralSourceService.DeleteReferralSource(id);

            return Ok(new { Message = "Success! Referral Source has been deleted successfully." });
        }
    }
}
