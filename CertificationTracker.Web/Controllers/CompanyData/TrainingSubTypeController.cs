﻿using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.CompanyData
{
    [ApiController]
    [Route("api/[controller]")]
    public class TrainingSubTypeController : ControllerBase
    {
        private ITrainingSubTypeService trainingSubTypeService;
        public TrainingSubTypeController(ITrainingSubTypeService _trainingSubTypeService)
        {
            this.trainingSubTypeService = _trainingSubTypeService;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("GetTrainingSubTypeById")]
        public IActionResult GetTrainingSubTypeById(int id) => Ok(trainingSubTypeService.GetTrainingSubTypeById(id));

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("GetTrainingSubTypes")]
        public IActionResult GetTrainingSubTypes(int trainingTypeId, bool showArchived) => Ok(trainingSubTypeService.GetTrainingSubTypes(trainingTypeId, showArchived));

        [Authorize(Roles = "Admin")]
        [HttpPost("SaveTrainingSubType")]
        public IActionResult SaveTrainingSubType(TrainingSubType trainingSubType)
        {
            trainingSubTypeService.SaveTrainingSubType(trainingSubType);

            return Ok(new { Message = "Success! Credential Sub Type has been updated successfully." });
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("ArchiveTrainingSubType")]
        public IActionResult ArchiveTrainingSubType(int id)
        {
            trainingSubTypeService.ArchiveTrainingSubType(id);

            return Ok(new { Message = "Success! Credential Sub Type has been deleted successfully." });
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("UnarchiveTrainingSubType")]
        public IActionResult UnarchiveTrainingSubType(int id)
        {
            var status = trainingSubTypeService.UnArchiveTrainingSubType(id);
            if (status)
                return Ok(new { Message = "Success! Credential Sub Type has been deleted successfully." });

            throw new HttpException(HttpStatusCode.BadRequest, "Credential Type for this subtype has been archived. Please unarchive the training type before unarchiving this subtype.");
        }
    }
}
