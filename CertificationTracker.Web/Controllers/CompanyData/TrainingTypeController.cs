﻿using CertificationTracker.Interface;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TrainingTypeController : ControllerBase
    {
        private ITrainingTypeService trainingTypeService;
        private IMapper mapper;
        public TrainingTypeController(ITrainingTypeService _trainingTypeService, IMapper _mapper)
        {
            this.trainingTypeService = _trainingTypeService;
            this.mapper = _mapper;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("GetTrainingTypeById")]
        public IActionResult GetTrainingTypeById(int id) => Ok(trainingTypeService.GetTrainingTypeById(id));

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("GetTrainingTypes")]
        public IActionResult GetTrainingTypes(bool showArchived) => Ok(trainingTypeService.GetTrainingTypes(showArchived));

        [Authorize(Roles = "Admin")]
        [HttpPost("SaveTrainingType")]
        public IActionResult SaveTrainingType(TrainingType trainingType)
        {
            //trainingTypeService.SaveTrainingType(mapper.Map<TrainingType>(trainingType));
            trainingTypeService.SaveTrainingType(trainingType);

            return Ok(new { Message = "Success! Credential Type has been updated successfully." });
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("ArchiveTrainingType")]
        public IActionResult ArchiveTrainingType(int id)
        {
            trainingTypeService.ArchiveTrainingType(id);

            return Ok(new { Message = "Success! Credential Type has been archived successfully." });
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("UnarchiveTrainingType")]
        public IActionResult UnarchiveTrainingType(int id)
        {
            trainingTypeService.UnArchiveTrainingType(id);

            return Ok(new { Message = "Success! Credential Type has been unarchived successfully." });
        }
    }
}
