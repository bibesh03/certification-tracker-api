﻿using CertificationTracker.Interface.Form;
using CertificationTracker.Web.Extensions;
using CertificationTracker.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Form
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class FormController : ControllerBase
    {
        private readonly IFormService formService;

        public FormController(IFormService _formService)
        {
            this.formService = _formService;
        }

        [HttpGet("GetAllForms")]
        public IActionResult GetAllForms() => Ok(formService.GetAllForms());

        [HttpGet("GetUploadFormById")]
        public IActionResult GetUploadFormById(int formId) => Ok(formService.GetUploadFormById(formId));

        [HttpPost("SaveUploadForm")]
        public IActionResult SaveUploadForm([FromForm]UploadFormDto uploadForm)
        {
            if (formService.IsFormNameAvailable(uploadForm.Name, uploadForm.Id))
            {
                if (uploadForm.File != null)
                {
                    SaveUploadFormFile(uploadForm); 
                }

                formService.SaveUploadForm(uploadForm);

                return Ok(new { Message = "Success! Form has successfully been saved" });
            }

            throw new HttpException(HttpStatusCode.BadRequest, "Form Name: " + uploadForm.Name+ " already exist. Please use unique Form Name and try saving again.");
        }

        [HttpGet("DeleteForm")]
        public IActionResult DeleteForm(int formId)
        {
            formService.DeleteForm(formId);

            return Ok(new { Message = "Success! Form has successfully been deleted" });
        }

        private void SaveUploadFormFile(UploadFormDto uploadForm)
        {
            var fileExtension = Path.GetExtension(uploadForm.File.FileName);
            var filePath = "/UploadForms/" + Guid.NewGuid() + fileExtension;

            if (Helper.SaveFileToServer(uploadForm.File, filePath))
            {
                uploadForm.FilePath = "Attachments" + filePath;
            }
        }
    }
}
