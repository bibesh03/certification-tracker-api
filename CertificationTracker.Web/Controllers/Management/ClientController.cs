﻿using CertificationTracker.Dto.Client;
using CertificationTracker.Interface;
using CertificationTracker.Models;
using CertificationTracker.Web.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ClientController : ControllerBase
    {
        private IClientService clientService;
        private readonly IMapper mapper;

        public ClientController(IClientService _clientService, IMapper _mapper)
        {
            this.clientService = _clientService;
            this.mapper = _mapper;
        }

        [HttpGet("GetClientById")]
        public IActionResult GetClientById(int id) => Ok(clientService.GetClientById(id));

        [HttpGet("GetClients")]
        public IActionResult GetClients() {
            var allClients = clientService.GetClients();

            return Ok(mapper.Map<List<ClientListingDto>>(allClients));
        }

        [HttpPost("SaveClient")]
        public IActionResult SaveClient([FromForm] ClientDetailDto client)
        {
            var filePath = "";
            if (client.UploadedFile != null)
            {
                filePath = "/Clients/" + Guid.NewGuid() + Path.GetExtension(client.UploadedFile.FileName);
                Helper.SaveFileToServer(client.UploadedFile, filePath);
                filePath = "Attachments" + filePath;
            }
            var clientLocationIds = clientService.SaveClient(client, filePath);

            return Ok(new { Message = "Success! Client has been created successfully.", clientId = clientLocationIds[0], locationId = clientLocationIds[1] });
        }

        [HttpPost("UpdateClient")]
        public IActionResult UpdateClient([FromBody] Client client)
        {
            clientService.UpdateClient(client);

            return Ok(new { Message = "Success! Client has been updated successfully." });
        }
        [HttpGet("DeleteClient")]
        public IActionResult DeleteClient(int id)
        {
            clientService.DeleteClient(id);

            return Ok(new { Message = "Success! Client has been deleted successfully." });
        }

        [HttpPost("UpdateClientLogo")]
        public IActionResult UpdateClientLogo([FromForm]ClientDetailDto client)
        {
            var imagePath = "/Clients/" + Guid.NewGuid() + Path.GetExtension(client.UploadedFile.FileName);
            Helper.SaveFileToServer(client.UploadedFile, imagePath);
            clientService.UpdateClientLogo(client.Id, "Attachments" + imagePath);

            return Ok(new { message = "Success! Client Logo has been updated.", imageUrl = "Attachments" + imagePath });
        }

        [HttpGet("GetAllClientEmails")]
        public IActionResult GetAllClientEmails(int clientId) => Ok(clientService.GetAllEmails(clientId));
    }
}
