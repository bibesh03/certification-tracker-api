﻿using CertificationTracker.Interface;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class LocationController : ControllerBase
    {
        private ILocationService locationService;
        public LocationController(ILocationService _locationService)
        {
            this.locationService = _locationService;
        }

        [HttpGet("GetLocationById")]
        public IActionResult GetLocationById(int id) => Ok(locationService.GetLocationById(id));

        [HttpGet("GetLocations")]
        public IActionResult GetLocations(int clientId) => Ok(locationService.GetLocations(clientId));

        [HttpPost("SaveLocation")]
        public IActionResult SaveLocation(Location location) =>  Ok(new { Message = "Success! Location has been updated successfully.", locationId = locationService.SaveLocation(location)});

        [HttpGet("DeleteLocation")]
        public IActionResult DeleteLocation(int id)
        {
            locationService.DeleteLocation(id);

            return Ok(new { Message = "Success! Location has been deleted successfully." });
        }
    }
}
