﻿using CertificationTracker.Interface.Management;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Management
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ReportEmailController : ControllerBase
    {
        private IReportEmailService reportEmailService;
        public ReportEmailController(IReportEmailService _reportEmailService)
        {
            this.reportEmailService = _reportEmailService;
        }

        [HttpGet("GetReportEmailById")]
        public IActionResult GetReportEmailById(int id) => Ok(reportEmailService.GetReportEmailById(id));

        [HttpGet("GetReportEmails")]
        public IActionResult GetReportEmails(int reportTypeId) => Ok(reportEmailService.GetReportEmails(reportTypeId));

        [HttpPost("SaveReportEmail")]
        public IActionResult SaveReportEmail(ReportEmail reportEmail)
        {
            reportEmailService.SaveReportEmail(reportEmail);

            return Ok(new { Message = "Success! Report Email has been updated successfully." });
        }

        [HttpGet("DeleteReportEmail")]
        public IActionResult DeleteReportEmail(int id)
        {
            reportEmailService.DeleteReportEmail(id);

            return Ok(new { Message = "Success! Report Email has been deleted successfully." });
        }
    }
}
