﻿using CertificationTracker.Interface.Management;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Management
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ReportTypeController : ControllerBase
    {
        private IReportTypeService reportTypeService;
        public ReportTypeController(IReportTypeService _reportTypeService)
        {
            this.reportTypeService = _reportTypeService;
        }

        [HttpGet("GetReportTypes")]
        public IActionResult GetReportTypes(bool isAutomated) => Ok(reportTypeService.GetReportTypes(isAutomated));
    }
}
