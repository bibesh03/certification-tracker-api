﻿using CertificationTracker.Interface.Management;
using CertificationTracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Management
{
    [Authorize(Roles = "Admin")]
    [ApiController]
    [Route("api/[controller]")]
    public class SettingController : ControllerBase
    {
        private ISettingService settingService;
        public SettingController(ISettingService _settingService)
        {
            this.settingService = _settingService;
        }

        [HttpGet("GetSettingById")]
        public IActionResult GetSettingById(int id) => Ok(settingService.GetSettingById(id));

        [HttpGet("GetSettings")]
        public IActionResult GetSettings() => Ok(settingService.GetSettings());

        [HttpPost("SaveSetting")]
        public IActionResult SaveSetting(Setting setting)
        {
            settingService.SaveSetting(setting);

            return Ok(new { Message = "Success! Setting has been updated successfully." });
        }

        [HttpGet("DeleteSetting")]
        public IActionResult DeleteSetting(int id)
        {
            settingService.DeleteSetting(id);

            return Ok(new { Message = "Success! Setting has been deleted successfully." });
        }
    }
}
