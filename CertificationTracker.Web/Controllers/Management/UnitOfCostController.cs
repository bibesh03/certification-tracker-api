﻿using CertificationTracker.Interface.Management;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Management
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class UnitOfCostController : ControllerBase
    {
        private IUnitOfCostService unitOfCostService;
        private readonly IMapper mapper;

        public UnitOfCostController(IUnitOfCostService _unitOfCostService, IMapper _mapper)
        {
            this.unitOfCostService = _unitOfCostService;
            this.mapper = _mapper;
        }

        [HttpGet("GetUnitOfCosts")]
        public IActionResult GetUnitOfCosts() => Ok(unitOfCostService.GetUnitOfCosts());
    }
}
