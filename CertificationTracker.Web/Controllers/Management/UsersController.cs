﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using CertificationTracker.Dto.User;
using CertificationTracker.Web.Extensions;
using Microsoft.AspNetCore.Http;
using CertificationTracker.Web.Helpers;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using AutoMapper;

namespace CertificationTracker.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]

    public class UsersController : ControllerBase
    {
        private IUserService userService;
        private readonly IMapper mapper;

        public UsersController(IUserService _userService, IMapper _mapper)
        {
            this.userService = _userService;
            this.mapper = _mapper;
        }

        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequestDto model)
        {
            var response = userService.Authenticate(model);
            if (response == null)
                throw new HttpException(HttpStatusCode.BadRequest, "Username or password is incorrect");

            return Ok(response);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("UpdateUserPicture")]
        public IActionResult UpdateUserPicture([FromForm] UserAvatarUpdateDto user)
        {
            var imagePath = "/UserImages/" + Guid.NewGuid() + Path.GetExtension(user.ImageUrl.FileName);
            Helper.SaveFileToServer(user.ImageUrl, imagePath);
            userService.UpdateUserAvatar(user.Id, "Attachments" + imagePath);
            
            return Ok(new { message = "Success! Avatar has been updated.", imageUrl = "Attachments" + imagePath });

        }

        [Authorize(Roles = "Admin")]
        [HttpPost("UpdatePersonalInformation")]
        public IActionResult UpdatePersonalInformation([FromForm] UserPersonalInformationDto user)
        {
            var signaturePath = "";
            if (user.SignatureFile != null)
            {
                signaturePath = "/UserSignatures/" + Guid.NewGuid() + Path.GetExtension(user.SignatureFile.FileName);
                Helper.SaveFileToServer(user.SignatureFile, signaturePath);
                user.SignatureImageUrl = "Attachments" + signaturePath;
            }
            var saved = userService.UpdatePersonalInformation(user);
            if (!saved)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Email: " + user.Email + "already exist. Please use unique Email and try saving again.");
            }
            return Ok(new { message = "Success! Updated personal information", userProfile = userService.GetUserDetails(user.Id) });
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("UpdateCompanyInformation")]
        public IActionResult UpdateCompanyInformation([FromBody] UserCompanyInfoDto user)
        {
            userService.UpdateCompanyInformation(user);
            
            return Ok(new { message = "Success! Updated company information", userProfile = userService.GetUserDetails(user.Id) });
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("Register")]
        public IActionResult Register([FromForm] RegisterUserDto user)
        {
            var imagePath = "";
            var signaturePath = "";

            if (userService.DoesUserNameOrEmailExist(user.Email, user.UserName) > 0)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Email or username already exist. Please use unique Email and Username.");
            }
            if (user.ImageUrl != null)
            {
                imagePath = "/UserImages/" + Guid.NewGuid() + Path.GetExtension(user.ImageUrl.FileName);
                Helper.SaveFileToServer(user.ImageUrl, imagePath);
                imagePath = "Attachments" + imagePath;
            }
            if (user.SignatureImage != null)
            {
                signaturePath = "/UserSignatures/" + Guid.NewGuid() + Path.GetExtension(user.SignatureImage.FileName);
                Helper.SaveFileToServer(user.SignatureImage, signaturePath);
                signaturePath = "Attachments" + signaturePath;
            }

            if (userService.RegisterUser(user, imagePath, signaturePath))
            {
                return Ok(new { message = "Success! " + user.UserName + " has been Registered." });
            }

            throw new HttpException(HttpStatusCode.InternalServerError, "Something went wrong trying to create a user. Please try again or contact tech support");
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("ChangeUserPassword")]
        public IActionResult ChangeUserPassword(int userId, string password)
        {
            userService.ChangePassword(userId, password);
            return Ok(new { message = "Success! Password has been updated successfully." });
        }

        [Authorize(Roles = "Admin, Project Manager, Worker, Technician")]
        [HttpGet("GetUserProfile")]
        public IActionResult GetUserProfile(int id) => Ok(userService.GetUserDetails(id));

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("GetAllUsers")]
        public IActionResult GetAllUsers(bool isProjectManagerView, bool isActive = true) =>  Ok(userService.GetAllUsers(isProjectManagerView, isActive));

        [Authorize(Roles = "Admin, Project Manager, Worker, Technician")]
        [HttpGet("GetProfileCardInfo")]
        public IActionResult GetProfileCardInfo(int id) {
            var userProfileInformation = userService.GetUserById(id);

            return Ok(mapper.Map<UserProfileCardDto>(userProfileInformation));
        }

        [Authorize(Roles = "Admin, Project Manager, Worker, Technician")]
        [HttpGet("GetPersonalInfo")]
        public IActionResult GetPersonalInfo(int id)
        {
            try
            {
                var userProfileInformation = userService.GetUserById(id);
                return Ok(mapper.Map<UserPersonalInformationDto>(userProfileInformation));
            } catch (Exception ex)
            {
                return Ok();
            }
        }

        [Authorize(Roles = "Admin, Project Manager, Worker, Technician")]
        [HttpGet("GetUserCompanyInfo")]
        public IActionResult GetUserCompanyInfo(int id)
        {
            var userProfileInformation = userService.GetUserById(id);

            return Ok(mapper.Map<UserCompanyInfoDto>(userProfileInformation));
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("GetUserSSN")]
        public IActionResult GetUserSSN(int id)
        {
            var userSSN = userService.GetUserSSN(id);

            return Ok(new { SSN = userSSN });
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("UpdateUserSSN")]
        public IActionResult UpdateUserSSN(int id, string ssn)
        {
            userService.UpdateUsersSSN(id, ssn);

            return Ok(new { message = "Success! SSN has been successfully updated" });
        }
    }
}
