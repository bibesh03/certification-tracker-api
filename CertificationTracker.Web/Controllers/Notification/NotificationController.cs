﻿using CertificationTracker.Web.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Notification
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService notificationService;
        private readonly IMapper mapper;

        public NotificationController(INotificationService _notificationService, IMapper _mapper)
        {
            this.notificationService = _notificationService;
            this.mapper = _mapper;
        }

        [HttpGet("GetNotifications")]
        public IActionResult GetNotifications() => Ok(notificationService.GetNotifications(Helper.GetLoggedInUserId(this.User)));

        [HttpPost("SetNotificationAsRead")]
        public IActionResult SetNotificationAsRead(int notificationId)
        {
            notificationService.SetNotificationAsRead(notificationId);

            return Ok(new { Success = true });
        }
    }
}
