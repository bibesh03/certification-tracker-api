﻿using CertificationTracker.Dto.Job;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class JobController : ControllerBase
    {
        private readonly IJobService jobService;
        private readonly IMapper mapper;

        public JobController(IJobService _jobService, IMapper _mapper)
        {
            this.jobService = _jobService;
            this.mapper = _mapper;
        }

        [HttpGet("GetJobs")]
        public IActionResult GetJobs() => Ok(jobService.GetAllJobs());

        [HttpGet("GetJobCardInfo")]
        public IActionResult GetJobCardInfo(int jobId) => Ok(mapper.Map<JobCardViewDto>(jobService.GetJobById(jobId)));

        [HttpGet("GetJobById")]
        public IActionResult GetJobById(int jobId)
        {
            var job = jobService.GetJobById(jobId);

            return Ok(job);
        }

        [HttpGet("GetWeekDayDifferenceBetweenTwoDates")]
        public IActionResult GetWeekDayDifferenceBetweenTwoDates(DateTime start, DateTime end)
        {
            var count = 0;

            while(start.Date <= end.Date)
            {
                if (start.DayOfWeek != DayOfWeek.Saturday && start.DayOfWeek != DayOfWeek.Sunday)
                {
                    count++;
                }

                start = start.AddDays(1);
            }

            return Ok(count);
        }

        [HttpPost("SaveJob")]
        public IActionResult SaveJob(Job job)
        {
            jobService.SaveJob(job);

            return Ok(new { Message = "Success! Job Information has been successfully saved." });
        }
    }
}
