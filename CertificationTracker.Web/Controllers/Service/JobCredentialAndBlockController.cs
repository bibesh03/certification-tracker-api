﻿using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class JobCredentialAndBlockController : ControllerBase
    {
        private readonly IJobCredentialAndBlockService jobCredentialAndBlockService;
        private readonly IMapper mapper;

        public JobCredentialAndBlockController(IJobCredentialAndBlockService _jobCredentialAndBlockService, IMapper _mapper)
        {
            this.jobCredentialAndBlockService = _jobCredentialAndBlockService;
            this.mapper = _mapper;
        }

        [HttpGet("GetJobCredentialAndBlocks")]
        public IActionResult GetJobCredentialAndBlocks(int jobId) => Ok(jobCredentialAndBlockService.GetJobCredentialAndBlocks(jobId));

        [HttpPost("SaveJobCredentialAndBlock")]
        public IActionResult SaveJobCredentialBlock(List<JobCredentialAndBlock> jobCredentialAndBlock)
        {
            jobCredentialAndBlockService.SaveJobCredentialAndBlock(jobCredentialAndBlock);

            return Ok(jobCredentialAndBlock);
        }

        [HttpGet("DeleteJobCredentialAndBlock")]
        public IActionResult DeleteJobCredentialAndBlock(int jobCredentialAndBlockId, bool isOrBlock)
        {
            jobCredentialAndBlockService.DeleteJobCredentialAndBlock(jobCredentialAndBlockId, isOrBlock);

            return Ok();
        }
    }
}
