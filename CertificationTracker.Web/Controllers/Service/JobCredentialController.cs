﻿using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class JobCredentialController : ControllerBase
    {
        private readonly IJobCredentialService jobCredentialService;
        private readonly IJobCredentialAndBlockService jobCredentialAndBlockService;

        public JobCredentialController(IJobCredentialService _jobCredentialService, IJobCredentialAndBlockService _jobCredentialAndBlockService)
        {
            this.jobCredentialAndBlockService = _jobCredentialAndBlockService;
            this.jobCredentialService = _jobCredentialService;
        }

        [HttpGet("GetJobCredentialById")]
        public IActionResult GetJobCredentialById(int id) => Ok(jobCredentialService.GetJobCredentialById(id));

        [HttpGet("GetJobCredentials")]
        public IActionResult GetJobCredentials(int jobId) => Ok(jobCredentialService.GetJobCredentials(jobId));

        [HttpPost("SaveJobCredentials")]
        public IActionResult SaveJobCredentials([FromBody] List<JobCredential> jobCredential)
        {
            if (jobCredentialService.SaveJobCredential(jobCredential))
            {
                return Ok(new { Message = "Success! Job Credential has been updated successfully.", jobCredentials = jobCredential });
            }

            throw new HttpException(HttpStatusCode.BadRequest, "One or many credential selected has already been added to the job. Please remove the ones that are already added and try again.");
        }

        [HttpGet("DeleteJobCredential")]
        public IActionResult DeleteJobCredential(int id, int jobId)
        {
            if (jobCredentialAndBlockService.IsCredentialCurrentlyBeingUsed(jobId, id))
            {
                throw new HttpException(HttpStatusCode.BadRequest, "This Credential is currently being used on the credential conditional block. Please remove the credential from the block before you remove from this credential.");
            }
            jobCredentialService.DeleteJobCredential(id);

            return Ok(new { Message = "Success! Job Credential has been deleted successfully." });
        }
    }
}
