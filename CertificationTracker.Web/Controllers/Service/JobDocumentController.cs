﻿using CertificationTracker.Dto.Job;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using CertificationTracker.Web.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class JobDocumentController : ControllerBase
    {
        private readonly IJobDocumentService jobDocumentService;
        private readonly IMapper mapper;

        public JobDocumentController(IJobDocumentService _jobDocumentService, IMapper _mapper)
        {
            this.jobDocumentService = _jobDocumentService;
            this.mapper = _mapper;
        }

        [HttpGet("GetJobDocuments")]
        public IActionResult GetJobDocuments(int jobId) => Ok(jobDocumentService.GetJobDocuments(jobId));

        [HttpPost("SaveJobDocument")]
        public IActionResult SaveJobDocument([FromForm] JobDocumentDto jobDocument)
        {
            var newJobDocument = mapper.Map<JobDocument>(jobDocument);

            if (jobDocument.File != null)
            {
                string filePath = "/JobDocuments/" + Guid.NewGuid() + Path.GetExtension(jobDocument.File.FileName);
                Helper.SaveFileToServer(jobDocument.File, filePath);
                newJobDocument.FilePath = "Attachments" + filePath;
            }

            newJobDocument.CreatedOn = DateTime.Now;
            newJobDocument.CreatedByUserId = Helper.GetLoggedInUserId(this.User);
            jobDocumentService.Create(newJobDocument);

            return Ok(new { jobDocument = newJobDocument, Message = "Success! " + ((jobDocument.File == null) ? "New Folder was created succesfully." : "New File was uploaded Successfully.")});
        }

        [HttpGet("DeleteJobDocument")]
        public IActionResult DeleteJobDocument(int jobDocumentId)
        {
            jobDocumentService.Delete(jobDocumentId);

            return Ok(new { Message = "Successfully Deleted" });
        }

        [HttpPost("RenameJobDocument")]
        public IActionResult RenameJobDocument(int jobDocumentId, string newName)
        {
            jobDocumentService.Rename(jobDocumentId, newName);

            return Ok(new { Message = "Successfully Renamed" });
        }
    }
}
