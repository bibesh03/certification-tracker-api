﻿using CertificationTracker.Interface;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using CertificationTracker.Web.Helpers;
using AutoMapper;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class JobTeamMemberController : ControllerBase
    {
        private readonly IJobTeamMemberService jobTeamMemberService;
        private readonly IMapper mapper;
        private readonly IUserCredentialService userCredentialService;

        public JobTeamMemberController(IJobTeamMemberService _jobTeamMemberService, IMapper _mapper, IUserCredentialService _userCredentialService)
        {
            this.jobTeamMemberService = _jobTeamMemberService;
            this.mapper = _mapper;
            this.userCredentialService = _userCredentialService;
        }

        [HttpGet("GetJobTeamMembers")]
        public IActionResult GetJobTeamMembers(int jobId) => Ok(jobTeamMemberService.GetJobTeamMembers(jobId));

        [HttpGet("GetAvailableUsersForJob")]
        public IActionResult GetAvailableUsersForJob(int jobId) => Ok( new { availableMembers = jobTeamMemberService.GetAvailableUsersForTeamMember(jobId), DoesFormLeadExist = jobTeamMemberService.DoesFormLeadExist(jobId) } );

        [AllowAnonymous]
        [HttpGet("ViewTeamMemberCredentialsForJob")]
        public IActionResult ViewTeamMemeberCredentialsForJob(int jobTeamMemberId, string token)
        {
            var roles = Helper.GetRolesFromToken(token);
            var rolesRequired = new string[] { "Admin", "Project Manager" };

            if (roles.Any(x => rolesRequired.Contains(x)))
            {
                var jobTeamMember = jobTeamMemberService.GetJobTeamMemberById(jobTeamMemberId);
                byte[] bytes = System.IO.File.ReadAllBytes(Path.Combine(Environment.CurrentDirectory, "../" + jobTeamMember.JobCredentialPdfPath));
                Response.Headers.Add("Content-Disposition", "inline; filename=" + "User Credentials for Job.pdf");

                return File(bytes, MediaTypeNames.Application.Pdf);
            }

            return null;
        }

        [HttpPost("SaveJobTeamMember")]
        public IActionResult SaveJobTeamMember(JobTeamMember jobTeamMember)
        {
            var filePaths = userCredentialService.GetCredentialsFilePathsBasedOnJobForUser(jobTeamMember.UserId, jobTeamMember.JobId);
            jobTeamMember.JobCredentialPdfPath = SaveCredentialFileToServer(jobTeamMember.JobId, filePaths);

            if (jobTeamMemberService.SaveJobTeamMember(jobTeamMember))
            {
                return Ok(new { Message = "Successfully added team member" });
            }
            
            throw new HttpException(HttpStatusCode.BadRequest, "This user is already in the team member. Please try to add a different user");
        }

        [HttpGet("DeleteTeamMember")]
        public IActionResult DeleteTeamMember(int jobTeamMemberId)
        {
            jobTeamMemberService.DeleteTeamMember(jobTeamMemberId);

            return Ok(new { Message = "Successfully deleted team member" });
        }

        private string SaveCredentialFileToServer(int jobId, List<string> filePaths)
        {
            var directoryPath = Path.Combine(Environment.CurrentDirectory, "../Attachments/" + "/JobCredentials/" + jobId + "/");
            var fileNameToSave = Guid.NewGuid() + ".pdf";
            System.IO.Directory.CreateDirectory(directoryPath);
            var pdfFileBytes = Helper.CombineAllFilesToPdf(filePaths);
            System.IO.File.WriteAllBytes(directoryPath + fileNameToSave, pdfFileBytes);

            return "Attachments/JobCredentials/" + jobId + "/" + fileNameToSave;
        }
    }
}
