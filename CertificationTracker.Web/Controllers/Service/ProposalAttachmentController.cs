﻿using CertificationTracker.Dto.Proposal;
using CertificationTracker.Interface.Service;
using CertificationTracker.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ProposalAttachmentController : ControllerBase
    {
        private readonly IProposalAttachmentService proposalAttachmentService;

        public ProposalAttachmentController(IProposalAttachmentService _proposalAttachmentService)
        {
            this.proposalAttachmentService = _proposalAttachmentService;
        }

        [HttpGet("GetProposalAttachment")]
        public IActionResult GetProposalAttachment(int id) => Ok(proposalAttachmentService.GetProposalAttachmentById(id));

        [HttpGet("GetProposalAttachments")]
        public IActionResult GetProposalAttachments(int proposalId) => Ok(proposalAttachmentService.GetProposalAttachments(proposalId));

        [HttpGet("DeleteProposalAttachment")]
        public IActionResult DeleteProposalAttachment(int id)
        {
            proposalAttachmentService.DeleteProposalAttachment(id, Helper.GetLoggedInUserId(this.User));

            return Ok(new { Message = "Success! Attachment has been deleted successfully. " });
        }

        [HttpPost("SaveProposalAttachment")]
        public IActionResult SaveProposalAttachment([FromForm] ProposalAttachmentDto proposalAttachmentDto)
        {
            var filePath = "";
            if (proposalAttachmentDto.UploadedFile != null)
            {
                filePath = "/ProposalAttachments/" + Guid.NewGuid() + Path.GetExtension(proposalAttachmentDto.UploadedFile.FileName);
                Helper.SaveFileToServer(proposalAttachmentDto.UploadedFile, filePath);
                filePath = "Attachments" + filePath;
                proposalAttachmentDto.UploadedDateTime = DateTime.Now;
                proposalAttachmentDto.UploadedById = Helper.GetLoggedInUserId(this.User);
            }
            proposalAttachmentService.SaveProposalAttachment(proposalAttachmentDto, filePath);

            return Ok(new { Message = "Success! Attachment has been updated successfully." });
        }
    }
}
