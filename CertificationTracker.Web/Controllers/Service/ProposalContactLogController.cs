﻿using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using CertificationTracker.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ProposalContactLogController : ControllerBase
    {
        private readonly IProposalContactLogService proposalContactLogService;

        public ProposalContactLogController(IProposalContactLogService _proposalContactLogService)
        {
            this.proposalContactLogService = _proposalContactLogService;
        }

        [HttpGet("GetProposalContactLogs")]
        public IActionResult GetProposalContactLogs(int proposalId) => Ok(proposalContactLogService.GetProposalContactLogs(proposalId));

        [HttpPost("SaveProposalContactLog")]
        public IActionResult SaveProposalContactLog(ProposalContactLog model)
        {
            var loggedInUserId = Helper.GetLoggedInUserId(this.User);
            if (model.Id == 0)
            {
                model.UserId = loggedInUserId;
                model.CreatedDate = DateTime.Now;
            }

            proposalContactLogService.SaveProposalContactLog(model);

            return Ok(new { Message = "Success! Log has been saved successfully."});
        }

    }
}
