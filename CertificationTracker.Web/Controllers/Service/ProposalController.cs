﻿using CertificationTracker.Dto.Proposal;
using CertificationTracker.Interface.Management;
using CertificationTracker.Interface.Report;
using CertificationTracker.Interface.Service;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using CertificationTracker.Web.Helpers;
using CertificationTracker.Web.Hubs;
using AutoMapper;
using FluentEmail.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers.Service
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class ProposalController : ControllerBase
    {
        private readonly IProposalService proposalService;
        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly IHubContext<ChatHub> chatHub;
        private readonly IHubContext<NotificationHub> notificationHub;
        private readonly IProposalReviewersLogService proposalReviewersLogService;
        public readonly IReportService reportService;
        public readonly IEmailService emailService;

        public ProposalController(IProposalService _proposalService, IMapper _mapper, IUserService _userService, IHubContext<ChatHub> _chatHub, IHubContext<NotificationHub> _notificationHub,
            IProposalReviewersLogService _proposalReviewersLogService, IReportService _reportService, IEmailService _emailService)
        {
            this.proposalService = _proposalService;
            this.mapper = _mapper;
            this.userService = _userService;
            this.chatHub = _chatHub;
            this.notificationHub = _notificationHub;
            this.proposalReviewersLogService = _proposalReviewersLogService;
            this.reportService = _reportService;
            this.emailService = _emailService;
        }

        [HttpGet("GetProposals")]
        public IActionResult GetProposals(string status)
        {
            var loggedInUserId = Helper.GetLoggedInUserId(this.User);
            var proposals = proposalService.GetProposals(status, userService.GetUserRoles(loggedInUserId).Contains("Admin"), loggedInUserId);

            return Ok(mapper.Map<List<ProposalListingDto>>(proposals));
        }

        [HttpGet("GetProposalById")]
        public IActionResult GetProposalById(int proposalId) => Ok(proposalService.GetProposalById(proposalId));

        [HttpGet("GetProposalManageInfo")]
        public IActionResult GetProposalManageInfo(int proposalId) => Ok(proposalService.GetProposalManageInfo(proposalId));

        [HttpPost("SaveProposal")]
        public IActionResult SaveProposal(Proposal proposal)
        {
            var loggedInUserId = Helper.GetLoggedInUserId(this.User);
            if (proposal.Id == 0)
            {
                proposal.CreatedBy = loggedInUserId;
                proposal.CreatedDateTime = DateTime.Now;
            } else
            {
                proposal.ScopeOfWorks.ToList().ForEach(x => {
                    x.AddedByUserId = x.AddedByUserId != 0 ? x.AddedByUserId : loggedInUserId;
                    x.AddedDateTime = x.AddedDateTime != DateTime.MinValue ? x.AddedDateTime : DateTime.Now;
                });
                proposal.UpdatedDateTime = DateTime.Now;
                proposal.UpdatedBy= loggedInUserId;
            }
            proposalService.SaveProposal(proposal);

            return Ok(new { Message = "Success! Proposal has been saved successfully.", proposalId = proposal.Id });
        }

        [HttpGet("DeleteProposal")]
        public IActionResult DeleteProposal(int proposalId)
        {
            proposalService.DeleteProposal(proposalId, Helper.GetLoggedInUserId(this.User));

            return Ok(new { Message = "Success! Proposal has been saved deleted." });
        }

        [HttpPost("SaveProposalReviewers")]
        public IActionResult SaveProposalReviewers([FromBody]ProposalReviewerDto reviewers)
        {
            var notification = proposalService.SaveProposalReviewers(reviewers, Helper.GetLoggedInUserId(this.User));
            SendChatNotyAndNotification(notification, reviewers.Id);

            return Ok(new { Message = "Success! Reviewers for the proposal has been successfully updated." });
        }

        [HttpGet("GetProposalReviewers")]
        public IActionResult GetProposalReviewers(int proposalId) => Ok(proposalService.GetProposalReviewers(proposalId));

        [HttpGet("GetUserPermissionForProposal")]
        public IActionResult GetUserPermissionForProposal(int proposalId)
        {
            var loggedInUserId = Helper.GetLoggedInUserId(this.User);
            var isAdmin = userService.GetUserRoles(loggedInUserId).Contains("Admin");
            var roleAndHasReviewed = proposalService.GetProposalPermission(proposalId, loggedInUserId);
            if (roleAndHasReviewed["Role"] == "NONE" && !isAdmin)
            {
                throw new HttpException(HttpStatusCode.Forbidden, "Error! You do not have authorization to open this proposal.");
            }

            return Ok(new
            {
                UserRole = (roleAndHasReviewed["Role"] == "Reviewer" && isAdmin) ? "AdminReviewer" : isAdmin ? "Admin" : roleAndHasReviewed["Role"],
                HasReviewed = bool.Parse(roleAndHasReviewed["HasReviewed"]),
                DateReviewed = roleAndHasReviewed["DateReviewed"] == "N/A" ? null : roleAndHasReviewed["DateReviewed"]
            });
        }

        [HttpGet("GetProposalMessages")]
        public IActionResult GetProposalMessages(int proposalId, int length) => Ok(proposalReviewersLogService.GetMessages(proposalId, length));

        [HttpGet("GetReviewersHistory")]
        public IActionResult GetReviewersHistory(int proposalId) => Ok(proposalService.GetReviewersHistory(proposalId));

        [HttpPost("SetProposalAsReviewed")]
        public IActionResult SetProposalAsReviewed(int proposalId, int userId)
        {
            var notyNotification = proposalService.SetProposalAsReviewed(proposalId, userId);
            SendChatNotyAndNotification(notyNotification, proposalId);

            return Ok(new { Success = true });
        }

        [HttpPost("AddExtension")]
        public IActionResult AddExtension(int proposalId, int noOfHours)
        {
            var notifications = proposalService.AddExtension(proposalId, noOfHours);
            SendChatNotyAndNotification(new ProposalNotyNotificationDto
            {
                ChatNoty = null,
                Notifications = notifications
            }, 0);

            return Ok(new { Message = "Success! Review deadline extension has been succesfully applied"});
        }

        [HttpGet("GetProposalSendInfo")]
        public IActionResult GetProposalSendInfo(int proposalId) =>  Ok(proposalService.GetProposalSendInfo(proposalId));

        [AllowAnonymous]
        [HttpGet("GetProposalPdf")]
        public IActionResult GetProposalPdf(int proposalId, string token)
        {
            var roles = Helper.GetRolesFromToken(token);
            var rolesRequired = new string[] { "Admin", "Project Manager" };

            if (roles.Any(x => rolesRequired.Contains(x)))
            {
                var proposalPdf = reportService.GetProposalPrintOut(proposalId);
                Response.Headers.Add("Content-Disposition", new ContentDisposition { FileName = "Proposal_Preview" + ".pdf", Inline = true }.ToString());

                return File(proposalPdf.ToArray(), "application/pdf");
            }

            return null;
        }

        [HttpGet("GetProposalEstimator")]
        public ActionResult GetProposalEstimator(int proposalId) => Ok(proposalService.GetProposalEstimator(proposalId));

        [HttpPost("SaveProposalEstimator")]
        public ActionResult SaveProposalEstimator([FromForm] ProposalEstimatorDto estimatorDto)
        {
            estimatorDto.ProposalEstimatedConsolidatedCharges = JsonConvert.DeserializeObject<ICollection<ProposalEstimatedConsolidatedCharge>>(estimatorDto.ProposalEstimatedConsolidatedChargesString);
            if (estimatorDto.UploadedFile != null)
            {
                var filePath = "/ProposalEstimators/" + Guid.NewGuid() + Path.GetExtension(estimatorDto.UploadedFile.FileName);

                Helper.SaveFileToServer(estimatorDto.UploadedFile, filePath);
                estimatorDto.EstimatorFilePath = filePath;
            }
            proposalService.SaveProposalEstimator(estimatorDto);

            return Ok(new { Message = "Success! Proposal Estimator details has been saved succesfully" });
        } 

        [HttpPost("SendProposalToClient")]
        public IActionResult SendProposalToClient(int proposalId)
        {
            var projectNumber = proposalService.GetProjectNumber(proposalId);
            var proposalPdf = reportService.GetProposalPrintOut(proposalId);

            System.IO.File.WriteAllBytes(Path.Combine(Environment.CurrentDirectory, "../Attachments/Proposals/" + projectNumber + ".pdf"), proposalPdf);
            SendProposalEmailToClient(projectNumber, proposalId);
            var notyNotification = proposalService.SendProposalToClient(proposalId, Helper.GetLoggedInUserId(this.User));
            SendChatNotyAndNotification(notyNotification, proposalId);

            return Ok(new { Message= "Success! Proposal along with documents selected has been succesfully emailed to client", Success = true });
        }

        [HttpPost("RejectProposal")]
        public IActionResult RejectProposal(int proposalId, string rejectionReason)
        {
            var notifications = proposalService.RejectProposal(proposalId, Helper.GetLoggedInUserId(this.User), rejectionReason);
            SendChatNotyAndNotification(new ProposalNotyNotificationDto
            {
                ChatNoty = null,
                Notifications = notifications
            }, 0);

            return Ok(new { Message = "Success! Proposal has been rejected successfully." });
        }

        [HttpGet("GetRejectionReason")]
        public IActionResult GetRejectionReason(int proposalId)
        {
            var proposal = proposalService.GetProposalById(proposalId);

            return Ok(new { RejectionReason = proposal.RejectionReason });
        }

        private void SendProposalEmailToClient(string projectNumber, int proposalId)
        {
            var provider = new FileExtensionContentTypeProvider();

            var emailsAndFiles = proposalService.GetProposalSendInfo(proposalId);
            var emailsToSend = emailsAndFiles.Emails.Select(x => new FluentEmail.Core.Models.Address {
                EmailAddress = x
            });

            emailsAndFiles.ProposalFiles.Insert(0, new ProposalFile
            {
                Name = "Proposal",
                Path = "Attachments/Proposals/" + projectNumber + ".pdf"
            });

            var filesToSend = new List<Attachment>();

            emailsAndFiles.ProposalFiles.ForEach(x =>
            {
                string contentType;
                if (!provider.TryGetContentType(x.Path, out contentType))
                {
                    contentType = "application/octet-stream";
                }

                filesToSend.Add(new Attachment()
                {
                     ContentType = contentType,
                     Filename = x.Name,
                    Data = System.IO.File.OpenRead((Path.Combine(Environment.CurrentDirectory, "../" + x.Path)))
                });
            });

            emailService.SendEmail<Proposal>(emailsToSend, "Proposal", "EmailTemplates/Proposal.cshtml", new Proposal { }, filesToSend);
        }

        [HttpGet("GetProposalProjectLogs")]
        public IActionResult GetProposalProjectLogs() => Ok(proposalService.GetProposalProjectLogs());

        [HttpPost("ApproveProposal")]
        public IActionResult ApproveProposal (int proposalId)
        {
            var notifications = proposalService.ApproveProposal(proposalId, Helper.GetLoggedInUserId(this.User));
            notifications.ForEach(notification =>
            {
                notificationHub.Clients.All.SendAsync("Notification/" + notification.UserId, notification);
            });

            return Ok(new { Message = "Success! Proposal has been successfully approved" });
        }

        private void SendChatNotyAndNotification(ProposalNotyNotificationDto proposalNotyNotificationDto, int proposalId)
        {
            if (proposalNotyNotificationDto.ChatNoty != null)
            {
                proposalNotyNotificationDto.ChatNoty.ForEach(noty =>
                {
                    chatHub.Clients.All.SendAsync("ProposalMessage/" + proposalId, new ProposalMessageDto
                    {
                        Message = noty,
                        ProposalId = proposalId
                    });
                });

            }

            if (proposalNotyNotificationDto.Notifications != null)
            {
                proposalNotyNotificationDto.Notifications.ForEach(notification =>
                {
                    notificationHub.Clients.All.SendAsync("Notification/" + notification.UserId, notification);
                });
            }
        }
    }
}
