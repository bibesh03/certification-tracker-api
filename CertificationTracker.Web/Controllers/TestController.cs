﻿using CertificationTracker.Dto.Credential;
using CertificationTracker.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using ExcelDataReader;
using System.Data;
using System.Text;
using CertificationTracker.Models;

namespace CertificationTracker.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private readonly IUserCredentialService userCredentialService;
        private readonly ICredentialService credentialService;
        
        public TestController(IUserCredentialService _userCredentialService, ICredentialService _credentialService)
        {
            userCredentialService = _userCredentialService;
            credentialService = _credentialService;
        }

        [HttpGet("Test")]
        public ActionResult Test()
        {
            var newClient = new Client
            {
                AccountNumber = "#0001234",
                ContactName = "John Doe",
                Name = "Primtek LLC",
                PrimaryPhone = "13249567",
                AdditionalEmails = "bibesh@primtek.net",
                IsActive = true,
                Address = new Address
                {
                    State = "LA"
                }
            };
            var oldClient = new Client
            {
                AccountNumber = "#0001234",
                ContactName = "Jane Doe",
                Name = "Primtek LLC",
                PrimaryPhone = "78945641",
                AdditionalEmails = "chase@primtek.net, chris@primtek.net",
                IsActive = false,
                Address = new Address
                {
                    State = "CA"
                }
            };
            var test = this.CompareObject<Client>(newClient, oldClient);

            return Ok();
        }

        private string CompareObject<T>(T newObject, T oldObject)
        {
            Type type = typeof(T);
            if (newObject == null || oldObject == null)
                return "";
            StringBuilder sb = new StringBuilder();

            foreach(System.Reflection.PropertyInfo property in type.GetProperties())
            {
                if (property.Name != "ExtensionData")
                {
                    string newValue = "";
                    string oldValue = "";

                    if(type.GetProperty(property.Name).GetValue(newObject, null) != null)
                        newValue = type.GetProperty(property.Name).GetValue(newObject, null).ToString();
                    if (type.GetProperty(property.Name).GetValue(oldObject, null) != null)
                        oldValue = type.GetProperty(property.Name).GetValue(oldObject, null).ToString();
                    if (newValue.Trim() != oldValue.Trim())
                    {
                        sb.Append("Changed the " + property.Name  + " from [ " + oldValue + " ] to [ " + newValue + " ] .") ;
                    }
                }
            }

            return sb.ToString();
        }

        [HttpGet("Go")]
        public ActionResult Go()
        {
            //using (var stream = System.IO.File.Open(@"D:\User_Certifications.csv", FileMode.Open, FileAccess.Read))
            //{
            //    using (var reader = ExcelReaderFactory.CreateCsvReader(stream))
            //    {
            //        var conf = new ExcelDataSetConfiguration
            //        {
            //            ConfigureDataTable = _ => new ExcelDataTableConfiguration
            //            {
            //                UseHeaderRow = true
            //            }
            //        };

            //        DataSet dataSet = reader.AsDataSet(conf);
            //        //DataTable dataTable = dataSet.Tables["Sheet1"];
            //        DataRowCollection row = dataSet.Tables["Table1"].Rows;
            //        //DataColumnCollection col = dataSet.Tables["Sheet1"].Columns;

            //        List<object> rowDataList = null;
            //        List<object> allRowsList = new List<object>();
            //        foreach (DataRow item in row)
            //        {
            //            var obj = item.ItemArray;
            //            var credential = credentialService.GetCredentialByName(obj[1] + "");

            //            if (credential != null)
            //            {
            //                var userCredential = new UserCredentialDetailDto
            //                {
            //                    ArchivedDateTime = null,
            //                    SiteCode = obj[7] + "" == "NULL" ? null : obj[7] + "",
            //                    Comments = obj[4] + "" == "NULL" ? null : obj[4] + "",
            //                    CredentialId = credential.Id,
            //                    EffectiveDate = DateTime.Parse(obj[2] + ""),
            //                    ExpiryDate = DateTime.Parse(obj[3] + ""),
            //                    IsArchived = Int32.Parse(obj[6] + "") == 1,
            //                    ImageUrl = obj[5] + "" == "NULL" ? null : obj[5] + "",
            //                    UserId = Int32.Parse(obj[0] + "")
            //                };
            //                userCredentialService.SaveUserCredential(userCredential);
            //            }
            //        };
            //    };
            //}

            return Ok();
        }

        [HttpGet("Index")]
        public IActionResult Index()
        {
            return View("../../EmailTemplates/Test", new UserCredentialEmail
            {
                Name = "Test",
            });
        }
    }
}
