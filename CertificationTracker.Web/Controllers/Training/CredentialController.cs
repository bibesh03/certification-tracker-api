﻿using CertificationTracker.Interface;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers
{
    [Authorize(Roles = "Admin, Project Manager")]
    [ApiController]
    [Route("api/[controller]")]
    public class CredentialController : ControllerBase
    {
        private ICredentialService credentialService;
        private IUserCredentialService userCredentialService;
        public CredentialController(ICredentialService _credentialService, IUserCredentialService _userCredentialService)
        {
            this.credentialService = _credentialService;
            this.userCredentialService = _userCredentialService;
        }

        [HttpGet("GetCredentialById")]
        public IActionResult GetCredentialById(int id) => Ok(credentialService.GetCredentialById(id));

        [HttpGet("GetCredentials")]
        public IActionResult GetCredentials(bool showArchived) => Ok(credentialService.GetCredentials(showArchived));

        [HttpGet("GetCredentialsByTypeAndSubType")]
        public IActionResult GetCredentialsByTypeAndSubType(int typeId, int? subTypeId) => Ok(credentialService.GetCredentialsByTypeAndSubType(typeId, subTypeId));

        [HttpPost("SaveCredential")]
        public IActionResult SaveCredential(Credential Credential)
        {
            credentialService.SaveCredential(Credential);

            return Ok(new { Message = "Success! Credential has been updated successfully." });
        }

        [HttpGet("ArchiveCredential")]
        public IActionResult ArchiveCredential(int id)
        {
            credentialService.ArchiveCredential(id);

            return Ok(new { Message = "Success! Credential has been archived successfully." });
        }

        [HttpGet("UnArchiveCredential")]
        public IActionResult UnArchiveCredential(int id)
        {
            var status = credentialService.UnArchiveCredential(id);
            if (status)
            {
                return Ok( new { Message = "Success! Credential has been un-archived successfully." });
            }

            throw new HttpException(HttpStatusCode.BadRequest, "Credentials's type or sub type is archived. Please make sure you unarchive both type or subtype of this credential.");
        }
    }
}
