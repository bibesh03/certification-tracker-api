﻿using CertificationTracker.Dto.Credential;
using CertificationTracker.Interface;
using CertificationTracker.Models;
using CertificationTracker.Web.Extensions;
using CertificationTracker.Web.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserCredentialController : ControllerBase
    {
        private IUserCredentialService userCredentialService;
        public UserCredentialController(IUserCredentialService _userCredentialService)
        {
            this.userCredentialService = _userCredentialService;
        }

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("GetUserCredentialById")]
        public IActionResult GetUserCredentialById(int id) => Ok(userCredentialService.GetUserCredentialById(id));
        
        [Authorize(Roles = "Admin, Project Manager, Technician, Worker")]
        [HttpGet("GetUserCredentialByUserId")]
        public IActionResult GetUserCredentialByUserId(int userId, bool showArchived) => Ok(userCredentialService.GetUserCredentialByUserId(userId, showArchived));

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("GetUserCredentials")]
        public IActionResult GetUserCredentials(bool showArchived) => Ok(userCredentialService.GetUserCredentials(showArchived));

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("CheckDuplicateUserCredential")]
        public IActionResult CheckDuplicateUserCredential(int userId, int credentialId, int? userCredentialId)
        {
            var duplicateCredential = userCredentialService.CheckDuplicateUserCredential(userId, credentialId, userCredentialId);
            
            return Ok(duplicateCredential == null ? 0 : duplicateCredential.Id);
        }

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpPost("SaveUserCredential")]
        public IActionResult SaveUserCredential([FromForm]UserCredentialDetailDto userCredential)
        {
            var allFilesUploaded = true;
            if (userCredential.UploadedFiles != null)
            {
                allFilesUploaded = UploadFileAndSetToUserCredential(userCredential);
            }
            if (allFilesUploaded)
            {
                userCredentialService.SaveUserCredential(userCredential);

                return Ok(new { Message = "Success! User Credential has been updated successfully." });
            }

            throw new HttpException(HttpStatusCode.BadRequest, "One or more files could not be uploaded to the server. Please review the file and make sure they are valid before submitting");
        }

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpPost("SaveAttachments")]
        public IActionResult SaveAttachments([FromForm] UserCredentialDetailDto userCredential)
        {
            UploadFileAndSetToUserCredential(userCredential);
            userCredentialService.SaveAttachments(userCredential);

            return Ok(new { Message = "Success! New Files have been successfully added to this credential." });
        }

        private bool UploadFileAndSetToUserCredential(UserCredentialDetailDto userCredential)
        {
            userCredential.UserCredentialAttachments = new List<UserCredentialAttachment>();

            foreach (var uploadedFile in userCredential.UploadedFiles)
            {
                var fileExtension = Path.GetExtension(uploadedFile.FileName);
                var filePath = "/UserCredentials/" + Guid.NewGuid() + fileExtension;

                if (Helper.SaveFileToServer(uploadedFile, filePath))
                {
                    userCredential.UserCredentialAttachments.Add(new UserCredentialAttachment
                    {
                        FilePath = "Attachments" + filePath,
                        FileName = Path.GetFileNameWithoutExtension(uploadedFile.FileName),
                        FileExtension = fileExtension,
                        UploadedDateTime = DateTime.Now,
                    });
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("DeleteUserCredential")]
        public IActionResult DeleteUserCredential(int id)
        {
            userCredentialService.DeleteUserCredential(id);

            return Ok(new { Message = "Success! User Credential has been deleted successfully." });
        }

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("ToggleUserCredentialArchived")]
        public IActionResult ToggleUserCredentialArchive(int id)
        {
            var isArchived = userCredentialService.ToogleUserCredentialArchive(id);
            if (isArchived == null)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "The credential assigned to the user is archived. Please unarchive the credential before unarchiving the user credential.");
            }

            return Ok(new { Message = "Success! Credential has been " + (isArchived == true ? "archived" : "unarchived") + " successfully." });
        }

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("GetAttachments")]
        public IActionResult GetAttachments(int userCredentialId) => Ok(userCredentialService.GetAttachments(userCredentialId));

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpPost("RenameAttachment")]
        public IActionResult RenameAttachment(int attachmentId, string name)
        {
            userCredentialService.RenameAttachment(attachmentId, name);

            return Ok(new { Message = "Success! File has been renamed to " + name });
        }

        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("DeleteAttachment")]
        public IActionResult DeleteAttachment(int attachmentId)
        {
            userCredentialService.DeleteAttachment(attachmentId);

            return Ok(new { Message = "Success! File has been deleted" });
        }

        [AllowAnonymous]
        [Authorize(Roles = "Admin, Project Manager")]
        [HttpGet("SearchEmployeeByCredential")]
        public IActionResult SearchEmployeeByCredential(string credentialIds) =>  Ok(userCredentialService.SearchEmployeeByCredential(Array.ConvertAll(credentialIds.Split(','), int.Parse)));
    }
}
