﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Helpers
{
    public static class Helper
    {
        private static string DIRECTORY_PATH = Path.Combine(Environment.CurrentDirectory, "../Attachments");

        public static bool SaveFileToServer(IFormFile file, string filePath)
        {
            try
            {
                using (var stream = new FileStream(DIRECTORY_PATH + filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int GetLoggedInUserId(this ClaimsPrincipal user)
        {
            return int.Parse(user.Claims.First(i => i.Type == "UserId").Value);
        }

        public static List<string> GetRolesFromToken(string jwtToken)
        {
            try
            {
                if (CanReadToken(jwtToken))
                {
                    var handler = new JwtSecurityTokenHandler();
                    var jsonToken = handler.ReadToken(jwtToken);
                    var tokenS = handler.ReadToken(jwtToken) as JwtSecurityToken;
                    var roles = tokenS.Claims.Where(claim => claim.Type == "role").Select(x => x.Value).ToList();

                    if (roles == null)
                    {
                        return new List<string>();
                    }

                    return roles;
                }
                return new List<string>();
            }
            catch (Exception ex)
            {
                return new List<string>();
            }
        }

        private static bool CanReadToken(string jwtToken)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Startup.StaticConfig["JwtSecretKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            List<Exception> validationFailures = null;
            SecurityToken validatedToken;
            var validator = new JwtSecurityTokenHandler();
            // These need to match the values used to generate the token
            TokenValidationParameters validationParameters = new TokenValidationParameters();
            validationParameters.ValidIssuer = "AES";
            validationParameters.ValidAudience = "AES_USERS";
            validationParameters.IssuerSigningKey = key;
            validationParameters.ValidateIssuerSigningKey = true;
            validationParameters.ValidateAudience = true;

            ClaimsPrincipal principal;
            try
            {
                principal = validator.ValidateToken(jwtToken, validationParameters, out validatedToken);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static byte[] CombineAllFilesToPdf(List<String> filePaths)
        {
            var file = new MemoryStream();
            var concatenate = new PdfConcatenate(file);
            
            foreach (var filePath in filePaths)
            {
                string actualPath = Path.Combine(Environment.CurrentDirectory, "../" + filePath);
                var fileExtension = Path.GetExtension(filePath);
                if (fileExtension == ".pdf" || fileExtension == ".doc" || fileExtension == ".docx")
                {
                    var pdf = System.IO.File.ReadAllBytes(actualPath);
                    concatenate.AddPages(new PdfReader(pdf));
                }
                else
                {
                    using (var ms = new MemoryStream())
                    {
                        var document = new iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0);
                        PdfWriter.GetInstance(document, ms).SetFullCompression();
                        document.Open();

                        iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(actualPath);
                        if (img.Height > img.Width)
                        {
                            float percentage = 0.0f;
                            percentage = 700 / img.Height;

                            img.ScalePercent(percentage * 100);
                        }
                        else
                        {
                            float percentage = 0.0f;
                            percentage = 540 / img.Width;
                            img.ScalePercent(percentage * 100);
                        }
                        img.Border = iTextSharp.text.Rectangle.BOX;
                        img.BorderColor = iTextSharp.text.BaseColor.BLACK;
                        img.SetAbsolutePosition((PageSize.A4.Width - img.ScaledWidth) / 2, (PageSize.A4.Height - img.ScaledHeight) / 2);

                        document.Add(img);
                        document.Close();

                        var reader = new PdfReader(ms.ToArray());
                        concatenate.AddPages(reader);
                    }

                    //iTextSharp.text.Rectangle pageSize = null;
                    //using (var srcImage = new Bitmap(actualPath))
                    //{
                    //    pageSize = new iTextSharp.text.Rectangle(0, 0, srcImage.Width, srcImage.Height);
                    //}

                    //using (var ms = new MemoryStream())
                    //{
                    //    var document = new iTextSharp.text.Document(pageSize, 0, 0, 0, 0);
                    //    PdfWriter.GetInstance(document, ms).SetFullCompression();
                    //    document.Open();

                    //    var image = iTextSharp.text.Image.GetInstance(actualPath);
                    //    document.Add(image);
                    //    document.Close();

                    //    var reader = new PdfReader(ms.ToArray());
                    //    concatenate.AddPages(reader);
                    //}
                }
            }

            concatenate.Close();

            return file.ToArray();
        }
    }
}
