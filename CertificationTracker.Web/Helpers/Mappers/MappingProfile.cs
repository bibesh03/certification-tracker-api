﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CertificationTracker.Dto.User;
using CertificationTracker.Models;
using CertificationTracker.Dto.Credential;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using CertificationTracker.Dto.Client;
using CertificationTracker.Dto.Proposal;
using CertificationTracker.Dto.Project;
using CertificationTracker.Dto.Form;
using CertificationTracker.Dto.Job;

namespace CertificationTracker.Web.Helpers.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //User Mapping
            CreateMap<RegisterUserDto, User>()
                .ForMember(x => x.Address, src => src.MapFrom(user => new Address
                {
                    Address1 = user.Address1,
                    Address2 = user.Address2,
                    City = user.City,
                    State = user.State,
                    ZipCode = user.ZipCode,
                }));

            CreateMap<User, RegisterUserDto>()
                .ForMember(x => x.Address1, src => src.MapFrom(user => user.Address.Address1))
                .ForMember(x => x.Address2, src => src.MapFrom(user => user.Address.Address2))
                .ForMember(x => x.City, src => src.MapFrom(user => user.Address.City))
                .ForMember(x => x.State, src => src.MapFrom(user => user.Address.State))
                .ForMember(x => x.ZipCode, src => src.MapFrom(user => user.Address.ZipCode))
                .ForMember(x => x.Roles, src => src.MapFrom(user => GetRoleForUser(user)))
                .ForMember(x => x.PictureUrl, src => src.MapFrom(user => user.ImageUrl))
                .ForMember(x => x.ImageUrl, src => src.Ignore());

            CreateMap<User, UserListingDto>()
                .ForMember(x => x.FullName, src => src.MapFrom(user => user.LastName + ", " + user.FirstName ))
                .ForMember(x => x.Role, src => src.MapFrom(user => GetRoleForUser(user)));

            CreateMap<User, UserProfileCardDto>()
                .ForMember(x => x.Roles, src => src.MapFrom(user => GetRoleForUser(user)))
                .ForMember(x => x.Location, src => src.MapFrom(user => user.Address != null ? user.Address.City : "-"));

            CreateMap<User, UserPersonalInformationDto>()
                .ForMember(x => x.Address1, src => src.MapFrom(user => user.Address.Address1))
                .ForMember(x => x.Address2, src => src.MapFrom(user => user.Address.Address2))
                .ForMember(x => x.City, src => src.MapFrom(user => user.Address.City))
                .ForMember(x => x.State, src => src.MapFrom(user => user.Address.State))
                .ForMember(x => x.ZipCode, src => src.MapFrom(user => user.Address.ZipCode))
                .ForMember(x => x.SSN, src => src.MapFrom(user => String.IsNullOrEmpty(user.SSN) ? "" : MaskSSN(DecryptText(user.SSN, user.SSNKey))));

            CreateMap<UserPersonalInformationDto, User>()
                .ForMember(x => x.SSN, src => src.Ignore());

            CreateMap<User, UserCompanyInfoDto>()
                .ForMember(x => x.Roles, src => src.MapFrom(user => GetRoleForUser(user)));

            CreateMap<UserCompanyInfoDto, RegisterUserDto>();

            CreateMap<UserCompanyInfoDto, User>();

            CreateMap<RegisterUserDto, Address>().ReverseMap();

            //TrainingType
            CreateMap<TrainingType, TrainingType>();

            //TrainingSubType
            CreateMap<TrainingSubType, TrainingSubType>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Name))
                .ForMember(d => d.Description, o => o.MapFrom(s => s.Description));

            //IntervalType
            CreateMap<IntervalType, IntervalType>();

            //Credentials
            CreateMap<Credential, CredentialListingDto>()
                .ForMember(x => x.TrainingType, src => src.MapFrom(cert => cert.TrainingType.Name))
                .ForMember(x => x.TrainingSubType, src => src.MapFrom(cert => cert.TrainingSubType.Name))
                .ForMember(x => x.IntervalType, src => src.MapFrom(cert => cert.IntervalType.Name));

            CreateMap<Credential, Credential>();

            CreateMap<UserCredential, UserCredentialListingDto>()
                .ForMember(x => x.Credential, src => src.MapFrom(cert => cert.Credential.Name))
                .ForMember(x => x.Type, src => src.MapFrom(cert => cert.Credential.TrainingType.Name))
                .ForMember(x => x.SubType, src => src.MapFrom(cert => cert.Credential.TrainingSubType.Name))
                .ForMember(x => x.Name, src => src.MapFrom(cert => cert.User.FirstName + " " + cert.User.LastName))
                .ForMember(x => x.ArchivedDateTime, src => src.MapFrom(cert => cert.ArchivedDateTime == null ? "-" : ConvertToFormattedDate(cert.ArchivedDateTime.Value)))
                .ForMember(x => x.EffectiveDate, src => src.MapFrom(cert => ConvertToFormattedDate(cert.EffectiveDate)))
                .ForMember(x => x.ExpiryDate, src => src.MapFrom(cert => ConvertToFormattedDate(cert.ExpiryDate)));

            CreateMap<UserCredential, UserCredential>();

            CreateMap<UserCredentialDetailDto, UserCredential>()
                .ForMember(x => x.TrainingSubTypeId, src => src.MapFrom(cert => cert.TrainingSubTypeId == 0 ? null : cert.TrainingSubTypeId));

            //Client
            CreateMap<Client, Client>().ForMember(x => x.LogoPath, src => src.Ignore());

            CreateMap<Client, ClientListingDto>()
                .ForMember(x => x.Address, src => src.MapFrom(client => client.Address.Address1))
                .ForMember(x => x.City, src => src.MapFrom(client => client.Address.City))
                .ForMember(x => x.State, src => src.MapFrom(client => client.Address.State))
                .ForMember(x => x.ZipCode, src => src.MapFrom(client => client.Address.ZipCode))
                .ForMember(x => x.Locations, src => src.MapFrom(client => client.Locations.Count()));


            CreateMap<ClientDetailDto, Client>()
                .ForMember(x => x.Address, src => src.MapFrom(user => new Address
                {
                    Address1 = user.Address1,
                    Address2 = user.Address2,
                    City = user.City,
                    State = user.State,
                    ZipCode = user.ZipCode,
                }));

            CreateMap<ClientDetailDto, Address>()
                .ForMember(x => x.Address1, x => x.MapFrom(a => a.Address1))
                .ForMember(x => x.Address2, x => x.MapFrom(a => a.Address2))
                .ForMember(x => x.City, x => x.MapFrom(a => a.City))
                .ForMember(x => x.State, x => x.MapFrom(a => a.State))
                .ForMember(x => x.ZipCode, x => x.MapFrom(a => a.ZipCode));

            //Location
            CreateMap<Location, Location>();

            //Project Type
            CreateMap<ProjectType, ProjectType>();

            //Project SubType
            CreateMap<ProjectSubType, ProjectSubType>();
            CreateMap<ProjectSubType, ProjectSubTypeListingDto>()
                .ForMember(x => x.ProjectType, x => x.MapFrom(x => x.ProjectType == null ? "-" : x.ProjectType.Name));

            //Projects
            CreateMap<Project, Project>();

            CreateMap<ProjectForm, ProjectForm>();

            CreateMap<ProjectForm, ProjectFormListingDto>()
                .ForMember(x => x.FormName, src => src.MapFrom(a => a.Form.Name));

            CreateMap<Project, ProjectListingDto>()
                .ForMember(x => x.ProjectSubTypeId, x => x.MapFrom(x => x.ProjectSubType == null ? 0 : x.ProjectSubTypeId))
                .ForMember(x => x.ProjectType, x => x.MapFrom(x => x.ProjectType == null ? 0 : x.ProjectTypeId))
                .ForMember(x => x.ProjectSubType, x => x.MapFrom(x => x.ProjectSubType == null ? "-" : x.ProjectSubType.Name))
                .ForMember(x => x.ProjectType, x => x.MapFrom(x => x.ProjectType == null ? "-" : x.ProjectType.Name))
                .ForMember(x => x.Forms, x => x.MapFrom(src => String.Join(", ", src.ProjectForms.Select(x => x.Form.Name))));

            //Proposal
            CreateMap<Proposal, Proposal>();

            CreateMap<Proposal, ProposalManageCardDto>()
                .ForMember(x => x.ProposalNumber, x => x.MapFrom(x => (x.Id + "").PadLeft(7, '0')))
                .ForMember(x => x.CreatedBy, x=> x.MapFrom(src => src.CreatedByUser.FirstName + " " + src.CreatedByUser.LastName))
                .ForMember(x => x.UpdatedBy, x=> x.MapFrom(src => src.UpdatedByUser != null ? (src.UpdatedByUser.FirstName + " " + src.UpdatedByUser.LastName) : "-"))
                .ForMember(x => x.SubmittedBy, x => x.MapFrom(src => src.SentByUser != null ? (src.SentByUser.FirstName + " " + src.SentByUser.LastName) : "-"))
                .ForMember(x => x.CreatedOn, x => x.MapFrom(x => ConvertToFormattedDateTime(x.CreatedDateTime.Value)))
                .ForMember(x => x.SubmittedOn, x => x.MapFrom(x => x.SentToClientDateTime != null ? ConvertToFormattedDateTime(x.SentToClientDateTime.Value) : "-"))
                .ForMember(x => x.UpdatedOn, x => x.MapFrom(x => x.UpdatedDateTime != null ? ConvertToFormattedDateTime(x.UpdatedDateTime.Value) : "-"));

            CreateMap<Proposal, ProposalListingDto>()
                .ForMember(x => x.Location, x => x.MapFrom(x => !String.IsNullOrEmpty(x.Location.BusinessName) ? x.Location.BusinessName + ", " + x.Location.Address.Address1 : x.Location.Address.Address1))
                .ForMember(x => x.ProjectSubType, x => x.MapFrom(x => x.ProjectSubType.Name))
                .ForMember(x => x.ProjectType, x => x.MapFrom(x => x.ProjectType.Name))
                .ForMember(x => x.CreatedBy, x => x.MapFrom(x => x.CreatedByUser.FirstName + " " + x.CreatedByUser.LastName))
                .ForMember(x => x.Client, x => x.MapFrom(x => x.Client.Name))
                .ForMember(x => x.ProposalId, x => x.MapFrom(x => (x.Id + "").PadLeft(7, '0')))
                .ForMember(x => x.ExpiryDate, x=> x.MapFrom(x => ConvertToFormattedDate(x.ExpirationDate)))
                .ForMember(x => x.IsApproved, x => x.MapFrom(x => (x.RejectionReason == null && x.HasApproved == null) ? null : (bool?)(x.RejectionReason == null && x.HasApproved == true)));

            CreateMap<ScopeOfWork, ScopeOfWork>();

            CreateMap<ProposalEstimatorDto, Proposal>().ReverseMap();

            CreateMap<Proposal, ProposalLogDto>()
                .ForMember(x => x.Client, src => src.MapFrom(x => x.Client.Name))
                .ForMember(x => x.ProjectType, src => src.MapFrom(x => x.ProjectType.Name))
                .ForMember(x => x.ProjectSubType, src => src.MapFrom(x => x.ProjectSubType.Name))
                .ForMember(x => x.Projects, src => src.MapFrom(x => String.Join(", ", x.ProposalProjects.Select(x => x.Project.Name).ToArray())))
                .ForMember(x => x.ReferralSource, src => src.MapFrom(x => x.ReferralSource.Name))
                .ForMember(x => x.JobId, src => src.MapFrom(x => x.Job.Id));

            CreateMap<ProposalProjectLocation, ProposalProjectLocation>();

            //Proposal Attachment
            CreateMap<ProposalAttachment, ProposalAttachment>();

            CreateMap<ProposalAttachmentDto, ProposalAttachment>();

            CreateMap<ProposalAttachment, ProposalAttachmentListingDto>()
                .ForMember(x => x.UploadedBy, x => x.MapFrom(src => src.UploadedBy.FirstName + " " + src.UploadedBy.LastName))
                .ForMember(x => x.UploadedDateTime, x => x.MapFrom(src => ConvertToFormattedDateTime(src.UploadedDateTime)))
                .ForMember(x => x.FileExtension, x => x.MapFrom(src => Path.GetExtension(src.FileLocation)));

            //Proposal Contact Logs
            CreateMap<ProposalContactLog, ProposalContactLog>();

            CreateMap<ProposalContactLog, ProposalContactLogDto>()
                .ForMember(x => x.Role, x => x.MapFrom(src => GetRoleForUser(src.User)))
                .ForMember(x => x.Email, x => x.MapFrom(src => src.User.Email))
                .ForMember(x => x.Phone, x => x.MapFrom(src => src.User.PhoneNumber))
                .ForMember(x => x.CreatedDate, x => x.MapFrom(src => ConvertToFormattedDateTime(src.CreatedDate)))
                .ForMember(x => x.FollowUpDate, x => x.MapFrom(src => ConvertToFormattedDate(src.FollowUpDate)))
                .ForMember(x => x.Name, x => x.MapFrom(src => src.User.FirstName + " " + src.User.LastName))
                .ForMember(x => x.ImageUrl, x => x.MapFrom(src => src.User.ImageUrl));

            //Referral Sources
            CreateMap<ReferralSource, ReferralSource>();

            //Setting
            CreateMap<Setting, Setting>();

            //Setting
            CreateMap<ReportEmail, ReportEmail>();

            //EmployeeSearc
            CreateMap<User, EmployeeSearchDto>()
                .ForMember(x => x.FullName, x => x.MapFrom(src => src.FirstName + " " + src.LastName));

            //UserCredentialAttachment
            CreateMap<UserCredentialAttachment, UserCredentialAttachmentDto>()
                .ForMember(x => x.UploadedDateTime, x => x.MapFrom(src => ConvertToFormattedDateTime(src.UploadedDateTime)));

            //Proposal Reviewers
            CreateMap<ProposalReviewer, ProposalReviewerHistoryDto>()
                .ForMember(x => x.AssignedDate, src => src.MapFrom(y => ConvertToFormattedDateTime(y.AssignedDateTime)))
                .ForMember(x => x.HasReviewed, src => src.MapFrom(y => y.IsReviewed))
                .ForMember(x => x.ProposalRole, src => src.MapFrom(x => "Reviewer"))
                .ForMember(x => x.ReviewedDateTime, src => src.MapFrom(y => y.DateReviewed != null ? ConvertToFormattedDateTime(y.DateReviewed.Value) : "N/A"))
                .ForMember(x => x.FullName, src => src.MapFrom(y => y.User.FirstName + " " + y.User.LastName))
                .ForMember(x => x.ImageUrl, src => src.MapFrom(y => y.User.ImageUrl));

            //Proposal Reviewer Logs
            CreateMap<ProposalMessageDto, ProposalReviewersLog>();

            CreateMap<ProposalReviewersLog, ProposalMessageDto>()
                .ForMember(x => x.ImageUrl, src => src.MapFrom(z => z.User.ImageUrl))
                .ForMember(x => x.Name, src => src.MapFrom(z => z.User.FirstName + " " + z.User.LastName));

            //Form
            CreateMap<Form, Form>();

            CreateMap<UploadForm, UploadForm>();

            CreateMap<UploadFormDto, Form>()
                .ForMember(x => x.UploadForm, src => src.MapFrom(x => new UploadForm
                {
                    UpdatedDateTime = DateTime.Now,
                    FilePath = x.FilePath
                }));

            CreateMap<Form, UploadFormDto>()
                .ForMember(x => x.FilePath, src => src.MapFrom(x => x.UploadForm.FilePath));

            CreateMap<Form, FormListingDto>()
                .ForMember(x => x.FormType, src => src.MapFrom(x => x.FormType.Name))
                .ForMember(x => x.FileExtension, src => src.MapFrom(x => GetFileExtensionForForm(x)));

            //Job
            CreateMap<Job, Job>();

            CreateMap<Job, JobListingDto>()
                .ForMember(x => x.Client, src => src.MapFrom(x => x.Proposal.Client.Name))
                .ForMember(x => x.ProjectNumber, src => src.MapFrom(x => x.Proposal.ProjectNumber))
                .ForMember(x => x.ProjectName, src => src.MapFrom(x => x.Proposal.ProjectName));

            CreateMap<Job, JobCardViewDto>()
                .ForMember(x => x.AwardedDateTime, src => src.MapFrom(x => x.Proposal.ApprovedOrRejectedDateTime != null ? ConvertToFormattedDateTime(x.Proposal.ApprovedOrRejectedDateTime.Value) : ""))
                .ForMember(x => x.ProjectNumber, src => src.MapFrom(x => x.Proposal.ProjectNumber))
                .ForMember(x => x.ProposalId, src => src.MapFrom(x => x.Proposal.Id))
                .ForMember(x => x.ProjectName, src => src.MapFrom(x => x.Proposal.ProjectName));

            CreateMap<JobDocumentDto, JobDocument>();

            //Job Team Member
            CreateMap<JobTeamMember, JobTeamMemberListingDto>()
                .ForMember(x => x.Name, src => src.MapFrom(member => member.User.FirstName + " " + member.User.LastName));
        }

        private string GetFileExtensionForForm(Form form)
        {
            switch (form.FormType.Name)
            {
                case "UPLOAD":
                    return Path.GetExtension(form.UploadForm.FilePath);
                case "BUILT-IN":
                    return "BUILT-IN";
                case "CUSTOM":
                    return "CUSTOM";
                default:
                    return "";
            }
        }

        private string ConvertToFormattedDate(DateTime date) => date.ToString("MM/dd/yyyy");

        private string ConvertToFormattedDateTime(DateTime date) => date.ToString("MM/dd/yyyy hh:mm:ss tt");

        private string GetRoleForUser(User user) => string.Join(", ", user.UserInRoles.ToList().Select(x => x.Role.Name));

        private string MaskSSN(string ssn)
        {
            ssn = ssn.Replace("-", "");
            Regex ssnRegex = new Regex("(?:[0-9]{3})(?:[0-9]{2})(?<last>[0-9]{4})");
            string formattedSSN = ssnRegex.Replace(ssn, "***-**-${last}");

            return formattedSSN;
        }

        private string DecryptText(string input, string ssnKey)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(ssnKey));
            byte[] bytesDecrypted = null;
            // The salt bytes must be at least 8 bytes. Private key.
            byte[] saltBytes = new byte[] { 53, 59, 61, 67, 71, 73, 79, 83 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    bytesDecrypted = ms.ToArray();
                }
            }

            return Encoding.UTF8.GetString(bytesDecrypted);
        }
    }
}
