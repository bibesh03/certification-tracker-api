﻿using CertificationTracker.Dto.Proposal;
using CertificationTracker.Interface.Management;
using CertificationTracker.Interface.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Hubs
{
    public class Typing
    {
        public int ProposalId { get; set; }
        public bool IsTyping { get; set; }
        public int TypingId { get; set; }
    }

    [Authorize]
    public class ChatHub : Hub
    {
        private readonly IProposalReviewersLogService proposalReviewersLogService;
        private readonly IUserService userService;

        public ChatHub(IProposalReviewersLogService _proposalReviewersLogService, IUserService _userService)
        {
            this.proposalReviewersLogService = _proposalReviewersLogService;
            this.userService = _userService;
        }

        public async Task SendProposalMessage(ProposalMessageDto messageDto)
        {
            var isMessageSaved = proposalReviewersLogService.SaveMessage(messageDto);
            if (isMessageSaved)
            {
                var user = userService.GetUserById(messageDto.SenderId ?? 0);
                messageDto.Name = user.FirstName + " " + user.LastName;
                messageDto.ImageUrl = user.ImageUrl;
            }

            await Clients.All.SendAsync("ProposalMessage/" + messageDto.ProposalId, messageDto);
        }

        public async Task IsSomeoneTypingOnProposal(Typing typing)
        {
            await Clients.All.SendAsync("IsSomeoneTypingOnProposal/" + typing.ProposalId, new { isTyping = typing.IsTyping, typingId = typing.TypingId });
        }
    }
} 