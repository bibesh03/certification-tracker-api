﻿using CertificationTracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertificationTracker.Web.Hubs
{
    [Authorize]
    public class NotificationHub : Hub
    {

        private readonly INotificationService notificationService;

        public NotificationHub(INotificationService _notificationService)
        {
            this.notificationService = _notificationService;
        }

        public async Task SendNotification(Notification notification)
        {
            await Clients.All.SendAsync("Notification" , notification);
        }
    }
}
