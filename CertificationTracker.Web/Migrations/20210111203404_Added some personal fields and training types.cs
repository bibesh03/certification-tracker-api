﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedsomepersonalfieldsandtrainingtypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AIId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DISAId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DriverLicenseNumber",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GulfCoastSafetyCouncilId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SafetyCouncilStudentId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TEICPin",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrainingTypeId",
                table: "Certifications",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "TrainingTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Certifications_TrainingTypeId",
                table: "Certifications",
                column: "TrainingTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Certifications_TrainingTypes_TrainingTypeId",
                table: "Certifications",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Certifications_TrainingTypes_TrainingTypeId",
                table: "Certifications");

            migrationBuilder.DropTable(
                name: "TrainingTypes");

            migrationBuilder.DropIndex(
                name: "IX_Certifications_TrainingTypeId",
                table: "Certifications");

            migrationBuilder.DropColumn(
                name: "AIId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DISAId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DriverLicenseNumber",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "GulfCoastSafetyCouncilId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SafetyCouncilStudentId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "TEICPin",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "TrainingTypeId",
                table: "Certifications");
        }
    }
}
