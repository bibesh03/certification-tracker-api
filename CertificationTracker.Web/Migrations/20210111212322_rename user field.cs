﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class renameuserfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TEICPin",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "TWICPin",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TWICPin",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "TEICPin",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
