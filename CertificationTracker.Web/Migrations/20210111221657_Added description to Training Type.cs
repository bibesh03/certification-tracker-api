﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class AddeddescriptiontoTrainingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Certifications_Users_UpdatedByUserId",
                table: "Certifications");

            migrationBuilder.DropIndex(
                name: "IX_Certifications_UpdatedByUserId",
                table: "Certifications");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Certifications");

            migrationBuilder.DropColumn(
                name: "UpdatedByUserId",
                table: "Certifications");

            migrationBuilder.DropColumn(
                name: "UpdatedDateTime",
                table: "Certifications");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "TrainingTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "TrainingTypes");

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Certifications",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedByUserId",
                table: "Certifications",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDateTime",
                table: "Certifications",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Certifications_UpdatedByUserId",
                table: "Certifications",
                column: "UpdatedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Certifications_Users_UpdatedByUserId",
                table: "Certifications",
                column: "UpdatedByUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
