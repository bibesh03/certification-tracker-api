﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class AddedtableIntervalType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Certifications_IntervalType_IntervalTypeId",
                table: "Certifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_IntervalType",
                table: "IntervalType");

            migrationBuilder.RenameTable(
                name: "IntervalType",
                newName: "IntervalTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_IntervalTypes",
                table: "IntervalTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Certifications_IntervalTypes_IntervalTypeId",
                table: "Certifications",
                column: "IntervalTypeId",
                principalTable: "IntervalTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Certifications_IntervalTypes_IntervalTypeId",
                table: "Certifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_IntervalTypes",
                table: "IntervalTypes");

            migrationBuilder.RenameTable(
                name: "IntervalTypes",
                newName: "IntervalType");

            migrationBuilder.AddPrimaryKey(
                name: "PK_IntervalType",
                table: "IntervalType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Certifications_IntervalType_IntervalTypeId",
                table: "Certifications",
                column: "IntervalTypeId",
                principalTable: "IntervalType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
