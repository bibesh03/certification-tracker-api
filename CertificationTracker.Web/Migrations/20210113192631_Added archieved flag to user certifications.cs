﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedarchievedflagtousercertifications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsArchieved",
                table: "UserCertifications",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<double>(
                name: "CreditHours",
                table: "Certifications",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsArchieved",
                table: "UserCertifications");

            migrationBuilder.AlterColumn<double>(
                name: "CreditHours",
                table: "Certifications",
                type: "float",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);
        }
    }
}
