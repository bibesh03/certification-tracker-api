﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Fixedarchivedspelling : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchievedDateTime",
                table: "UserCertifications");

            migrationBuilder.DropColumn(
                name: "IsArchieved",
                table: "UserCertifications");

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchivedDateTime",
                table: "UserCertifications",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchived",
                table: "UserCertifications",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchivedDateTime",
                table: "UserCertifications");

            migrationBuilder.DropColumn(
                name: "IsArchived",
                table: "UserCertifications");

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchievedDateTime",
                table: "UserCertifications",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchieved",
                table: "UserCertifications",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
