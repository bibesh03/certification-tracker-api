﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Cascadedeletelocationonclientdeletion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Clients_ClientId1",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_ClientId1",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "ClientId1",
                table: "Location");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClientId1",
                table: "Location",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location_ClientId1",
                table: "Location",
                column: "ClientId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Clients_ClientId1",
                table: "Location",
                column: "ClientId1",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
