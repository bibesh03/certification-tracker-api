﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class AddedLocationstodatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Address_AddressId",
                table: "Location");

            migrationBuilder.DropForeignKey(
                name: "FK_Location_Clients_ClientId",
                table: "Location");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Location",
                table: "Location");

            migrationBuilder.RenameTable(
                name: "Location",
                newName: "Locations");

            migrationBuilder.RenameIndex(
                name: "IX_Location_ClientId",
                table: "Locations",
                newName: "IX_Locations_ClientId");

            migrationBuilder.RenameIndex(
                name: "IX_Location_AddressId",
                table: "Locations",
                newName: "IX_Locations_AddressId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Locations",
                table: "Locations",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Address_AddressId",
                table: "Locations",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Clients_ClientId",
                table: "Locations",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Address_AddressId",
                table: "Locations");

            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Clients_ClientId",
                table: "Locations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Locations",
                table: "Locations");

            migrationBuilder.RenameTable(
                name: "Locations",
                newName: "Location");

            migrationBuilder.RenameIndex(
                name: "IX_Locations_ClientId",
                table: "Location",
                newName: "IX_Location_ClientId");

            migrationBuilder.RenameIndex(
                name: "IX_Locations_AddressId",
                table: "Location",
                newName: "IX_Location_AddressId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Location",
                table: "Location",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Address_AddressId",
                table: "Location",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Clients_ClientId",
                table: "Location",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id");
        }
    }
}
