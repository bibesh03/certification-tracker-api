﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class AddedAccountinginfocertificationtolocationandupdatedprojectsubtype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_ProjectSubTypes_ProjectSubTypeId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectSubTypes_ProjectTypes_ProjectTypeId",
                table: "ProjectSubTypes");

            migrationBuilder.DropIndex(
                name: "IX_ProjectSubTypes_ProjectTypeId",
                table: "ProjectSubTypes");

            migrationBuilder.AddColumn<string>(
                name: "AccountingEmail",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AccountingName",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AccountingPhone",
                table: "Locations",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProjectTypeProjectSubType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectTypeId = table.Column<int>(nullable: false),
                    ProjectSubTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectTypeProjectSubType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectTypeProjectSubType_ProjectSubTypes_ProjectSubTypeId",
                        column: x => x.ProjectSubTypeId,
                        principalTable: "ProjectSubTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectTypeProjectSubType_ProjectTypes_ProjectTypeId",
                        column: x => x.ProjectTypeId,
                        principalTable: "ProjectTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTypeProjectSubType_ProjectSubTypeId",
                table: "ProjectTypeProjectSubType",
                column: "ProjectSubTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTypeProjectSubType_ProjectTypeId",
                table: "ProjectTypeProjectSubType",
                column: "ProjectTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_ProjectSubTypes_ProjectSubTypeId",
                table: "Leads",
                column: "ProjectSubTypeId",
                principalTable: "ProjectSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_ProjectSubTypes_ProjectSubTypeId",
                table: "Leads");

            migrationBuilder.DropTable(
                name: "ProjectTypeProjectSubType");

            migrationBuilder.DropColumn(
                name: "AccountingEmail",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "AccountingName",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "AccountingPhone",
                table: "Locations");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectSubTypes_ProjectTypeId",
                table: "ProjectSubTypes",
                column: "ProjectTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_ProjectSubTypes_ProjectSubTypeId",
                table: "Leads",
                column: "ProjectSubTypeId",
                principalTable: "ProjectSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectSubTypes_ProjectTypes_ProjectTypeId",
                table: "ProjectSubTypes",
                column: "ProjectTypeId",
                principalTable: "ProjectTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
