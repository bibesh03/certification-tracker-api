﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class AddedrelationtoclientandlocationforLead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Leads_ClientId",
                table: "Leads",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LocationId",
                table: "Leads",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Clients_ClientId",
                table: "Leads",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Locations_LocationId",
                table: "Leads",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Clients_ClientId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Locations_LocationId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_ClientId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_LocationId",
                table: "Leads");
        }
    }
}
