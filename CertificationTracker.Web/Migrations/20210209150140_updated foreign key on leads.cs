﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class updatedforeignkeyonleads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Users_CreatedByUserId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Users_SubmittedByUserId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_CreatedByUserId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_SubmittedByUserId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "CreatedByUserId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "SubmittedByUserId",
                table: "Leads");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_CreatedBy",
                table: "Leads",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_SubmittedBy",
                table: "Leads",
                column: "SubmittedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Users_CreatedBy",
                table: "Leads",
                column: "CreatedBy",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Users_SubmittedBy",
                table: "Leads",
                column: "SubmittedBy",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Users_CreatedBy",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Users_SubmittedBy",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_CreatedBy",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_SubmittedBy",
                table: "Leads");

            migrationBuilder.AddColumn<int>(
                name: "CreatedByUserId",
                table: "Leads",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubmittedByUserId",
                table: "Leads",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Leads_CreatedByUserId",
                table: "Leads",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_SubmittedByUserId",
                table: "Leads",
                column: "SubmittedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Users_CreatedByUserId",
                table: "Leads",
                column: "CreatedByUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Users_SubmittedByUserId",
                table: "Leads",
                column: "SubmittedByUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
