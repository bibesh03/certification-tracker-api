﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedreferralsourcetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferralSource",
                table: "Leads");

            migrationBuilder.AddColumn<int>(
                name: "ReferralSourceId",
                table: "Leads",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ReferralSources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferralSources", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Leads_ReferralSourceId",
                table: "Leads",
                column: "ReferralSourceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_ReferralSources_ReferralSourceId",
                table: "Leads",
                column: "ReferralSourceId",
                principalTable: "ReferralSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_ReferralSources_ReferralSourceId",
                table: "Leads");

            migrationBuilder.DropTable(
                name: "ReferralSources");

            migrationBuilder.DropIndex(
                name: "IX_Leads_ReferralSourceId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "ReferralSourceId",
                table: "Leads");

            migrationBuilder.AddColumn<string>(
                name: "ReferralSource",
                table: "Leads",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
