﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedleadfileattachment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContactLogs",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeletedBy",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDateTime",
                table: "Leads",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Leads",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "LeadAttachments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadId = table.Column<int>(nullable: false),
                    UploadedById = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    UploadedDateTime = table.Column<DateTime>(nullable: false),
                    FileLocation = table.Column<string>(nullable: true),
                    DeletedById = table.Column<int>(nullable: true),
                    DeletedDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadAttachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeadAttachments_Users_DeletedById",
                        column: x => x.DeletedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LeadAttachments_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LeadAttachments_Users_UploadedById",
                        column: x => x.UploadedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Leads_DeletedBy",
                table: "Leads",
                column: "DeletedBy");

            migrationBuilder.CreateIndex(
                name: "IX_LeadAttachments_DeletedById",
                table: "LeadAttachments",
                column: "DeletedById");

            migrationBuilder.CreateIndex(
                name: "IX_LeadAttachments_LeadId",
                table: "LeadAttachments",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadAttachments_UploadedById",
                table: "LeadAttachments",
                column: "UploadedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Users_DeletedBy",
                table: "Leads",
                column: "DeletedBy",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Users_DeletedBy",
                table: "Leads");

            migrationBuilder.DropTable(
                name: "LeadAttachments");

            migrationBuilder.DropIndex(
                name: "IX_Leads_DeletedBy",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "ContactLogs",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "DeletedDateTime",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Leads");
        }
    }
}
