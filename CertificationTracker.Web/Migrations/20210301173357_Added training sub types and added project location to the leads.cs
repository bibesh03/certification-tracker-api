﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedtrainingsubtypesandaddedprojectlocationtotheleads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Clients_ClientId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Locations_LocationId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Clients_ClientId",
                table: "Locations");

            migrationBuilder.AddColumn<string>(
                name: "ProjectLocation",
                table: "Leads",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TrainingSubTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TrainingTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingSubTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                        column: x => x.TrainingTypeId,
                        principalTable: "TrainingTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TrainingSubTypes_TrainingTypeId",
                table: "TrainingSubTypes",
                column: "TrainingTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Clients_ClientId",
                table: "Leads",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Locations_LocationId",
                table: "Leads",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Clients_ClientId",
                table: "Locations",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Clients_ClientId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Locations_LocationId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Clients_ClientId",
                table: "Locations");

            migrationBuilder.DropTable(
                name: "TrainingSubTypes");

            migrationBuilder.DropColumn(
                name: "ProjectLocation",
                table: "Leads");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Clients_ClientId",
                table: "Leads",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Locations_LocationId",
                table: "Leads",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Clients_ClientId",
                table: "Locations",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id");
        }
    }
}
