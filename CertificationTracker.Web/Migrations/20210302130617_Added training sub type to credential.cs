﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedtrainingsubtypetocredential : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TrainingSubTypeId",
                table: "Credentials",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Credentials_TrainingSubTypeId",
                table: "Credentials",
                column: "TrainingSubTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Credentials_TrainingSubTypes_TrainingSubTypeId",
                table: "Credentials",
                column: "TrainingSubTypeId",
                principalTable: "TrainingSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Credentials_TrainingSubTypes_TrainingSubTypeId",
                table: "Credentials");

            migrationBuilder.DropIndex(
                name: "IX_Credentials_TrainingSubTypeId",
                table: "Credentials");

            migrationBuilder.DropColumn(
                name: "TrainingSubTypeId",
                table: "Credentials");
        }
    }
}
