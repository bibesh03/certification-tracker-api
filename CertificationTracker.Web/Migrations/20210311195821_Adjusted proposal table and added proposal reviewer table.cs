﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Adjustedproposaltableandaddedproposalreviewertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_Users_SubmittedBy",
                table: "Proposals");

            migrationBuilder.DropIndex(
                name: "IX_Proposals_SubmittedBy",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "IsSubmitted",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "SubmittedBy",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "SubmittedDateTime",
                table: "Proposals");

            migrationBuilder.AddColumn<bool>(
                name: "HasSentToClient",
                table: "Proposals",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "SentToClientDateTime",
                table: "Proposals",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SentUserId",
                table: "Proposals",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "Proposals",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDateTime",
                table: "Proposals",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProposalReviewer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ProposalId = table.Column<int>(nullable: false),
                    IsReviewed = table.Column<bool>(nullable: false),
                    AssignedDateTime = table.Column<DateTime>(nullable: false),
                    ReviewDeadLineDateTime = table.Column<DateTime>(nullable: false),
                    DateReviewed = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalReviewer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProposalReviewer_Proposals_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "Proposals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProposalReviewer_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_SentUserId",
                table: "Proposals",
                column: "SentUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_UpdatedBy",
                table: "Proposals",
                column: "UpdatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalReviewer_ProposalId",
                table: "ProposalReviewer",
                column: "ProposalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProposalReviewer_UserId",
                table: "ProposalReviewer",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_Users_SentUserId",
                table: "Proposals",
                column: "SentUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_Users_UpdatedBy",
                table: "Proposals",
                column: "UpdatedBy",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_Users_SentUserId",
                table: "Proposals");

            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_Users_UpdatedBy",
                table: "Proposals");

            migrationBuilder.DropTable(
                name: "ProposalReviewer");

            migrationBuilder.DropIndex(
                name: "IX_Proposals_SentUserId",
                table: "Proposals");

            migrationBuilder.DropIndex(
                name: "IX_Proposals_UpdatedBy",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "HasSentToClient",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "SentToClientDateTime",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "SentUserId",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "UpdatedDateTime",
                table: "Proposals");

            migrationBuilder.AddColumn<bool>(
                name: "IsSubmitted",
                table: "Proposals",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SubmittedBy",
                table: "Proposals",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SubmittedDateTime",
                table: "Proposals",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_SubmittedBy",
                table: "Proposals",
                column: "SubmittedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_Users_SubmittedBy",
                table: "Proposals",
                column: "SubmittedBy",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
