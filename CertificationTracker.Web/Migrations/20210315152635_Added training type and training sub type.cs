﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedtrainingtypeandtrainingsubtype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TrainingSubTypeId",
                table: "UserCredentials",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TrainingTypeId",
                table: "UserCredentials",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserCredentials_TrainingSubTypeId",
                table: "UserCredentials",
                column: "TrainingSubTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCredentials_TrainingTypeId",
                table: "UserCredentials",
                column: "TrainingTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserCredentials_TrainingSubTypes_TrainingSubTypeId",
                table: "UserCredentials",
                column: "TrainingSubTypeId",
                principalTable: "TrainingSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCredentials_TrainingTypes_TrainingTypeId",
                table: "UserCredentials",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserCredentials_TrainingSubTypes_TrainingSubTypeId",
                table: "UserCredentials");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCredentials_TrainingTypes_TrainingTypeId",
                table: "UserCredentials");

            migrationBuilder.DropIndex(
                name: "IX_UserCredentials_TrainingSubTypeId",
                table: "UserCredentials");

            migrationBuilder.DropIndex(
                name: "IX_UserCredentials_TrainingTypeId",
                table: "UserCredentials");

            migrationBuilder.DropColumn(
                name: "TrainingSubTypeId",
                table: "UserCredentials");

            migrationBuilder.DropColumn(
                name: "TrainingTypeId",
                table: "UserCredentials");
        }
    }
}
