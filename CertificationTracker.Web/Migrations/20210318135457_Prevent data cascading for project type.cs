﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Preventdatacascadingforprojecttype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes");

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes");

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
