﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedisarchivedflagtotypesubtypeandcredential : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Credentials");

            migrationBuilder.AddColumn<bool>(
                name: "IsArchived",
                table: "TrainingTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchived",
                table: "TrainingSubTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchived",
                table: "Credentials",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsArchived",
                table: "TrainingTypes");

            migrationBuilder.DropColumn(
                name: "IsArchived",
                table: "TrainingSubTypes");

            migrationBuilder.DropColumn(
                name: "IsArchived",
                table: "Credentials");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Credentials",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
