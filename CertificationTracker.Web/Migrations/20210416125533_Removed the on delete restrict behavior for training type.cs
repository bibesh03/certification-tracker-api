﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Removedtheondeleterestrictbehaviorfortrainingtype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes");

            migrationBuilder.DropColumn(
                name: "GulfCoastSafetyCouncilId",
                table: "Users");

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes");

            migrationBuilder.AddColumn<string>(
                name: "GulfCoastSafetyCouncilId",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
