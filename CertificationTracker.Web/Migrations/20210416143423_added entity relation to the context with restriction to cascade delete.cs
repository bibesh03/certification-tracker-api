﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class addedentityrelationtothecontextwithrestrictiontocascadedelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Credentials_TrainingTypes_TrainingTypeId",
                table: "Credentials");

            migrationBuilder.DropForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCredentials_Credentials_CredentialId",
                table: "UserCredentials");

            migrationBuilder.AddForeignKey(
                name: "FK_Credentials_TrainingTypes_TrainingTypeId",
                table: "Credentials",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCredentials_Credentials_CredentialId",
                table: "UserCredentials",
                column: "CredentialId",
                principalTable: "Credentials",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Credentials_TrainingTypes_TrainingTypeId",
                table: "Credentials");

            migrationBuilder.DropForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCredentials_Credentials_CredentialId",
                table: "UserCredentials");

            migrationBuilder.AddForeignKey(
                name: "FK_Credentials_TrainingTypes_TrainingTypeId",
                table: "Credentials",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingSubTypes_TrainingTypes_TrainingTypeId",
                table: "TrainingSubTypes",
                column: "TrainingTypeId",
                principalTable: "TrainingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCredentials_Credentials_CredentialId",
                table: "UserCredentials",
                column: "CredentialId",
                principalTable: "Credentials",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
