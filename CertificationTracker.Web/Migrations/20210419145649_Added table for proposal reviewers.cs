﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedtableforproposalreviewers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProposalReviewer_Proposals_ProposalId",
                table: "ProposalReviewer");

            migrationBuilder.DropForeignKey(
                name: "FK_ProposalReviewer_Users_UserId",
                table: "ProposalReviewer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProposalReviewer",
                table: "ProposalReviewer");

            migrationBuilder.RenameTable(
                name: "ProposalReviewer",
                newName: "ProposalReviewers");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalReviewer_UserId",
                table: "ProposalReviewers",
                newName: "IX_ProposalReviewers_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalReviewer_ProposalId",
                table: "ProposalReviewers",
                newName: "IX_ProposalReviewers_ProposalId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProposalReviewers",
                table: "ProposalReviewers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalReviewers_Proposals_ProposalId",
                table: "ProposalReviewers",
                column: "ProposalId",
                principalTable: "Proposals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalReviewers_Users_UserId",
                table: "ProposalReviewers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProposalReviewers_Proposals_ProposalId",
                table: "ProposalReviewers");

            migrationBuilder.DropForeignKey(
                name: "FK_ProposalReviewers_Users_UserId",
                table: "ProposalReviewers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProposalReviewers",
                table: "ProposalReviewers");

            migrationBuilder.RenameTable(
                name: "ProposalReviewers",
                newName: "ProposalReviewer");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalReviewers_UserId",
                table: "ProposalReviewer",
                newName: "IX_ProposalReviewer_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalReviewers_ProposalId",
                table: "ProposalReviewer",
                newName: "IX_ProposalReviewer_ProposalId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProposalReviewer",
                table: "ProposalReviewer",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalReviewer_Proposals_ProposalId",
                table: "ProposalReviewer",
                column: "ProposalId",
                principalTable: "Proposals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalReviewer_Users_UserId",
                table: "ProposalReviewer",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
