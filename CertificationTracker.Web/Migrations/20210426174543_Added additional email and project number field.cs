﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedadditionalemailandprojectnumberfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdditionalEmails",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectNumber",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdditionalEmails",
                table: "Locations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdditionalEmails",
                table: "Clients",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdditionalEmails",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ProjectNumber",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "AdditionalEmails",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "AdditionalEmails",
                table: "Clients");
        }
    }
}
