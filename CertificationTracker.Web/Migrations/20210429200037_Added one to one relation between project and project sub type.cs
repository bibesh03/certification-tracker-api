﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedonetoonerelationbetweenprojectandprojectsubtype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_ProjectSubTypes_ProjectSubTypeId",
                table: "Projects");

            migrationBuilder.DropTable(
                name: "ProjectTypeProjectSubType");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectTypeId",
                table: "ProjectSubTypes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectSubTypeId",
                table: "Projects",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectSubTypes_ProjectTypeId",
                table: "ProjectSubTypes",
                column: "ProjectTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_ProjectSubTypes_ProjectSubTypeId",
                table: "Projects",
                column: "ProjectSubTypeId",
                principalTable: "ProjectSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectSubTypes_ProjectTypes_ProjectTypeId",
                table: "ProjectSubTypes",
                column: "ProjectTypeId",
                principalTable: "ProjectTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_ProjectSubTypes_ProjectSubTypeId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectSubTypes_ProjectTypes_ProjectTypeId",
                table: "ProjectSubTypes");

            migrationBuilder.DropIndex(
                name: "IX_ProjectSubTypes_ProjectTypeId",
                table: "ProjectSubTypes");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectTypeId",
                table: "ProjectSubTypes",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProjectSubTypeId",
                table: "Projects",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "ProjectTypeProjectSubType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectSubTypeId = table.Column<int>(type: "int", nullable: false),
                    ProjectTypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectTypeProjectSubType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectTypeProjectSubType_ProjectSubTypes_ProjectSubTypeId",
                        column: x => x.ProjectSubTypeId,
                        principalTable: "ProjectSubTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectTypeProjectSubType_ProjectTypes_ProjectTypeId",
                        column: x => x.ProjectTypeId,
                        principalTable: "ProjectTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTypeProjectSubType_ProjectSubTypeId",
                table: "ProjectTypeProjectSubType",
                column: "ProjectSubTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectTypeProjectSubType_ProjectTypeId",
                table: "ProjectTypeProjectSubType",
                column: "ProjectTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_ProjectSubTypes_ProjectSubTypeId",
                table: "Projects",
                column: "ProjectSubTypeId",
                principalTable: "ProjectSubTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
