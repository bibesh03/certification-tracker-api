﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class AddedFormtypetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FormType",
                table: "Projects");

            migrationBuilder.AddColumn<int>(
                name: "FormTypeId",
                table: "Projects",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FormTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_FormTypeId",
                table: "Projects",
                column: "FormTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_FormTypes_FormTypeId",
                table: "Projects",
                column: "FormTypeId",
                principalTable: "FormTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_FormTypes_FormTypeId",
                table: "Projects");

            migrationBuilder.DropTable(
                name: "FormTypes");

            migrationBuilder.DropIndex(
                name: "IX_Projects_FormTypeId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "FormTypeId",
                table: "Projects");

            migrationBuilder.AddColumn<string>(
                name: "FormType",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
