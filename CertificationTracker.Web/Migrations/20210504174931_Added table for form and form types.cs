﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedtableforformandformtypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_FormTypes_FormTypeId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_FormTypeId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "FormTypeId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "UploadPath",
                table: "Projects");

            migrationBuilder.CreateTable(
                name: "ProjectForms",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    FormId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectForm", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectForm_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UploadForm",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FormId = table.Column<int>(type: "int", nullable: false),
                    FilePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedDateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadForm", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Form",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FormTypeId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    UploadFormId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Form", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Form_FormTypes_FormTypeId",
                        column: x => x.FormTypeId,
                        principalTable: "FormTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Form_UploadForm_UploadFormId",
                        column: x => x.UploadFormId,
                        principalTable: "UploadForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Form_FormTypeId",
                table: "Form",
                column: "FormTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Form_UploadFormId",
                table: "Form",
                column: "UploadFormId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectForm_FormId",
                table: "ProjectForm",
                column: "FormId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectForm_ProjectId",
                table: "ProjectForm",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadForm_FormId",
                table: "UploadForm",
                column: "FormId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectForm_Form_FormId",
                table: "ProjectForm",
                column: "FormId",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UploadForm_Form_FormId",
                table: "UploadForm",
                column: "FormId",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Form_UploadForm_UploadFormId",
                table: "Form");

            migrationBuilder.DropTable(
                name: "ProjectForms");

            migrationBuilder.DropTable(
                name: "UploadForm");

            migrationBuilder.DropTable(
                name: "Form");

            migrationBuilder.AddColumn<int>(
                name: "FormTypeId",
                table: "Projects",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UploadPath",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Projects_FormTypeId",
                table: "Projects",
                column: "FormTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_FormTypes_FormTypeId",
                table: "Projects",
                column: "FormTypeId",
                principalTable: "FormTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
