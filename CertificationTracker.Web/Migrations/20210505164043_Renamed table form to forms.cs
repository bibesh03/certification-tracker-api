﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Renamedtableformtoforms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Form_FormTypes_FormTypeId",
                table: "Form");

            migrationBuilder.DropForeignKey(
                name: "FK_Form_UploadForm_UploadFormId",
                table: "Form");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectForm_Form_FormId",
                table: "ProjectForm");

            migrationBuilder.DropForeignKey(
                name: "FK_UploadForm_Form_FormId",
                table: "UploadForm");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Form",
                table: "Form");

            migrationBuilder.RenameTable(
                name: "Form",
                newName: "Forms");

            migrationBuilder.RenameIndex(
                name: "IX_Form_UploadFormId",
                table: "Forms",
                newName: "IX_Forms_UploadFormId");

            migrationBuilder.RenameIndex(
                name: "IX_Form_FormTypeId",
                table: "Forms",
                newName: "IX_Forms_FormTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Forms",
                table: "Forms",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Forms_FormTypes_FormTypeId",
                table: "Forms",
                column: "FormTypeId",
                principalTable: "FormTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Forms_UploadForm_UploadFormId",
                table: "Forms",
                column: "UploadFormId",
                principalTable: "UploadForm",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectForm_Forms_FormId",
                table: "ProjectForm",
                column: "FormId",
                principalTable: "Forms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UploadForm_Forms_FormId",
                table: "UploadForm",
                column: "FormId",
                principalTable: "Forms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Forms_FormTypes_FormTypeId",
                table: "Forms");

            migrationBuilder.DropForeignKey(
                name: "FK_Forms_UploadForm_UploadFormId",
                table: "Forms");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectForm_Forms_FormId",
                table: "ProjectForm");

            migrationBuilder.DropForeignKey(
                name: "FK_UploadForm_Forms_FormId",
                table: "UploadForm");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Forms",
                table: "Forms");

            migrationBuilder.RenameTable(
                name: "Forms",
                newName: "Form");

            migrationBuilder.RenameIndex(
                name: "IX_Forms_UploadFormId",
                table: "Form",
                newName: "IX_Form_UploadFormId");

            migrationBuilder.RenameIndex(
                name: "IX_Forms_FormTypeId",
                table: "Form",
                newName: "IX_Form_FormTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Form",
                table: "Form",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Form_FormTypes_FormTypeId",
                table: "Form",
                column: "FormTypeId",
                principalTable: "FormTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Form_UploadForm_UploadFormId",
                table: "Form",
                column: "UploadFormId",
                principalTable: "UploadForm",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectForm_Form_FormId",
                table: "ProjectForm",
                column: "FormId",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UploadForm_Form_FormId",
                table: "UploadForm",
                column: "FormId",
                principalTable: "Form",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
