﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Removedtherelationshipfromuploadformtablewithformtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UploadForm_Forms_FormId",
                table: "UploadForm");

            migrationBuilder.DropIndex(
                name: "IX_UploadForm_FormId",
                table: "UploadForm");

            migrationBuilder.DropColumn(
                name: "FormId",
                table: "UploadForm");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FormId",
                table: "UploadForm",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_UploadForm_FormId",
                table: "UploadForm",
                column: "FormId");

            migrationBuilder.AddForeignKey(
                name: "FK_UploadForm_Forms_FormId",
                table: "UploadForm",
                column: "FormId",
                principalTable: "Forms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
