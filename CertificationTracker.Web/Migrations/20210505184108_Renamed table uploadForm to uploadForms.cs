﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class RenamedtableuploadFormtouploadForms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Forms_UploadForm_UploadFormId",
                table: "Forms");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UploadForm",
                table: "UploadForm");

            migrationBuilder.RenameTable(
                name: "UploadForm",
                newName: "UploadForms");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UploadForms",
                table: "UploadForms",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Forms_UploadForms_UploadFormId",
                table: "Forms",
                column: "UploadFormId",
                principalTable: "UploadForms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Forms_UploadForms_UploadFormId",
                table: "Forms");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UploadForms",
                table: "UploadForms");

            migrationBuilder.RenameTable(
                name: "UploadForms",
                newName: "UploadForm");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UploadForm",
                table: "UploadForm",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Forms_UploadForm_UploadFormId",
                table: "Forms",
                column: "UploadFormId",
                principalTable: "UploadForm",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
