﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedproposalprojectstable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProject_Projects_ProjectId",
                table: "ProposalProject");

            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProject_Proposals_ProposalId",
                table: "ProposalProject");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProposalProject",
                table: "ProposalProject");

            migrationBuilder.RenameTable(
                name: "ProposalProject",
                newName: "ProposalProjects");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalProject_ProposalId",
                table: "ProposalProjects",
                newName: "IX_ProposalProjects_ProposalId");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalProject_ProjectId",
                table: "ProposalProjects",
                newName: "IX_ProposalProjects_ProjectId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProposalProjects",
                table: "ProposalProjects",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProjects_Projects_ProjectId",
                table: "ProposalProjects",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProjects_Proposals_ProposalId",
                table: "ProposalProjects",
                column: "ProposalId",
                principalTable: "Proposals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProjects_Projects_ProjectId",
                table: "ProposalProjects");

            migrationBuilder.DropForeignKey(
                name: "FK_ProposalProjects_Proposals_ProposalId",
                table: "ProposalProjects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProposalProjects",
                table: "ProposalProjects");

            migrationBuilder.RenameTable(
                name: "ProposalProjects",
                newName: "ProposalProject");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalProjects_ProposalId",
                table: "ProposalProject",
                newName: "IX_ProposalProject_ProposalId");

            migrationBuilder.RenameIndex(
                name: "IX_ProposalProjects_ProjectId",
                table: "ProposalProject",
                newName: "IX_ProposalProject_ProjectId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProposalProject",
                table: "ProposalProject",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProject_Projects_ProjectId",
                table: "ProposalProject",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProposalProject_Proposals_ProposalId",
                table: "ProposalProject",
                column: "ProposalId",
                principalTable: "Proposals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
