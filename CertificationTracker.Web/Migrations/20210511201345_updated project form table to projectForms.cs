﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class updatedprojectformtabletoprojectForms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectForm_Forms_FormId",
                table: "ProjectForm");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectForm_Projects_ProjectId",
                table: "ProjectForm");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProjectForm",
                table: "ProjectForm");

            migrationBuilder.RenameTable(
                name: "ProjectForm",
                newName: "ProjectForms");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectForm_ProjectId",
                table: "ProjectForms",
                newName: "IX_ProjectForms_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectForm_FormId",
                table: "ProjectForms",
                newName: "IX_ProjectForms_FormId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProjectForms",
                table: "ProjectForms",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectForms_Forms_FormId",
                table: "ProjectForms",
                column: "FormId",
                principalTable: "Forms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectForms_Projects_ProjectId",
                table: "ProjectForms",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectForms_Forms_FormId",
                table: "ProjectForms");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectForms_Projects_ProjectId",
                table: "ProjectForms");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProjectForms",
                table: "ProjectForms");

            migrationBuilder.RenameTable(
                name: "ProjectForms",
                newName: "ProjectForm");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectForms_ProjectId",
                table: "ProjectForm",
                newName: "IX_ProjectForm_ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectForms_FormId",
                table: "ProjectForm",
                newName: "IX_ProjectForm_FormId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProjectForm",
                table: "ProjectForm",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectForm_Forms_FormId",
                table: "ProjectForm",
                column: "FormId",
                principalTable: "Forms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectForm_Projects_ProjectId",
                table: "ProjectForm",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
