﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedadditionalcolumnsforproposal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasSentToClient",
                table: "Proposals");

            migrationBuilder.AddColumn<int>(
                name: "ApprovedOrRejectedByUserId",
                table: "Proposals",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovedOrRejectedDateTime",
                table: "Proposals",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "HasApproved",
                table: "Proposals",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProposalHeader",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProposalSenderName",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProposalSenderSignature",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProposalSenderTitle",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RejectionReason",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ScheduleAndCharges",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ScopeOfWorks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProposalId = table.Column<int>(type: "int", nullable: false),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddedByUserId = table.Column<int>(type: "int", nullable: false),
                    AddedDateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScopeOfWorks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScopeOfWorks_Proposals_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "Proposals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScopeOfWorks_Users_AddedByUserId",
                        column: x => x.AddedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_ApprovedOrRejectedByUserId",
                table: "Proposals",
                column: "ApprovedOrRejectedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ScopeOfWorks_AddedByUserId",
                table: "ScopeOfWorks",
                column: "AddedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ScopeOfWorks_ProposalId",
                table: "ScopeOfWorks",
                column: "ProposalId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_Users_ApprovedOrRejectedByUserId",
                table: "Proposals",
                column: "ApprovedOrRejectedByUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_Users_ApprovedOrRejectedByUserId",
                table: "Proposals");

            migrationBuilder.DropTable(
                name: "ScopeOfWorks");

            migrationBuilder.DropIndex(
                name: "IX_Proposals_ApprovedOrRejectedByUserId",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ApprovedOrRejectedByUserId",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ApprovedOrRejectedDateTime",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "HasApproved",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ProposalHeader",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ProposalSenderName",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ProposalSenderSignature",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ProposalSenderTitle",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "RejectionReason",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "ScheduleAndCharges",
                table: "Proposals");

            migrationBuilder.AddColumn<bool>(
                name: "HasSentToClient",
                table: "Proposals",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
