﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Updatedcolumnsforproposal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_Users_SentUserId",
                table: "Proposals");

            migrationBuilder.RenameColumn(
                name: "SentUserId",
                table: "Proposals",
                newName: "SentByUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Proposals_SentUserId",
                table: "Proposals",
                newName: "IX_Proposals_SentByUserId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ApprovedOrRejectedDateTime",
                table: "Proposals",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_Users_SentByUserId",
                table: "Proposals",
                column: "SentByUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_Users_SentByUserId",
                table: "Proposals");

            migrationBuilder.RenameColumn(
                name: "SentByUserId",
                table: "Proposals",
                newName: "SentUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Proposals_SentByUserId",
                table: "Proposals",
                newName: "IX_Proposals_SentUserId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ApprovedOrRejectedDateTime",
                table: "Proposals",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_Users_SentUserId",
                table: "Proposals",
                column: "SentUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
