﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class AddednewtablesforscheduleofchargeandunitPfCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "SendToClient",
                table: "ProposalAttachments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "UnitOfCosts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Unit = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitOfCosts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScheduleOfCharges",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProposalId = table.Column<int>(type: "int", nullable: false),
                    UnitOfCostId = table.Column<int>(type: "int", nullable: false),
                    SortOrder = table.Column<int>(type: "int", nullable: false),
                    Service = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Cost = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleOfCharges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScheduleOfCharges_Proposals_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "Proposals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScheduleOfCharges_UnitOfCosts_UnitOfCostId",
                        column: x => x.UnitOfCostId,
                        principalTable: "UnitOfCosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleOfCharges_ProposalId",
                table: "ScheduleOfCharges",
                column: "ProposalId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleOfCharges_UnitOfCostId",
                table: "ScheduleOfCharges",
                column: "UnitOfCostId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScheduleOfCharges");

            migrationBuilder.DropTable(
                name: "UnitOfCosts");

            migrationBuilder.DropColumn(
                name: "SendToClient",
                table: "ProposalAttachments");
        }
    }
}
