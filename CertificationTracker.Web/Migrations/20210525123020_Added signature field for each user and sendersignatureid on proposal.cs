﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedsignaturefieldforeachuserandsendersignatureidonproposal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProposalSenderSignature",
                table: "Proposals");

            migrationBuilder.AddColumn<string>(
                name: "SignatureImageUrl",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProposalSenderSignatureUserId",
                table: "Proposals",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_ProposalSenderSignatureUserId",
                table: "Proposals",
                column: "ProposalSenderSignatureUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposals_Users_ProposalSenderSignatureUserId",
                table: "Proposals",
                column: "ProposalSenderSignatureUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposals_Users_ProposalSenderSignatureUserId",
                table: "Proposals");

            migrationBuilder.DropIndex(
                name: "IX_Proposals_ProposalSenderSignatureUserId",
                table: "Proposals");

            migrationBuilder.DropColumn(
                name: "SignatureImageUrl",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ProposalSenderSignatureUserId",
                table: "Proposals");

            migrationBuilder.AddColumn<string>(
                name: "ProposalSenderSignature",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
