﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Readdedtheprojectformtableagain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Frequency",
                table: "ProjectForms",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsShareable",
                table: "ProjectForms",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Frequency",
                table: "ProjectForms");

            migrationBuilder.DropColumn(
                name: "IsShareable",
                table: "ProjectForms");
        }
    }
}
