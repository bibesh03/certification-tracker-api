﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addeddeletedfieldonjobdocumenttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CreatedByUserId",
                table: "JobDocuments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "JobDocuments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDateTime",
                table: "JobDocuments",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "JobDocuments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_JobDocuments_CreatedByUserId",
                table: "JobDocuments",
                column: "CreatedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobDocuments_Users_CreatedByUserId",
                table: "JobDocuments",
                column: "CreatedByUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobDocuments_Users_CreatedByUserId",
                table: "JobDocuments");

            migrationBuilder.DropIndex(
                name: "IX_JobDocuments_CreatedByUserId",
                table: "JobDocuments");

            migrationBuilder.DropColumn(
                name: "CreatedByUserId",
                table: "JobDocuments");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "JobDocuments");

            migrationBuilder.DropColumn(
                name: "DeletedDateTime",
                table: "JobDocuments");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "JobDocuments");
        }
    }
}
