﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addednewtableforandorblockforjobcredentials : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobCredentialAndBlocks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JobId = table.Column<int>(type: "int", nullable: false),
                    CredentialId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCredentialAndBlocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobCredentialAndBlocks_Credentials_CredentialId",
                        column: x => x.CredentialId,
                        principalTable: "Credentials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobCredentialAndBlocks_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobCredentialOrBlock",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JobCredentialAndBlockId = table.Column<int>(type: "int", nullable: false),
                    CredentialId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCredentialOrBlock", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobCredentialOrBlock_Credentials_CredentialId",
                        column: x => x.CredentialId,
                        principalTable: "Credentials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobCredentialOrBlock_JobCredentialAndBlocks_JobCredentialAndBlockId",
                        column: x => x.JobCredentialAndBlockId,
                        principalTable: "JobCredentialAndBlocks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobCredentialAndBlocks_CredentialId",
                table: "JobCredentialAndBlocks",
                column: "CredentialId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCredentialAndBlocks_JobId",
                table: "JobCredentialAndBlocks",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCredentialOrBlock_CredentialId",
                table: "JobCredentialOrBlock",
                column: "CredentialId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCredentialOrBlock_JobCredentialAndBlockId",
                table: "JobCredentialOrBlock",
                column: "JobCredentialAndBlockId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobCredentialOrBlock");

            migrationBuilder.DropTable(
                name: "JobCredentialAndBlocks");
        }
    }
}
