﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedestimatednumberoffieldonthejobforpmstechniciansandprojectmanager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EstimatedNumberOfProjectManagers",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EstimatedNumberOfTechnicians",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EstimatedNumberOfWorkers",
                table: "Jobs",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstimatedNumberOfProjectManagers",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EstimatedNumberOfTechnicians",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EstimatedNumberOfWorkers",
                table: "Jobs");
        }
    }
}
