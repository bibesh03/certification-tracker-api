﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedmultipleproposalprojectlocations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProjectLocation",
                table: "Proposals");

            migrationBuilder.CreateTable(
                name: "ProposalProjectLocations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProposalId = table.Column<int>(type: "int", nullable: false),
                    ProjectLocation = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProposalProjectLocations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProposalProjectLocations_Proposals_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "Proposals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProposalProjectLocations_ProposalId",
                table: "ProposalProjectLocations",
                column: "ProposalId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProposalProjectLocations");

            migrationBuilder.AddColumn<string>(
                name: "ProjectLocation",
                table: "Proposals",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
