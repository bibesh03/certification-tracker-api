﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CertificationTracker.Web.Migrations
{
    public partial class Addedcredentialpdfpathandformleadonthejobteammembertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsFormLead",
                table: "JobTeamMembers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "JobCredentialPdfPath",
                table: "JobTeamMembers",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFormLead",
                table: "JobTeamMembers");

            migrationBuilder.DropColumn(
                name: "JobCredentialPdfPath",
                table: "JobTeamMembers");
        }
    }
}
