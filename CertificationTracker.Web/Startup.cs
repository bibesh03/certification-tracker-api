using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using CertificationTracker.Data;
using CertificationTracker.Services;
using CertificationTracker.Web.Extensions;
using CertificationTracker.Web.Helpers.Mappers;
using Microsoft.Extensions.FileProviders;
using System.IO;
using CertificationTracker.Interface;
using CertificationTracker.Interface.CompanyData;
using CertificationTracker.Services.CompanyData;
using CertificationTracker.Interface.Service;
using CertificationTracker.Services.Service;
using CertificationTracker.Interface.Management;
using CertificationTracker.Services.Management;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;
using CertificationTracker.Web.Hubs;
using CertificationTracker.Interface.Form;
using CertificationTracker.Services.Form;
using CertificationTracker.Services.Report;
using CertificationTracker.Interface.Report;

namespace CertificationTracker.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public static IConfiguration StaticConfig { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            StaticConfig = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("ClientPermission", policy =>
                {
                    policy.AllowAnyHeader()
                        .AllowAnyMethod()
                        .WithOrigins(new string[] { "http://demo.primtek.net", "http://localhost:3000" })
                        .AllowCredentials();
                });
            });

            services.AddControllers();
            services.AddMvc().AddNewtonsoftJson(o =>
            {
                o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            services.AddSignalR();

            //connection string setup
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<DataContext>(options => options.UseSqlServer(connectionString, b => b.MigrationsAssembly("CertificationTracker.Web")).EnableSensitiveDataLogging());

            //register services
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITrainingTypeService, TrainingTypeService>();
            services.AddTransient<IIntervalTypeService, IntervalTypeService>();
            services.AddTransient<IUserCredentialService, UserCredentialService>();
            services.AddTransient<ICredentialService, CredentialService>();
            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IProjectSubTypeService, ProjectSubTypeService>();
            services.AddTransient<IProjectTypeService, ProjectTypeService>();
            services.AddTransient<IProposalService, ProposalService>();
            services.AddTransient<IReferralSourceService, ReferralSourceService>();
            services.AddTransient<ISettingService, SettingService>();
            services.AddTransient<IReportEmailService, ReportEmailService>();
            services.AddTransient<IReportTypeService, ReportTypeService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IProposalAttachmentService, ProposalAttachmentService>();
            services.AddTransient<IProposalContactLogService, ProposalContactLogService>();
            services.AddTransient<ITrainingSubTypeService, TrainingSubTypeService>();
            services.AddTransient<IProposalReviewersLogService, ProposalReviewersLogService>();
            services.AddTransient<IFormTypeService, FormTypeService>();
            services.AddTransient<IFormService, FormService>();
            services.AddTransient<IUnitOfCostService, UnitOfCostService>();
            services.AddTransient<IPrintService, PrintService>();
            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<IProjectFormService, ProjectFormService>();
            services.AddTransient<IJobService, JobService>();
            services.AddTransient<IJobCredentialService, JobCredentialService>();
            services.AddTransient<IJobDocumentService, JobDocumentService>();
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<IJobCredentialAndBlockService, JobCredentialAndBlockService>();
            services.AddTransient<IJobTeamMemberService, JobTeamMemberService>();

            //configure automapper
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            //fluent email
            var serviceProvider = services.BuildServiceProvider();
            var _settingService = serviceProvider.GetService<ISettingService>();
            services.AddHttpContextAccessor();

            var client = new SmtpClient(_settingService.GetSettingByKey("smtpHost"), Convert.ToInt32(_settingService.GetSettingByKey("smtpPort")))
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_settingService.GetSettingByKey("smtpUsername"), _settingService.GetSettingByKey("smtpPassword")),
                EnableSsl = true,
            };
            services.AddFluentEmail(_settingService.GetSettingByKey("smtpUsername")).AddRazorRenderer().AddSmtpSender(client);


            //configure jwt authentication
            var key = Encoding.ASCII.GetBytes(Configuration["JwtSecretKey"]);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

                x.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];

                        // If the request is for our hub...
                        var path = context.HttpContext.Request.Path;
                        if (!string.IsNullOrEmpty(accessToken) &&
                            (path.StartsWithSegments("/hubs/chat") || path.StartsWithSegments("/hubs/notification")))
                        {
                            // Read the token out of the query string
                            context.Token = accessToken;
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

            //test
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            //error Handler
            app.UseNativeGlobalExceptionHandler();
            app.UseRouting();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(env.ContentRootPath, "../Attachments")),
                RequestPath = "/Attachments"
            });

            app.UseCors("ClientPermission");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/hubs/chat");
                endpoints.MapHub<NotificationHub>("/hubs/notification");
            });
             
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CertificationTracker API Endpoints");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
